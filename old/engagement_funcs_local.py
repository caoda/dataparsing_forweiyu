from __future__ import division


import Measurements_video as measurements_video
import Measurements_storyline as measurements_storyline
import Measurements_pdf as measurements_pdf
import Measurements_article as measurements_article
import json

#TODO: Bring life cycle, window layout and navi into this function to speed things up!
algo_debug = False
def print_debug(*args):
    if algo_debug is True:
        print(args);


def getRelevant(dictlist, key, valuelist):
      return [dictio for dictio in dictlist if dictio.get(key) in valuelist]

                                                                            #todo: a list of userIDs
def get_engagement_dic_chapter_users(courseID, chapter, startDate, endDate, miic_users, measurements):
    #measurements = {'articleM':articleM, 'videoM':videoM, 'pdfM':pdfM,'slideChM':slideChM,'slidePlayM':slidePlayM, 'slideCompM':slideCompM,'quizM':quizM,'navM':navM,'winM':winM, 'lifeM':lifeM}

    mChap = {}
    for key in measurements:
        if key is not 'lifeM': #LifeCycle Measurements have no chapter
            mChap[key] = getRelevant(measurements[key], 'chapter', [chapter])

        else:
            mChap[key] = measurements[key]

    contents=chapter.contents.all()

    users_infos = []

    for miic_user in miic_users:
        dicu = {}

        dicu['id'] = miic_user

        mChapU = {}

        for key in mChap:
            mChapU[key] = getRelevant(mChap[key], 'user', [miic_user])
            #get rid of everything from this user
            mChap[key] = [x for x in mChap[key] if x not in mChapU[key]]



        for c in contents:

            if c.type == 'epub':
                dic = get_article_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['article'] = dic
                    dicu['articleLen'] = dic['articleLength']/200.0


            if c.type == 'storyline':
                dic = get_slideshow_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['slideshow'] = dic
                    dicu['slideShowLen'] = dic['totalDuration']

            if c.type == 'captivate':
                dic = get_slideshow_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['slideshow'] = dic
                    dicu['capLen'] = dic['totalDuration']


            if c.type == 'lectora':
                dic = get_slideshow_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['slideshow'] = dic
                    dicu['lectoraLen'] = dic['totalDuration']

            if c.type == 'video':
                dic = get_video_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['video'] = dic
                    dicu['videoLen'] = dic['duration']

            if c.type == 'pdf':
                dic = get_pdf_engagement_values(courseID, chapter, c, startDate, endDate, miic_user, mChapU)

                if dic is not None:
                    dicu['pdf'] = dic
                    dicu['pdfLen'] = dic['numPages']

        users_infos.append(dicu)

    weight_chapter = 1 #todo: should initialize to 1 instead of 0
    modesLen = ['articleLen', 'slideShowLen', 'capLen', 'lectoraLen',  'videoLen', 'pdfLen']
    existing = []
    for item in modesLen:

        for dicu in users_infos:
            if item in dicu:
                weight_chapter += dicu[item]
                existing.append(item)
                break

    print_debug("check chapter %s,  weight: %s ; existing modes: %s  " %(chapter, weight_chapter,existing) )


    if len(users_infos) > 0:
        return process_and_normalise_engagement_chapter(users_infos), weight_chapter
    else:
        return None


def process_and_normalise_engagement_chapter(user_infos):
    # V = views; B = Bookmarks; H = Highliths; N = Notes; t = timeSpent
    maxArticleV = 0
    maxArticleT = 0
    maxArticleB = 0
    maxArticleH = 0
    maxArticleN = 0

    maxSlideshowV = 0
    maxSlideshowT = 0
    maxSlideshowB = 0
    maxSlideshowN = 0

    maxVideoT = 0
    maxVideoV = 0
    maxVideoB = 0
    maxVideoN = 0

    maxPdfV = 0
    maxPdfT = 0
    maxPdfB = 0
    maxPdfN = 0

    for info in user_infos:
        if 'article' in info:
            a = info['article']
            if a['views'] > maxArticleV:
                maxArticleV = a['views']

            if a['bookmarks'] > maxArticleB:
                maxArticleB = a['bookmarks']

            if a['highlights'] > maxArticleH:
                maxArticleH = a['highlights']

            if a['notes'] > maxArticleN:
                maxArticleN = a['notes']

            if a['totalTimeSpent'] > maxArticleT:
                maxArticleT = a['totalTimeSpent']

        if 'slideshow' in info:
            a = info['slideshow']
            if a['views'] > maxSlideshowV:
                maxSlideshowV = a['views']

            if a['bookmarks'] > maxSlideshowB:
                maxSlideshowB = a['bookmarks']

            if a['notes'] > maxSlideshowN:
                maxSlideshowN = a['notes']

            if a['totalTimeSpent'] > maxSlideshowT:
                maxSlideshowT = a['totalTimeSpent']

        if 'video' in info:
            a = info['video']
            if a['totalTimeSpent'] > maxVideoT:
                maxVideoT = a['totalTimeSpent']

            if a['views'] > maxVideoV:
                maxVideoV = a['views']

            if a['bookmarks'] > maxVideoB:
                maxVideoB = a['bookmarks']

            if a['notes'] > maxVideoN:
                maxVideoN = a['notes']

        if 'pdf' in info:
            a = info['pdf']
            if a['views'] > maxPdfV:
                maxPdfV = a['views']

            if a['totalTimeSpent'] > maxPdfT:
                maxPdfT = a['totalTimeSpent']

            if a['bookmarks'] > maxPdfB:
                maxPdfB = a['bookmarks']

            if a['notes'] > maxPdfN:
                maxPdfN = a['notes']


    for info in user_infos:
        if 'article' in info:

            a = info['article']
            aV = 0
            if maxArticleV > 0:
                aV = a['views'] / maxArticleV

            aT = 0
            if maxArticleT > 0:
                aT = a['totalTimeSpent'] / maxArticleT

            aB = 0
            if maxArticleB > 0:
                aB = a['bookmarks'] / maxArticleB

            aH = 0
            if maxArticleH > 0:
                aH = a['highlights'] / maxArticleH

            aN = 0
            if maxArticleN > 0:
                aN = a['notes'] / maxArticleN

            v = (aV + aB + aH + aN + aT) / 5.0
            info['article'] = v

        if 'slideshow' in info:

            a = info['slideshow']
            aV = 0
            if maxSlideshowV > 0:
                aV = a['views'] / maxSlideshowV

            aB = 0
            if maxSlideshowB > 0:
                aB = a['bookmarks'] / maxSlideshowB

            aN = 0
            if maxSlideshowN > 0:
                aN = a['notes'] / maxSlideshowN

            aT = 0
            if maxSlideshowT > 0:
                aT = a['totalTimeSpent'] / maxSlideshowT

            v = (aV + aB + aT + aN) / 4.0


            info['slideshow'] = v

        if 'video' in info:
            a = info['video']

            aB = 0
            if maxVideoB > 0:
                aB = a['bookmarks'] / maxVideoB

            aN = 0
            if maxVideoN > 0:
                aN = a['notes'] / maxVideoN

            aT = 0
            if maxVideoT > 0:
                aT = a['totalTimeSpent'] / maxVideoT

            aV = 0
            if maxVideoV > 0:
                aV = a['views'] / maxVideoV

            v = (aB + aT + aV + aN) / 4.0
            info['video'] = v

        if 'pdf' in info:
            a = info['pdf']
            aV = 0
            if maxPdfV > 0:
                aV = a['views'] / maxPdfV

            aT = 0
            if maxPdfT > 0:
                aT = a['views'] / maxPdfT

            aB = 0
            if maxPdfB > 0:
                aB = a['bookmarks'] / maxPdfB

            aN = 0
            if maxPdfN > 0:
                aN = a['notes'] / maxPdfN

            v = (aV + aB + aN + aT) / 4.0
            info['pdf'] = v

    maxArticle = 0
    maxSlideshow = 0
    maxVideo = 0
    maxPDF = 0

    for info in user_infos:

        if 'article' in info:
            a = info['article']

            if a > maxArticle:
                maxArticle = a

        if 'slideshow' in info:
            a = info['slideshow']

            if a > maxSlideshow:
                maxSlideshow = a

        if 'video' in info:
            a = info['video']
            if a > maxVideo:
                maxVideo = a

        if 'pdf' in info:
            a = info['pdf']

            if a > maxPDF:
                maxPDF = a

    for info in user_infos:

        itemsCount = 0
        total = 0

        if 'article' in info:
            a = info['article']

            if maxArticle > 0:
                info['article'] = (a / maxArticle) * 100
                itemsCount = itemsCount + 1
            else:
                info['article'] = 0

            total = total + info['article']


        if 'slideshow' in info:
            a = info['slideshow']

            if maxSlideshow > 0:
                info['slideshow'] = (a / maxSlideshow) * 100
                total = total + info['slideshow']


            else:
                info['slideshow'] = 0

            itemsCount = itemsCount + 1

        if 'video' in info:
            a = info['video']

            if maxVideo > 0:
                info['video'] = (a / maxVideo) * 100
                itemsCount = itemsCount + 1


            else:
                info['video'] = 0

            total = total + info['video']

        if 'pdf' in info:
            a = info['pdf']

            if maxPDF > 0:
                info['pdf'] = (a / maxPDF) * 100
                itemsCount = itemsCount + 1

            else:
                info['pdf'] = 0

            total = total + info['pdf']

        if itemsCount > 0:
            info['average'] = total / float(itemsCount)
        else:
            info['average'] = total


    return user_infos


def get_article_engagement_values(courseID, chapter, content, startDate, endDate, miic_user, measurements):
    dic = {}

    dic['highlights'] = 0
    dic['bookmarks'] = 0
    dic['notes'] = 0
    dic['views'] = 0
    dic['totalTimeSpent'] = 0
    dic['articleLength'] = 0

    articleMeasurements = getRelevant(measurements['articleM'], 'article', [content])

    data = None


    if len(articleMeasurements) > 0:


        naviMeasurements = measurements['navM']

        windowLayouts = measurements['winM']

        lifeCycles = measurements['lifeM']


        data = measurements_article.get_measurements({miic_user: articleMeasurements}, {miic_user: naviMeasurements},
                                                   {miic_user: windowLayouts}, {miic_user: lifeCycles})

    # print "result %s" % data
    if data is not None and data != -1 and len(data) > 0:
        dic['articleLength'] = data["articleLength"]
        dic['totalTimeSpent'] = data['totalTimeSpent']

        views = 0

        for view in data['views']:
            if view['timeSpent'] > 3 + view['timesViewed'] * 0.2:
                views = views + view['timesViewed']

        dic['views'] = views
    else:
        return None

    bookmarks = []
    notes = []
    highlights = []

    dic['highlights'] = len(highlights)
    dic['bookmarks'] = len(bookmarks)
    dic['notes'] = len(notes)

    return dic


def get_slideshow_engagement_values(courseID, chapter, content, startDate, endDate, miic_user, measurements):

    dic = {}

    dic['views'] = 0
    dic['totalTimeSpent'] = 0
    dic['bookmarks'] = 0
    dic['notes'] = 0
    dic['totalDuration'] = 0

    slideChangeds = getRelevant(measurements['slideChM'], 'content', [content])
    data = None



    if len(slideChangeds) > 0:

        slidePlays = getRelevant(measurements['slidePlayM'], 'content', [content])

        slideCompleted = getRelevant(measurements['slideCompM'], 'content', [content])

        naviMeasurements = measurements['navM']

        windowLayouts = measurements['winM']

        lifeCycles = measurements['lifeM']


        data = measurements_storyline.get_measurements({miic_user: slidePlays},  {miic_user: naviMeasurements},
                                                    {miic_user: slideChangeds}, {miic_user: slideCompleted}, {miic_user: windowLayouts},
                                                    {miic_user: lifeCycles})


    #print "chapter %s" % chapter
    #print "user................ %s" % miic_user.user.last_name
    #print "data %s" % data

    if data is not None and data != -1 and len(data) > 0:

        # get the total duration of storyline
        for slide in data["views"]: # each slide is a dictionary with info of this slide
            dic['totalDuration'] += slide["duration"]


        dic['totalTimeSpent'] = data['totalTimeSpent']


        views = 0

        for view in data['views']:
            if view['timeSpent'] > 2 + view['timesViewed'] * 0.2:
                views = views + view['timesViewed']

        dic['views'] = views
    else:
        return None


    bookmarks = []
    notes = []
    dic['bookmarks'] = len(bookmarks)
    dic['notes'] = len(notes)

    return dic


def get_video_engagement_values(courseID, chapter, content, startDate, endDate, miic_user, measurements):

    dic = {}
    dic['views'] = 0
    dic['totalTimeSpent'] = 0
    dic['bookmarks'] = 0
    dic['notes'] = 0
    dic['duration'] = 0



    videoMeasurements = getRelevant(measurements['videoM'], 'video', [content])

    data = None


    if len(videoMeasurements) > 0:


        naviMeasurements = measurements['navM']

        windowLayouts = measurements['winM']

        lifeCycles = measurements['lifeM']


        data = measurements_video.get_measurements({miic_user: videoMeasurements}, {miic_user: naviMeasurements},
                                                   {miic_user: windowLayouts}, {miic_user: lifeCycles})



    if data is not None and data != -1 and len(data) > 0:
        # #:
        # print "totalTimeSpent for user %s" % data['totalTimeSpent']
        dic['duration'] = data["duration"]

        dic['totalTimeSpent'] = data['totalTimeSpent']

        views = 0

        for view in data['views']:
                views = views + view['timesViewed']

        dic['views'] = views
    else:
        return None


    bookmarks = []
    notes = []
    dic['bookmarks'] = len(bookmarks)
    dic['notes'] = len(notes)

    return dic


def get_pdf_engagement_values(courseID, chapter, content, startDate, endDate, miic_user, measurements):

    dic = {}
    dic['totalTimeSpent'] = 0
    dic['views'] = 0
    dic['bookmarks'] = 0
    dic['notes'] = 0
    dic['numPages'] = 0

    pdfMeasurements = getRelevant(measurements['pdfM'], 'content', [content])

    data = None


    if len(pdfMeasurements) > 0:


        naviMeasurements = measurements['navM']

        windowLayouts = measurements['winM']

        lifeCycles = measurements['lifeM']


        data, stuff = measurements_pdf.get_measurements({miic_user: pdfMeasurements}, {miic_user: naviMeasurements},
                                                   {miic_user: windowLayouts}, {miic_user: lifeCycles})


    if data is not None and data != -1 and len(data) > 0:

        dic['numPages'] = data["numPages"]

        dic['totalTimeSpent'] = data['totalTimeSpent']

        views = 0
        for view in data['views']:
            if view['timeSpent'] > 2:
                views = views + view['timesViewed']

        dic['views'] = views
    else:
        return None

    bookmarks = []
    notes = []

    dic['bookmarks'] = len(bookmarks)
    dic['notes'] = len(notes)

    return dic
