__author__ = 'dacao'

from measurements import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
import json
import csv
import datetime
import unicodedata
import codecs

import functions as func
import define_attributes_Vel1
import get_Vel1_engagement_data

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


raw_fileName = 'raw_clickstream/measurements-velocity-chess-vel_1.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])

chap_content = {

    'VEL_1_CHAPTER0':[['VEL_1_CONTENT0'], ['pdf']],
    'VEL_1_CHAPTER1':[['VEL_1_CONTENT1'], ['epub']],
    'VEL_1_CHAPTER2':[['VEL_1_CONTENT2'], ['storyline']],
    'VEL_1_CHAPTER3':[['VEL_1_CONTENT3'], ['storyline']],
    'VEL_1_CHAPTER4':[['VEL_1_CONTENT4'], ['storyline']],
    'VEL_1_CHAPTER5':[['VEL_1_CONTENT5'], ['storyline']],
    'VEL_1_CHAPTER6':[['VEL_1_CONTENT6'], ['epud']]
}

clean_events = []
for evt in events:
    if 'chapter' in evt:
        if 'content' in evt and evt['content'] not in chap_content[ evt['chapter'] ][0]:
            continue
        if 'article' in evt and evt['article'] not in chap_content[ evt['chapter'] ][0]:
            continue
    clean_events.append(evt)

events = clean_events
events = sorted(events, key=lambda k: k['timestamp'])
del clean_events

content_list = []
for evt in events:
    if 'content' in evt and evt['content'] not in content_list:
        content_list.append(evt['content'])
    if 'article' in evt and evt['article'] not in content_list:
        content_list.append(evt['article'])
content_list = sorted(content_list)



chapters = chap_content.keys()
chap_questions = {}
for c in chapters:
    chap_questions[c] = []
for evt in events:
    if 'question' in evt and 'chapter' in evt:
        c = evt['chapter']
        q = evt['question']
        if q not in chap_questions[c]:
            chap_questions[c].append(q)
for c in chap_questions:
    chap_questions[c].sort()

# for c in chap_questions:
#     print('%s : %s' %(c, chap_questions[c]))


uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
users = sorted(uv.keys())




''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, existingModes, dates = define_attributes_Vel1.get(events, chap_questions)


'''/////////////////////////////////////  Get the engagement info ///////////////////////////////////////////////'''
titles, scores = get_Vel1_engagement_data.get()

for title in titles:
    attr.append( 'engagement_'+title )


def filter_dup_Pause(storyLine_evts):
    new = []
    ref = -1
    for evt in storyLine_evts:
        add = True
        if 'm_type' in evt and evt['m_type'] == m_slide:
            if 'type' in evt and evt['type'] == 'Pause':
                t = evt['timestamp']
                if t-ref < 0.2 and t-ref > -0.2: # duplicate event found
                    print('dup pause  %s  %s' %(evt['chapter'], evt['user']) )
                    add = False
                else:
                    ref = t
        if add:
            new.append(evt)
    return new





def init_user_file_timespent_dict(users, content_list):

    user_file_timespent = {}
    for user in users:
        user_file_timespent[user] = {}
        for content in content_list:
            user_file_timespent[user][content] = 0

    return user_file_timespent


def get_content_id_from_events(storyline_events):
    contentIDs = []
    for evt in storyline_events:
        if 'content' in evt and evt['content'] not in contentIDs:
            contentIDs.append( evt['content'] )

    if len(contentIDs) == 1:
        return contentIDs[0]
    else:
        return False


def write_user_file_timespent_matrix_to_csv(user_file_timespent, content_list):

    nameSTR = 'user_file_timespent.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + content_list)

    for user in user_file_timespent:
        tmp = [user]
        for content_id in content_list:
            tmp.append(  user_file_timespent[user][content_id]  )

        wr.writerow(tmp)

    fileName.close()
    print('user_file_timespent csv file done!! \n')
    return 0






# todo: main
def main():

    nameSTR = 'matrix_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)


    user_file_timespent = init_user_file_timespent_dict(users, content_list)

    for user in users:

        attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}}
        for d in dates:
            attr_thisUser['login'][d] = 0

        all_evts = uv[user]
        attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        for evt in all_lc:
            readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
            y = unicode(readTime.year)
            m = unicode(readTime.month)
            day = unicode(readTime.day)
            y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
            m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
            day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
            date_str = y+'_'+m+'_'+day
            attr_thisUser['login'][date_str] = 1

        all_lc = {user: all_lc}

        for ch in chap_names:
            if ch not in chapters:
                continue

            if ch not in attr_thisUser:
                attr_thisUser[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)

            attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
            attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
            attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode
            window_evts = {user: window_evts}


            if m_article in existing_types:
                art_evts = func.get_evt_from_list(evts_thisChap, m_article)
                numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
                art_evts = {user: art_evts}

                result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
                art_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo["epub"]['numMax']
                numMinWindow = windowInfo["epub"]['numMin']

                attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                                'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                                'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up }

                content_id = result['article']
                user_file_timespent[user][content_id] = art_timeSpent

            if m_pdf in existing_types:

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)

                numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
                pdf_evts = {user: pdf_evts}
                result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = result['averageCompletionRate']
                pdf_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['pdf']["numMax"]
                numMinWindow = windowInfo['pdf']["numMin"]

                attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                    numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up, 'numScroll_down':numScroll_down}

                content_id = result['pdf']
                user_file_timespent[user][content_id] = pdf_timeSpent

            if m_slide in existing_types:

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
                storyLine_evts = filter_dup_Pause(storyLine_evts)

                content_id = get_content_id_from_events(storyLine_evts)

                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
                                                                 window_evts, all_lc)
                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['storyline']['numMax']
                numMinWindow = windowInfo['storyline']['numMin']

                numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

                attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                                  'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                                  'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow}

                user_file_timespent[user][content_id] = storyline_timeSpent

            for i in xrange(0, len(titles)):
                if titles[i] == ch:
                    tmp = scores[i]
                    for studentID in tmp:
                        if studentID == user:
                            attr_thisUser[ch]["EngagementScore"] = tmp[studentID]
                            break

                    break

        row = func.attr_dict_to_list(attr, attr_thisUser)

        for tmp in scores:
            if user in tmp:
                row.append(tmp[user])
            else:
                row.append(0)

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!! \n')


    print('attributes are:   ')
    for i in xrange(0, len(attr)):
        print(attr[i])


    write_user_file_timespent_matrix_to_csv(user_file_timespent, content_list)

    return 0

def get_slides_total_duration(events):
    storyline_events = func.get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output

########################################################################################################################



def get_users_paths(all_nav, user, all_path):

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-1] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_path[user]= path


def user_path_dictionary():

    # nameSTR = 'users_and_paths_Vel1.csv'
    # fileName = open(nameSTR, 'wb')
    # wr = csv.writer( fileName, dialect= 'excel')
    # wr.writerow(["chapters/freq"] + allChapNums)

    all_paths = {}

    for user in users:
        all_evts = uv[user]
        # chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        # all_lc = {user: all_lc}
        get_users_paths(func.get_evt_from_list(all_evts, m_navi), user, all_paths)

    for user in all_paths:
        print('%s:   %s' %(user, all_paths[user]))


    #wr.writerow(row)
    # fileName.close()
    print('transition_freq_matrix.csv done!!')

    return 0


def quiz_matrix():

    user_questionEvents = {}
    for u in users:
        user_questionEvents[u] = []
    for evt in events:
        if 'question' in evt and 'chapter' in evt:
            u = evt['user']
            user_questionEvents[u].append(evt)


    # define attributes
    attr = []
    for ch in sorted( chap_questions.keys() ):
        for q in sorted( chap_questions[ch] ):
            attr.append('%s_%s'  %(ch, q))

    all_num_attempts_to_correct = []
    all_num_of_answering = []

    for user in users:

        this_user_attributes = []
        this_num_of_answering = []

        for ch in sorted( chap_questions.keys() ):
            for q in sorted( chap_questions[ch] ):

                quiz_events = []
                for evt in user_questionEvents[user]:
                    if evt['question'] == q and evt['chapter'] == ch:
                        quiz_events.append(evt)
                quiz_events = sorted(quiz_events, key=lambda k: k['timestamp'])

                num_attemps_to_correct = 1
                for i, quiz_evt in enumerate(quiz_events):
                    if quiz_evt['score'] != 10:
                        num_attemps_to_correct += 1
                    else:
                        break

                    if i == len(quiz_events) - 1  and quiz_evt['score'] != 10:
                        num_attemps_to_correct = -1

                if len(quiz_events) == 0:
                    num_attemps_to_correct = 0

                this_user_attributes.append(num_attemps_to_correct)
                this_num_of_answering.append( len(quiz_events) )

        all_num_attempts_to_correct.append(this_user_attributes)
        all_num_of_answering.append(this_num_of_answering)


    nameSTR = 'num_attempts_to_correct_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow( ['user/quiz'] + attr )
    for i, user in enumerate(users):
        row = [user] + all_num_attempts_to_correct[i]
        wr.writerow(row)
    fileName.close()
    print('%s done!!' %nameSTR)

    nameSTR = 'num_of_answering_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow( ['user/quiz'] + attr )
    for i, user in enumerate(users):
        row = [user] + all_num_of_answering[i]
        wr.writerow(row)
    fileName.close()
    print('%s done!!' %nameSTR)


    return 0



quiz_matrix()
user_path_dictionary()
main()


