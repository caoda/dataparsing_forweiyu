
import json



def get():

    raw = json.load(open("engmt_data_from_dashboard/MAH103_engagement_data_2015_11_17.txt"))
    engmt_overall = {}

    title = []
    score = []


    for line in raw['students']:
        if line['id'] not in engmt_overall:
            engmt_overall[ line['id'] ] = line['engagement']['average']

    print('\n\n')


    title.append(raw['id'])
    tmp = {}
    for student in raw['students']:
        tmp[student['id']] = student['engagement']['average']
    score.append(tmp)

    for child in raw['children']:
        title.append(child['id'])
        tmp = {}
        for student in child['students']:
            tmp[student['id']] = student['engagement']['average']
        score.append(tmp)


        for grand_child in child['children']:
            title.append(grand_child['id'])
            tmp = {}
            for student in grand_child['students']:
                tmp[student['id']] = student['engagement']['average']
            score.append(tmp)



    return title, score


