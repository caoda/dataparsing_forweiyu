__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np


from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
#from measurements import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
from pdf_words_in_courses import *
import absolute_engagement_func as AB_eng

import functions as func
import define_attributes
import get_WLC305_engmt_data

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'

alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017


raw_fileName = 'raw_clickstream/measurements-pbp-pbp_wlc_305_2.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])
courseID = func.get_course_id(events)

# 2015_10_5    1444070075
# 2015_10_1    1443704921

timestamp_filter = 1443704921
new = []
for evt in events:
    if evt['timestamp'] < timestamp_filter:
        new.append(evt)
events = new
del new



''' get the storyline contentIDs '''
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}



chap_content = {

    'PBP_WLC_305_2_CHAPTER00':[['PBP_WLC_305_2_CONTENT00'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER01':[['PBP_WLC_305_2_CONTENT10'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER02':[['PBP_WLC_305_2_CONTENT20'], ['epud']],
    'PBP_WLC_305_2_CHAPTER03':[['PBP_WLC_305_2_CONTENT30'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER04':[['PBP_WLC_305_2_CONTENT40'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER05':[['PBP_WLC_305_2_CONTENT50'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER06':[['PBP_WLC_305_2_CONTENT60'], ['epud']],
    'PBP_WLC_305_2_CHAPTER07':[['PBP_WLC_305_2_CONTENT70'], ['epud']],
    'PBP_WLC_305_2_CHAPTER08':[['PBP_WLC_305_2_CONTENT80', 'PBP_WLC_305_2_CONTENT81'], ['pdf', 'epud']],
    'PBP_WLC_305_2_CHAPTER09':[['PBP_WLC_305_2_CONTENT90'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER10':[['PBP_WLC_305_2_CONTENT100'], ['epud']],
    'PBP_WLC_305_2_CHAPTER11':[['PBP_WLC_305_2_CONTENT110'], ['epud']]
}

clean_events = []
for evt in events:

    if 'chapter' in evt:

        if 'content' in evt and evt['content'] not in chap_content[ evt['chapter'] ][0]:
            continue
        if 'article' in evt and evt['article'] not in chap_content[ evt['chapter'] ][0]:
            continue

    clean_events.append(evt)

events = clean_events
del clean_events







''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, existingModes, dates = define_attributes.get(events)


'''/////////////////////////////////////  Get the engagement info ///////////////////////////////////////////////'''
titles, scores = get_WLC305_engmt_data.get()
for title in titles:
    print(title)
    attr.append( 'engagement_'+title )


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

fname = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_2-from-2015-09-01-to-2015-12-18.csv'
csvfile = open(fname, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), fname))



passInfo = []

with open("pass_info/wlc305_pass_extend_info.csv") as f:
    content = f.readlines()
    content = content[0].split('\r')

    for row in content:
        line = row.split(',')
        passInfo.append(line)

print("there are %s users in wlc305_pass_extend_info.csv" %len(passInfo))


def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            email = line[2]

            for row in passInfo:
                if row[0] == email:
                    if 'PASS' in row:
                        return 1

    return 0



def get_extend(userID):
    #print("\n user: %s " %userID)
    for line in userInfo:
        if userID == line[3]:

            email = line[2]

            found = False

            for row in passInfo:
                if row[0] == email:

                    found = True
                    row.append("found")

                    if 'Extend' in row:

                        return 1

            if not found:
                print("not found in new.csv")
                #print(email)
    return 0
'''//////////////////////////////////  Finish getting the pass/extend info /////////////////////////////////////'''


def filter_dup_Pause(storyLine_evts):
    new = []
    ref = -1
    for evt in storyLine_evts:
        add = True
        if 'm_type' in evt and evt['m_type'] == m_slide:
            if 'type' in evt and evt['type'] == 'Pause':
                t = evt['timestamp']
                if t-ref < 0.2 and t-ref > -0.2: # duplicate event found
                    print('dup pause  %s  %s' %(evt['chapter'], evt['user']) )
                    add = False
                else:
                    ref = t
        if add:
            new.append(evt)
    return new



all_paths = []

# todo: main
def main():

    nameSTR = 'matrix_WLC305_2015_10_01.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in users:

        attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}, 'pass':get_pass(user), 'extend':get_extend(user)}
        for d in dates:
            attr_thisUser['login'][d] = 0

        all_evts = uv[user]
        attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        for evt in all_lc:
            readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
            y = unicode(readTime.year)
            m = unicode(readTime.month)
            day = unicode(readTime.day)
            y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
            m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
            day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
            date_str = y+'_'+m+'_'+day
            attr_thisUser['login'][date_str] = 1

        all_lc = {user: all_lc}

        attr_thisUser["sequence132"] = get_seq132(func.get_evt_from_list(all_evts, m_navi), user)


        for ch in chap_names:
            if ch not in chapters:
                continue

            if ch not in attr_thisUser:
                attr_thisUser[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)

            # todo: get number of bookmarks, notes and highlights from evts_thisChap ??? need to ask
            attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
            attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
            attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode
            window_evts = {user: window_evts}


            if m_article in existing_types:

                art_evts = func.get_evt_from_list(evts_thisChap, m_article)

                numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
                art_evts = {user: art_evts}

                result, hm_all_tSpent = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                article_id = result['article']
                articleLength = result['articleLength']
                art_compRate = result['averageCompletionRate']
                art_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo["epub"]['numMax']
                numMinWindow = windowInfo["epub"]['numMin']

                attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                                'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                                'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up }

                this_user_article = AB_eng.UserArticle(alpha_mode, user, ch, article_id, articleLength, hm_all_tSpent)
                eng = this_user_article.get_engagement_score()



            if m_pdf in existing_types:

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)

                numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
                pdf_evts = {user: pdf_evts}


                numMaxWindow = windowInfo['pdf']["numMax"]
                numMinWindow = windowInfo['pdf']["numMin"]


                overall_result, result = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = overall_result['averageCompletionRate']
                pdf_timeSpent = overall_result['totalTimeSpent']
                pdf_id = overall_result['pdf']
                numPages = overall_result['numPages']


                fname = 'pdf_words_in_courses/%s_all_PDFs_words.json'%courseID
                in_file = open(fname,"r")
                words_in_PDFs_of_a_course = json.load(in_file) # words_in_PDFs_of_a_course[contentID][page_name] = [all words in the page]
                in_file.close()


                wordCounts_in_PDFs_of_a_course = {}
                for pageName in words_in_PDFs_of_a_course[pdf_id]:
                    a = pageName.index('[')
                    b = pageName.index(']')
                    pageNumber = int(pageName[a+1:b])
                    wordCounts_in_PDFs_of_a_course[pageNumber] = len( words_in_PDFs_of_a_course[pdf_id][pageName] )


                heatmap = []
                expected_timeSpent_on_pages = []
                for p in xrange(1, numPages+1):
                    if p not in result[user]:
                        continue
                    heatmap.append(  result[user][p]['timeSpent'])
                    expected_timeSpent = wordCounts_in_PDFs_of_a_course[p-1] / 3.0
                    if expected_timeSpent == 0:
                        expected_timeSpent = 1
                    expected_timeSpent_on_pages.append(expected_timeSpent)


                for i in xrange(0, len(heatmap)):
                    if heatmap[i] > pdf_timeSpent_cap_on_page:
                        heatmap[i] = pdf_timeSpent_cap_on_page


                this_user_pdf = AB_eng.UserPdf(alpha_mode, user, ch, pdf_id, heatmap, expected_timeSpent_on_pages)
                eng = this_user_pdf.get_engagement_score()
                print(eng)


                attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                    numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up, 'numScroll_down':numScroll_down}



            if m_slide in existing_types:

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
                contentID = storyLine_evts[0]['content']
                storyLine_evts = filter_dup_Pause(storyLine_evts)

                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                slide_infos = dict_storylineID_slideInfos[contentID]['slide_infos']
                slide_durations_list = dict_storylineID_slideInfos[contentID]['slide_durations_list']

                result, matrices_dic = Measurements_storyline.get_measurements(slideChanged, slideComp, storyLine_evts,
                                                       slide_infos, nav_evts, all_lc, {},
                                                       window_evts, end_timestamp)

                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['storyline']['numMax']
                numMinWindow = windowInfo['storyline']['numMin']
                numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

                time_spent_matrix = matrices_dic['time_spent_matrix']
                time_spent_matrix = np.asarray(time_spent_matrix).ravel().tolist()
                expected_timeSpent_on_slides = slide_durations_list

                this_user_storyline = AB_eng.UserStoryline(alpha_mode, user, ch, contentID, time_spent_matrix, expected_timeSpent_on_slides)
                eng = this_user_storyline.get_engagement_score()
                print(eng)

                attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                                  'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                                  'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow}





            for i in xrange(0, len(titles)):
                if titles[i] == ch:
                    tmp = scores[i]
                    for studentID in tmp:
                        if studentID == user:
                            attr_thisUser[ch]["EngagementScore"] = tmp[studentID]
                            break

                    break


        row = func.attr_dict_to_list(attr, attr_thisUser)

        for tmp in scores:
            if user in tmp:
                row.append(tmp[user])
            else:
                row.append(0)

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!!')

    for i in xrange(0, len(attr)):
        print(attr[i])



    return 0





def get_seq132( all_nav , user):

    num = 0

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-2:] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_paths.append(path)

    if len(path) < 3:
        return 0
    else:
        for i in xrange(0,len(path)-2):
            if path[i] == 1 and path[i] == 2 and path[i] == 3:
                num += 1

    return num


def get_slides_total_duration(events):
    storyline_events = func.get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output

########################################################################################################################



main()









# # get transition freq matrix
# nameSTR = 'transition_freq_matrix_WLC305.csv'
# fileName = open(nameSTR, 'wb')
# wr = csv.writer( fileName, dialect= 'excel')
# wr.writerow(["chapters/freq"] + allChapNums)
#
# # initialize trans_dict
# trans_dict = {}
# for i in allChapNums:
#     trans_dict[i] = {}
#     for j in allChapNums:
#         trans_dict[i][j] = 0
#
# # fill in trans_dict
# for path in all_paths:
#     for i in xrange(0, len(path)-1):
#         origin = path[i]
#         destin = path[i+1]
#         trans_dict[origin][destin] += 1
#
# # fill in the csv.
# for origin in allChapNums:
#     row = []
#
#     destinations = sorted( trans_dict[origin].keys() )
#     for dest in destinations:
#         row.append(trans_dict[origin][dest])
#
#     row = [origin] + row
#
#     wr.writerow(row)
#
# fileName.close()
# print('transition_freq_matrix.csv done!!')





# # write events to csv
# nameSTR = 'events_matrix_WLC305.csv'
# fileName = open(nameSTR, 'wb')
# wr = csv.writer( fileName, dialect= 'excel')
# wr.writerow(['course', 'session', 'user', 'm_type', 'action', 'timestamp', 'ID'])
# for evt in events:
#
#     row = []
#     row.append(evt['course'])
#     row.append(evt['session'])
#     row.append(evt['user'])
#     row.append(evt['m_type'])
#     if 'action' in evt:
#         row.append(evt['action'])
#     elif 'type' in evt:
#         row.append(evt['type'])
#     else:
#         row.append("none")
#     row.append(evt['timestamp'])
#     row.append(evt['id'])
#
#     wr.writerow(row)
#
# fileName.close()
# print('events_matrix.csv done!!')

