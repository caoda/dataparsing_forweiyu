
import json
import numpy
import csv

import get_MAH102_engmt_data
import get_MAH103_engmt_data
import get_WLC305_engmt_data


def get(fileName):

    raw = json.load(open(fileName))
    engmt_overall = {}

    title = []
    score = []


    for line in raw['students']:
        if line['id'] not in engmt_overall:
            engmt_overall[ line['id'] ] = line['engagement']['average']

    print('\n\n')


    title.append(raw['id'])
    tmp = {}
    for student in raw['students']:
        tmp[student['id']] = student['engagement']['average']
    score.append(tmp)

    for child in raw['children']:
        title.append(child['id'])
        tmp = {}
        for student in child['students']:
            tmp[student['id']] = student['engagement']['average']
        score.append(tmp)


        for grand_child in child['children']:
            title.append(grand_child['id'])
            tmp = {}
            for student in grand_child['students']:
                tmp[student['id']] = student['engagement']['average']
            score.append(tmp)



    quantiled_scores = []
    for i in xrange(0, len(score)):
        target = score[i].values()

        # get the threshold for top 25% of the scores
        thresh = numpy.percentile(numpy.array(target), 75)

        for j in xrange(0, len(target)):
            if target[j] >= thresh:
                target[j] = 100.0
            else:
                target[j] = (target[j]/float(thresh)) * 100.0

        quantiled_scores.append(target)

    return title, score, quantiled_scores



def main(inFile, outFile):
    if inFile == fileName102:
        title, score = get_MAH102_engmt_data.get()
    elif inFile == fileName103:
        title, score = get_MAH103_engmt_data.get()
    elif inFile == fileName305:
        title, score = get_WLC305_engmt_data.get()

    fileName = open(outFile, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')

    for i in xrange(0, len(title)):
        tmp = []
        tmp.append(title[i])
        for j in xrange(0, len(score[i])):
            tmp.append(score[i][j])

        wr.writerow( tmp )

    fileName.close()

    return 0



fileName102 = "engmt_data_from_dashboard/MAH102_engagement_data_new_method.txt"
fileName103 = "engmt_data_from_dashboard/MAH103_engagement_data_new_method.txt"
#fileName302 = "engmt_data_from_dashboard/WLC302_engagement_data_new_method.txt"
#fileName303 = "engmt_data_from_dashboard/WLC303_engagement_data_new_method.txt"
fileName305 = "engmt_data_from_dashboard/WLC305_engagement_data_new_method.txt"

output102 = 'new_engmt_scores_mah102.csv'
output103 = 'new_engmt_scores_mah103.csv'
# output302 = 'quantiled_engmt_scores_wlc302.csv'
#output303 = 'new_engmt_scores_wlc303.csv'
output305 = 'new_engmt_scores_wlc305.csv'

main(fileName102, output102)
main(fileName103, output103)
# main(fileName303, output303)
main(fileName305, output305)



