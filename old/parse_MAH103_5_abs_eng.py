__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

import functions as func
import define_attributes_new_with_abs_engagement
import PBP_feature_parsing_for_one_user_abs_eng as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017
timestamp_filter = end_timestamp
course_name = 'MAH_103'
session_number = 5
course_name_to_remove_in_feature_name = 'PBP_MAH_103_5_CHAPTER'


''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_mah_103_5.json'
users_from_reporting_tool = "first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_103_5-from-2015-10-01-to-2016-03-16.csv"
pass_fail_extend_info = "pass_info/MAH103_Pass.txt"

''' file name for output matrix '''
output_file_name = 'new_feature_matrix_with_abs_eng/matrix_MAH103_5_new.csv'



events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])
courseID = func.get_course_id(events)

change_chapter_names = {
'PBP_MAH_103_5_CHAPTER00' : 'PBP_MAH_103_5_CHAPTER00',
'PBP_MAH_103_5_CHAPTER01' : 'PBP_MAH_103_5_CHAPTER01',
'PBP_MAH_103_5_CHAPTER02' : 'PBP_MAH_103_5_CHAPTER02',
'PBP_MAH_103_5_CHAPTER03' : 'PBP_MAH_103_5_CHAPTER03',
'PBP_MAH_103_5_CHAPTER04' : 'PBP_MAH_103_5_CHAPTER04',
'PBP_MAH_103_5_CHAPTER05' : 'delete',
'PBP_MAH_103_5_CHAPTER06' : 'PBP_MAH_103_5_CHAPTER05',
'PBP_MAH_103_5_CHAPTER07' : 'PBP_MAH_103_5_CHAPTER06',
'PBP_MAH_103_5_CHAPTER08' : 'PBP_MAH_103_5_CHAPTER07',
'PBP_MAH_103_5_CHAPTER09' : 'PBP_MAH_103_5_CHAPTER08',
'PBP_MAH_103_5_CHAPTER10' : 'delete',
'PBP_MAH_103_5_CHAPTER11' : 'PBP_MAH_103_5_CHAPTER09',
'PBP_MAH_103_5_CHAPTER12' : 'PBP_MAH_103_5_CHAPTER10' }

for evt in events:
    if 'chapter' in evt:
        evt['chapter'] = change_chapter_names[  evt['chapter']  ]

tmp = []
for evt in events:
    if 'chapter' in evt and evt['chapter'] == 'delete':
        continue

    else:
        tmp.append(evt)

events = tmp




# 2015_10_5    1444070075
# 2015_10_1    1443704921
events = func.filter_with_timestamp(events, timestamp_filter)


''' get the storyline contentIDs '''
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}


''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, dates, chap_existing_modes = define_attributes_new_with_abs_engagement.get(events)


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

csvfile = open(users_from_reporting_tool, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), users_from_reporting_tool))

passInfo = []

with open(pass_fail_extend_info) as f:
    content = f.readlines()
    content = content[0].split('\r')
    content.pop(0)

    for row in content:
        line = row.split('\t')
        passInfo.append(line)
        print(line)

print("there are %s users in %s" %(len(passInfo), pass_fail_extend_info))


def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                print(row)
                if row[1] == family_name and row[0] == given_name:
                    if 'PASS' in row:
                        return 1

    return 0


def get_NoPass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'NO PASS' in row:
                        return 1

    return 0


'''//////////////////////////////////  the pass/extend info ///////////////////////////////////////////'''




def remove_course_name_from_chapterIDs(attr):
    feature_names = []
    for name in attr:
        if len(name) > 21 and name[0:21] == course_name_to_remove_in_feature_name:
            tmp = name[14:]
            feature_names.append(tmp)
        else:
            feature_names.append(name)

    return feature_names


def main():

    nameSTR = output_file_name
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    feature_names = remove_course_name_from_chapterIDs(attr)
    wr.writerow(["user"] + feature_names)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in users:

        user_pass = get_pass(user)
        user_nopass = get_NoPass(user)
        user_extend = 0

        row = parsing.core(user, uv, chap_names, courseID, attr,
                                        dict_storylineID_slideInfos, userInfo, user_pass, user_nopass, user_extend, course_name, session_number)

        if row == -1:
            continue

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!!')

    for i in xrange(0, len(feature_names)):
        print(feature_names[i])

    return 0




main()













