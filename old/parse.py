__author__ = 'dacao'

import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
import json
import csv
import datetime
import unicodedata
import codecs

import functions as func
import define_attributes

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


#events = json.load(open("measurements-pbp-pbp_wlc_305_2.json"))
#events = json.load(open("JSON_DUMP_WLC305_Oct22.txt"))
#events = json.load(open("measurements-pbp-pbp_mah_102_4.txt"))
events = json.load(open("measurements-pbp-pbp_wlc_303_4.json"))
events = events['measurements']

# timestamp_filter = 1443484800.0
# events = sorted(events, key=lambda k: k['timestamp'])
# tmp = []
# for event in events:
#     if event['timestamp'] >= timestamp_filter:
#         tmp.append(event)
# events = tmp




''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, all_paths, existingModes, dates = define_attributes.get(events)


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

fname = "first-time-users-pbp-pbp_mah_102_4-from-2015-09-01-to-2015-12-03.csv"
csvfile = open(fname, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

# fname = "users.txt"
# with open(fname) as f:
#     content = f.readlines()
#
#     userInfo = []
#     for row in content:
#         line = row.split()
#         userInfo.append(line)
#         if line[0] != line[1]:
#             continue
#         ID = line[0]


print("there are %s users in first-time-users-pbp-pbp_mah_102_4-from-2015-09-01-to-2015-12-03.csv" %len(userInfo))

passInfo = []

with open("MAH102_Pass.txt") as f:
    content = f.readlines()
    content = content[0].split('\r')
    content.pop(0)

    for row in content:
        line = row.split('\t')
        passInfo.append(line)

print("there are %s users in MAH102_Pass.txt" %len(passInfo))

for row in passInfo:
    if row[1] not in familyNames or row[0] not in givenNames:
        print("%s , %s" %(row[1], row[0]))



# def get_pass(userID):
#     for line in userInfo:
#         if userID == line[0]:
#             email = line[-1]
#
#             for row in passInfo:
#                 if row[0] == email:
#                     if 'PASS' in row:
#                         return 1
#
#     return 0

def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'PASS' in row:
                        return 1

    return 0


def get_NoPass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'NO PASS' in row:
                        return 1

    return 0

def get_extend(userID):
    #print("\n user: %s " %userID)
    for line in userInfo:
        if userID == line[0]:

            email = line[-1]

            found = False

            for row in passInfo:
                if row[0] == email:

                    found = True
                    row.append("found")

                    if 'Extend' in row:

                        return 1

            if not found:
                print("not found in new.csv")
                #print(email)
    return 0
'''//////////////////////////////////  Finish getting the pass/extend info /////////////////////////////////////'''


def get_engagements(uv):
    timeSpent_info = {}
    weights = {}
    max_ts = {}

    storylineLens = get_slides_total_duration(events)

    for user in uv:
        timeSpent_info[user] = {}

        all_evts = uv[user]
        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        all_lc = {user: all_lc}

        for ch in chap_names:
            if ch not in max_ts:
                max_ts[ch] = {}

            if ch not in chapters:
                continue

            timeSpent_info[user][ch] = {}
            if ch not in weights:
                weights[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)
            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            window_evts = {user: window_evts}

            if m_article in existing_types:
                if 'article' not in max_ts[ch]:
                    max_ts[ch]['article'] = 0

                art_evts = func.get_evt_from_list(evts_thisChap, m_article)
                weight = (art_evts[0]["totalCharacters"] / 200.0)  # get the weight for computing engagement
                weights[ch]['article'] = weight
                art_evts = {user: art_evts}

                result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
                art_timeSpent = result['totalTimeSpent']

                timeSpent_info[user][ch]['article'] = art_timeSpent
                if art_timeSpent > max_ts[ch]['article']:
                    max_ts[ch]['article'] = art_timeSpent


            if m_pdf in existing_types:
                if 'pdf' not in max_ts[ch]:
                    max_ts[ch]['pdf'] = 0

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)
                weight = pdf_evts[0]["totalPages"]  # get the weight for computing engagement
                weights[ch]['pdf'] = weight

                pdf_evts = {user: pdf_evts}
                result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = result['averageCompletionRate']
                pdf_timeSpent = result['totalTimeSpent']

                timeSpent_info[user][ch]['pdf'] = pdf_timeSpent
                if pdf_timeSpent > max_ts[ch]['pdf']:
                    max_ts[ch]['pdf'] = pdf_timeSpent

            if m_slide in existing_types:
                if 'storyline' not in max_ts[ch]:
                    max_ts[ch]['storyline'] = 0

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
                weights[ch]['storyline'] = storylineLens[ch]
                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
                                                                 window_evts, all_lc)
                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']

                timeSpent_info[user][ch]['storyline'] = storyline_timeSpent
                if storyline_timeSpent > max_ts[ch]['storyline']:
                    max_ts[ch]['storyline'] = storyline_timeSpent

    # averaged over the max from all users ( for each mode )
    for user in timeSpent_info:
        for ch in timeSpent_info[user]:
            for mode in timeSpent_info[user][ch]:
                if float(max_ts[ch][mode]) != 0:
                    timeSpent_info[user][ch][mode] = 100 * (timeSpent_info[user][ch][mode] / float(max_ts[ch][mode]))

    engagements = {}
    for user in timeSpent_info:
        engagements[user] = {}
        for ch in timeSpent_info[user]:
            eng = 0
            divider = 0

            for mode in timeSpent_info[user][ch]:
                eng += timeSpent_info[user][ch][mode] * weights[ch][mode]
                divider += 1

            if divider == 0:
                engagements[user][ch] = 0
            else:
                engagements[user][ch] = eng/float(divider)



    return  engagements


# todo: main
def main():

    nameSTR = 'matrix_WLC303.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)

    uv = func.get_uv_dict(events)

    engagements = get_engagements(uv)

    users = sorted(uv.keys())

    for user in users:

        attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}, 'pass':get_pass(user), 'no_pass':get_NoPass(user)}
        for d in dates:
            attr_thisUser['login'][d] = 0

        all_evts = uv[user]
        attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        for evt in all_lc:
            readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
            y = unicode(readTime.year)
            m = unicode(readTime.month)
            day = unicode(readTime.day)
            y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
            m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
            day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
            date_str = y+'_'+m+'_'+day
            attr_thisUser['login'][date_str] = 1

        all_lc = {user: all_lc}

        attr_thisUser["sequence132"] = get_seq132( func.get_evt_from_list(all_evts, m_navi)  ,  user)

        for ch in chap_names:
            if ch not in chapters:
                continue

            if ch not in attr_thisUser:
                attr_thisUser[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)

            # todo: get number of bookmarks, notes and highlights from evts_thisChap ??? need to ask
            attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
            attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
            attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode
            window_evts = {user: window_evts}

            # if ch == 'PBP_WLC_305_2_CHAPTER06':
            #     print("user %s lifecycle events are:  %s" %(user, all_lc[user]))


            #todo: to delete
            shit_user = 'a5e96cd5-6bee-4cd9-9db0-72d878fdb52c'
            shit_chap = 'PBP_WLC_303_4_CHAPTER05'
            #todo: to delete


            if m_article in existing_types:

                art_evts = func.get_evt_from_list(evts_thisChap, m_article)
                # if ch == shit_chap and user == shit_user:
                #     print('\n\n article events found in %s for user %s ' %(ch, user))
                #     print(art_evts[0:2])
                    # time1 = art_evts[0]['timestamp']
                    # time2 = art_evts[-1]['timestamp']
                    # print(time1)
                    # for lc_evt in all_lc[user]:
                    #     if lc_evt['timestamp'] >= time1 and lc_evt['timestamp'] <= time2:
                    #         #print("action: %s  at  time %s" %(lc_evt['action'], lc_evt['timestamp']))
                    #         print(lc_evt)
                    # print(time2)

                numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
                art_evts = {user: art_evts}

                result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
                art_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo["epub"]['numMax']
                numMinWindow = windowInfo["epub"]['numMin']

                attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                                'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                                'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up }


            if m_pdf in existing_types:

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)
                #
                # if ch == shit_chap and user == shit_user:
                #     print('\n\n PDF events found in %s for user %s ' %(ch, user))
                #     print(pdf_evts)

                numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
                pdf_evts = {user: pdf_evts}
                result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = result['averageCompletionRate']
                pdf_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['pdf']["numMax"]
                numMinWindow = windowInfo['pdf']["numMin"]

                attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                    numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up, 'numScroll_down':numScroll_down}


            if m_slide in existing_types:

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
                if ch == shit_chap: # and user == shit_user:
                    print('\n\n storyline events found in %s for user %s ' %(ch, user))
                    print(storyLine_evts)

                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
                                                                 window_evts, all_lc)
                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['storyline']['numMax']
                numMinWindow = windowInfo['storyline']['numMin']

                numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

                attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                                  'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                                  'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow}

            attr_thisUser[ch]["EngagementScore"] = engagements[user][ch]


        row = attr_dict_to_list(attr_thisUser)

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!!')


    # for i in attr:
    #     print(i)

    return 0





def get_seq132( all_nav , user):

    num = 0

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-2:] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_paths.append(path)

    if len(path) < 3:
        return 0
    else:
        for i in xrange(0,len(path)-2):
            if path[i] == 1 and path[i] == 2 and path[i] == 3:
                num += 1

    return num



def attr_dict_to_list(attr_dict):

    row = []
    for item in attr:
        val = 0 # initiate it to be zero
        x = item.split("$$")
        if len(x) == 3:
            [ch, mode, meas] = x[0:3]
            if ch in attr_dict:
                if mode in attr_dict[ch]:
                    val = attr_dict[ch][mode][meas]
        elif len(x) == 2:
            [ch, meas] = x[0:2]
            if ch in attr_dict:
                val = attr_dict[ch][meas]
        elif len(x) == 1:
            meas = x[0]
            val = attr_dict[meas]
        else:
            print("error with attr:  %s" %item)

        row.append(val)

    return row









def get_slides_total_duration(events):
    storyline_events = func.get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output

########################################################################################################################



main()









# get transition freq matrix
nameSTR = 'transition_freq_matrix_WLC303.csv'
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')
wr.writerow(["chapters/freq"] + allChapNums)

# initialize trans_dict
trans_dict = {}
for i in allChapNums:
    trans_dict[i] = {}
    for j in allChapNums:
        trans_dict[i][j] = 0

# fill in trans_dict
for path in all_paths:
    for i in xrange(0, len(path)-1):
        origin = path[i]
        destin = path[i+1]
        trans_dict[origin][destin] += 1

# fill in the csv.
for origin in allChapNums:
    row = []

    destinations = sorted( trans_dict[origin].keys() )
    for dest in destinations:
        row.append(trans_dict[origin][dest])

    row = [origin] + row

    wr.writerow(row)

fileName.close()
print('transition_freq_matrix.csv done!!')





# write events to csv
nameSTR = 'events_matrix_WLC303.csv'
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')
wr.writerow(['course', 'session', 'user', 'm_type', 'action', 'timestamp', 'ID'])
for evt in events:

    row = []
    row.append(evt['course'])
    row.append(evt['session'])
    row.append(evt['user'])
    row.append(evt['m_type'])
    if 'action' in evt:
        row.append(evt['action'])
    elif 'type' in evt:
        row.append(evt['type'])
    else:
        row.append("none")
    row.append(evt['timestamp'])
    row.append(evt['id'])

    wr.writerow(row)


fileName.close()
print('events_matrix.csv done!!')



# for row in passInfo:
#     if "found" not in row:
#         print(row)