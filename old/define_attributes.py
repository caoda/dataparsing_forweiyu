
import json
import csv
import datetime
import unicodedata
import codecs


import functions as func


m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''

def get(events):
    # get list of all chapters, sorted by name
    # get all dates of the course
    chap_names = []
    dates = []
    for evt in events:
        if 'chapter' in evt and evt['chapter'] not in chap_names:
            chap_names.append(evt['chapter'])

        time = evt['timestamp']
        readTime = datetime.datetime.fromtimestamp(int(time))

        year = str(readTime.year)
        month = str(readTime.month)
        day = str(readTime.day)
        date_str = year + '_' + month + '_' + day

        if date_str not in dates:
            dates.append(date_str)

    chap_names = sorted(chap_names)

    attr = [ 'firstName', 'lastName' , 'email' ]
    for ch in chap_names:

        existingModes = []
        for evt in events:

            if 'chapter' in evt and evt['chapter'] == ch:

                if evt['m_type'] == m_slide and 'storyline' not in existingModes:

                    existingModes.append('storyline')

                if evt['m_type'] == m_article and 'article' not in existingModes:

                    existingModes.append('article')

                if evt['m_type'] == m_pdf and 'pdf' not in existingModes:

                    existingModes.append('pdf')

                if evt['m_type'] == m_video and 'video' not in existingModes:

                    existingModes.append('video')

        existingModes = sorted(existingModes)

        for mode in ['article', 'pdf', 'storyline', 'video']:
            str_tmp = ch + '$$' + mode
            attr.append( str_tmp + "$$compRate"  )
            attr.append( str_tmp + "$$timeSpent" )
            attr.append( str_tmp + "$$numMaxWindow" )
            attr.append( str_tmp + "$$numMinWindow" )

            if mode == 'storyline':
                attr.append( str_tmp + '$$numPa' )
                attr.append( str_tmp + '$$numSb')
                attr.append( str_tmp + '$$numSf')
                attr.append( str_tmp + '$$numRe')
            if mode == 'pdf' or mode == 'article':
                attr.append( str_tmp + "$$numScroll_up" )
                attr.append( str_tmp + "$$numScroll_down" )

        attr.append( ch + "$$#sign_in")
        attr.append( ch + "$$#Notes" )
        attr.append( ch + "$$#Bookmarks" )
        attr.append( ch + "$$#Highlights")

        attr.append( ch + "$$EngagementScore")

    attr.append('sequence132')
    attr.append('#sessionCourse')
    for d in dates:
        attr.append("login$$"+d)

    attr.append('pass')
    attr.append('no_pass')
    attr.append('extend')


    allChapNums = []
    all_nav = func.get_evt_from_list(events, m_navi)
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-2:] )
            if chapNum not in allChapNums:
                allChapNums.append(chapNum)
    allChapNums = sorted(allChapNums)


    return attr, chap_names, allChapNums, existingModes, dates