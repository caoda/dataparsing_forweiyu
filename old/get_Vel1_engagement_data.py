import json
import numpy


def get():

    raw = json.load(open("engmt_data_from_dashboard/vel_engagement_from_dashboard.txt"))

    titles = []
    scores = []
    for unit_index in xrange(0, 7):
        unit_info = raw['children'][0]['children'][unit_index]
        #print(   'unit_name: %s       unit_id: %s'   %(unit_info['name'],unit_info['id'])   )
        unit_id = unit_info['id']
        titles.append(unit_id)

        all_students_scores_in_the_unit = {}
        for student in unit_info['students']:
            eng = student['engagement']['average']
            id = student['id']
            all_students_scores_in_the_unit[id] = eng

        scores.append(all_students_scores_in_the_unit)

    return titles, scores





# raw = json.load(open("engmt_data_from_dashboard/vel_engagement_from_dashboard.txt"))
# print(raw.keys())
# print raw['children'][0]['name']  # "Pins"
# print raw['children'][0]['title'] # "Pins"
# print raw['children'][0]['id'] # "VEL_1_TOPIC0"
# print raw['children'][0]['engagement']['average']
# for i in xrange(0,15): # "VEL_1_TOPIC0" engagement by users
#     eng = raw['children'][0]['students'][i]['engagement']['average']
#     id = raw['children'][0]['students'][i]['id']
#     name = raw['children'][0]['students'][i]['name']
#     print  "id: %s     name: %s     engagement: %s" %(id, name, eng)

# raw['children'][0]['children'][0] # unit 0  "user guide"
# raw['children'][0]['children'][1] # unit 1
# raw['children'][0]['children'][2] # unit 2
#

# for i in xrange(0,5):
#     eng = unit_info['students'][i]['engagement']['average']
#     id = unit_info['students'][i]['id']
#     name = unit_info['students'][i]['name']
#     print  "id: %s     name: %s     engagement: %s" %(id, name, eng)

