__author__ = 'dacao'

import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
import json
import csv
import datetime
import unicodedata
import codecs

import functions as func
import define_attributes
import get_MAH103_engmt_data

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


raw_fileName = 'raw_clickstream/measurements-pbp-pbp_mah_103_5.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


change_chapter_names = {
'PBP_MAH_103_5_CHAPTER00' : 'PBP_MAH_103_5_CHAPTER00',
'PBP_MAH_103_5_CHAPTER01' : 'PBP_MAH_103_5_CHAPTER01',
'PBP_MAH_103_5_CHAPTER02' : 'PBP_MAH_103_5_CHAPTER02',
'PBP_MAH_103_5_CHAPTER03' : 'PBP_MAH_103_5_CHAPTER03',
'PBP_MAH_103_5_CHAPTER04' : 'PBP_MAH_103_5_CHAPTER04',
'PBP_MAH_103_5_CHAPTER05' : 'delete',
'PBP_MAH_103_5_CHAPTER06' : 'PBP_MAH_103_5_CHAPTER05',
'PBP_MAH_103_5_CHAPTER07' : 'PBP_MAH_103_5_CHAPTER06',
'PBP_MAH_103_5_CHAPTER08' : 'PBP_MAH_103_5_CHAPTER07',
'PBP_MAH_103_5_CHAPTER09' : 'PBP_MAH_103_5_CHAPTER08',
'PBP_MAH_103_5_CHAPTER10' : 'delete',
'PBP_MAH_103_5_CHAPTER11' : 'PBP_MAH_103_5_CHAPTER09',
'PBP_MAH_103_5_CHAPTER12' : 'PBP_MAH_103_5_CHAPTER10' }

for evt in events:
    if 'chapter' in evt:
        evt['chapter'] = change_chapter_names[  evt['chapter']  ]

tmp = []
for evt in events:
    if 'chapter' in evt and evt['chapter'] == 'delete':
        continue

    else:
        tmp.append(evt)

events = tmp





''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, existingModes, dates = define_attributes.get(events)


'''/////////////////////////////////////  Get the engagement info ///////////////////////////////////////////////'''
titles, scores = get_MAH103_engmt_data.get()
tmp_titles = []
tmp_scores = []
for i in xrange(0, len(titles)):
    if titles[i] in change_chapter_names:
        titles[i] = change_chapter_names[titles[i]]


for i, title in enumerate(titles):
    if title == 'delete':
        print('delete this unit!!!!')
    else:
        tmp_titles.append(title)
        tmp_scores.append(scores[i])

titles = tmp_titles
scores = tmp_scores


for title in titles:
    print(title)
    attr.append( 'engagement_'+title )


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

fname = 'first-time-users-pbp-pbp_mah_103_5-from-2015-10-01-to-2016-03-16.csv'
csvfile = open(fname, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), fname))



passInfo = []

with open("pass_info/MAH103_Pass.txt") as f:
    content = f.readlines()
    content = content[0].split('\r')
    content.pop(0)

    for row in content:
        line = row.split('\t')
        passInfo.append(line)
        print(line)

print("there are %s users in mah103_4_passInfo.xlsx" %len(passInfo))


def get_names(userID):

    first = 'given'
    last = 'family'
    email = ''
    for line in userInfo:
        if userID == line[3]:
            first = line[0]
            last = line[1]
            email = line[2]

    return first, last, email


def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'PASS' in row:
                        return 1

    return 0


def get_NoPass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'No Pass' in row:
                        return 1

    return 0



'''//////////////////////////////////  Finish getting the pass/extend info /////////////////////////////////////'''

all_paths = []

def main():

    nameSTR = 'matrix_MAH103_5.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in users:
        first, last, email = get_names(user)
        if first == 'given' and last == 'family':
            continue

        #attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}, 'pass':get_pass(user), 'extend':get_NoPass(user)}
        attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}, 'pass':0, 'extend':0,'firstName':first, 'lastName':last, 'email':email}

        for d in dates:
            attr_thisUser['login'][d] = 0

        all_evts = uv[user]
        attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        for evt in all_lc:
            readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
            y = unicode(readTime.year)
            m = unicode(readTime.month)
            day = unicode(readTime.day)
            y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
            m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
            day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
            date_str = y+'_'+m+'_'+day
            attr_thisUser['login'][date_str] = 1

        all_lc = {user: all_lc}

        attr_thisUser["sequence132"] = get_seq132(func.get_evt_from_list(all_evts, m_navi), user)


        for ch in chap_names:
            if ch not in chapters:
                continue

            if ch not in attr_thisUser:
                attr_thisUser[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)

            # todo: get number of bookmarks, notes and highlights from evts_thisChap ??? need to ask
            attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
            attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
            attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode
            window_evts = {user: window_evts}

            if m_article in existing_types:

                art_evts = func.get_evt_from_list(evts_thisChap, m_article)

                numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
                art_evts = {user: art_evts}

                result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
                art_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo["epub"]['numMax']
                numMinWindow = windowInfo["epub"]['numMin']

                attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                                'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                                'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up }


            if m_pdf in existing_types:

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)


                numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
                pdf_evts = {user: pdf_evts}
                result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = result['averageCompletionRate']
                pdf_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['pdf']["numMax"]
                numMinWindow = windowInfo['pdf']["numMin"]

                attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                    numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up, 'numScroll_down':numScroll_down}


            if m_slide in existing_types:

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)


                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
                                                                 window_evts, all_lc)
                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['storyline']['numMax']
                numMinWindow = windowInfo['storyline']['numMin']

                numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

                attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                                  'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                                  'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow}

                if ch == 'PBP_MAH_103_5_CHAPTER04':
                    print('PBP_MAH_103_5_CHAPTER04  storyline_compRate  %s' %storyline_compRate)


            for i in xrange(0, len(titles)):
                if titles[i] == ch:
                    tmp = scores[i]
                    for studentID in tmp:
                        if studentID == user:
                            attr_thisUser[ch]["EngagementScore"] = tmp[studentID]
                            break

                    break


        row = func.attr_dict_to_list(attr, attr_thisUser)

        for tmp in scores:
            if user in tmp:
                row.append(tmp[user])
            else:
                row.append(0)

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!!')

    for i in xrange(0, len(attr)):
        print(attr[i])



    return 0





def get_seq132( all_nav , user):

    num = 0

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-2:] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_paths.append(path)

    if len(path) < 3:
        return 0
    else:
        for i in xrange(0,len(path)-2):
            if path[i] == 1 and path[i] == 2 and path[i] == 3:
                num += 1

    return num


def get_slides_total_duration(events):
    storyline_events = func.get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output

########################################################################################################################



main()









# get transition freq matrix
nameSTR = 'transition_freq_matrix_MAH103_5.csv'
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')
wr.writerow(["chapters/freq"] + allChapNums)

# initialize trans_dict
trans_dict = {}
for i in allChapNums:
    trans_dict[i] = {}
    for j in allChapNums:
        trans_dict[i][j] = 0

# fill in trans_dict
for path in all_paths:
    for i in xrange(0, len(path)-1):
        origin = path[i]
        destin = path[i+1]
        trans_dict[origin][destin] += 1

# fill in the csv.
for origin in allChapNums:
    row = []

    destinations = sorted( trans_dict[origin].keys() )
    for dest in destinations:
        row.append(trans_dict[origin][dest])

    row = [origin] + row

    wr.writerow(row)

fileName.close()
print('transition_freq_matrix.csv done!!')





# write events to csv
nameSTR = 'events_matrix_MAH103_5.csv'
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')
wr.writerow(['course', 'session', 'user', 'm_type', 'action', 'timestamp', 'ID'])
for evt in events:

    row = []
    row.append(evt['course'])
    row.append(evt['session'])
    row.append(evt['user'])
    row.append(evt['m_type'])
    if 'action' in evt:
        row.append(evt['action'])
    elif 'type' in evt:
        row.append(evt['type'])
    else:
        row.append("none")
    row.append(evt['timestamp'])
    row.append(evt['id'])

    wr.writerow(row)


fileName.close()
print('events_matrix.csv done!!')


