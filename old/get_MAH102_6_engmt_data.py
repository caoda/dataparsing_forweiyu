
import json
import numpy


def get():

    raw = json.load(open("engmt_data_from_dashboard/MAH102_6_engagement_data_2016_06_14.txt"))
    engmt_overall = {}

    title = []
    score = []


    for line in raw['students']:
        if line['id'] not in engmt_overall:
            engmt_overall[ line['id'] ] = line['engagement']['average']

    print('\n\n')


    title.append(raw['id'])
    tmp = {}
    for student in raw['students']:
        tmp[student['id']] = student['engagement']['average']
    score.append(tmp)

    for child in raw['children']:
        title.append(child['id'])
        tmp = {}
        for student in child['students']:
            tmp[student['id']] = student['engagement']['average']
        score.append(tmp)


        for grand_child in child['children']:
            title.append(grand_child['id'])
            tmp = {}
            for student in grand_child['students']:
                tmp[student['id']] = student['engagement']['average']
            score.append(tmp)



    quantiled_scores = []
    for i in xrange(0, len(score)):
        target = score[i].values()

        # get the threshold for top 25% of the scores
        thresh = numpy.percentile(numpy.array(target), 75)

        for j in xrange(0, len(target)):
            if target[j] >= thresh:
                target[j] = 100.0

        quantiled_scores.append(target)

    return title, score




