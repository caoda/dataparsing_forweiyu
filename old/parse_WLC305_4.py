__author__ = 'dacao'

from measurements import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
import json
import csv
import datetime
import unicodedata
import codecs

import functions as func
import define_attributes
import get_WLC305_engmt_data

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


raw_fileName = 'raw_clickstream/measurements-pbp-pbp_wlc_305_4.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])

# 2016_2_11    1455211211
# 2016_2_12    1455293114

# timestamp_filter = 1455293114
# new = []
# for evt in events:
#     if evt['timestamp'] < timestamp_filter:
#         new.append(evt)
# events = new
# del new


change_chap_names = {
    "PBP_WLC_305_4_CHAPTER00":"PBP_WLC_305_4_CHAPTER00",
    "PBP_WLC_305_4_CHAPTER10":"PBP_WLC_305_4_CHAPTER01",
    "PBP_WLC_305_4_CHAPTER20":"PBP_WLC_305_4_CHAPTER02",
    "PBP_WLC_305_4_CHAPTER30":"PBP_WLC_305_4_CHAPTER03",
    "PBP_WLC_305_4_CHAPTER40":"PBP_WLC_305_4_CHAPTER04",
    "PBP_WLC_305_4_CHAPTER50":"PBP_WLC_305_4_CHAPTER05",
    "PBP_WLC_305_4_CHAPTER60":"PBP_WLC_305_4_CHAPTER06",
    "PBP_WLC_305_4_CHAPTER70":"PBP_WLC_305_4_CHAPTER07",
    "PBP_WLC_305_4_CHAPTER80":"PBP_WLC_305_4_CHAPTER08",
    "PBP_WLC_305_4_CHAPTER90":"PBP_WLC_305_4_CHAPTER09",
    "PBP_WLC_305_4_CHAPTER100":"PBP_WLC_305_4_CHAPTER10",
}


for evt in events:
    if 'chapter' in evt and evt['chapter'] in change_chap_names:
        evt['chapter'] = change_chap_names[  evt['chapter']  ]




''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, existingModes, dates = define_attributes.get(events)


'''/////////////////////////////////////  Get the engagement info ///////////////////////////////////////////////'''
titles, scores = get_WLC305_engmt_data.get()
for title in titles:
    #print(title)
    attr.append( 'engagement_'+title )


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

fname = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_4-from-2015-10-01-to-2016-05-31.csv'
csvfile = open(fname, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    print(row)
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), fname))



passInfo = []

pass_fail_fname = 'wlc305_4_pass_info.csv'
with open("pass_info/%s" %pass_fail_fname) as f:
    content = f.readlines()
    content = content[0].split('\r')
    for row in content:
        line = row.split(',')
        passInfo.append(line)
print("there are %s users in %s" %(len(passInfo), pass_fail_fname))


def get_names(userID):

    first = 'given'
    last = 'family'
    email = ''
    for line in userInfo:
        if userID == line[3]:
            first = line[0]
            last = line[1]
            email = line[2]

    return first, last, email

def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            email = line[2]

            for row in passInfo:
                if row[0] == email:
                    if 'PASS' in row:
                        #print('PASS!')
                        return 1

    return 0

def get_extend(userID):
    #print("\n user: %s " %userID)
    for line in userInfo:
        if userID == line[3]:
            email = line[2]
            found = False
            for row in passInfo:
                if row[0] == email:

                    found = True
                    row.append("found")

                    if 'Extend' in row:

                        #print('extend!')

                        return 1

            if not found:
                print("not found in new.csv")
                #print(email)
    return 0
'''//////////////////////////////////  Finish getting the pass/extend info /////////////////////////////////////'''


def filter_dup_Pause(storyLine_evts):
    new = []
    ref = -1
    for evt in storyLine_evts:
        add = True
        if 'm_type' in evt and evt['m_type'] == m_slide:
            if 'type' in evt and evt['type'] == 'Pause':
                t = evt['timestamp']
                if t-ref < 0.2 and t-ref > -0.2: # duplicate event found
                    print('dup pause  %s  %s' %(evt['chapter'], evt['user']) )
                    add = False
                else:
                    ref = t
        if add:
            new.append(evt)
    return new


all_paths = []

# todo: main
def main():

    nameSTR = 'feature_matrix/matrix_WLC305_4_all_data.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in users:


        first, last, email = get_names(user)
        if first == 'given' and last == 'family':
            continue

        attr_thisUser = {"sequence132":0, "#session": 0, 'login':{}, 'pass':get_pass(user), 'extend':get_extend(user),
                         'firstName':first, 'lastName':last, 'email':email}
        for d in dates:
            attr_thisUser['login'][d] = 0

        all_evts = uv[user]
        attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)
        for evt in all_lc:
            readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
            y = unicode(readTime.year)
            m = unicode(readTime.month)
            day = unicode(readTime.day)
            y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
            m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
            day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
            date_str = y+'_'+m+'_'+day
            attr_thisUser['login'][date_str] = 1

        all_lc = {user: all_lc}

        attr_thisUser["sequence132"] = get_seq132(func.get_evt_from_list(all_evts, m_navi), user)


        for ch in chap_names:
            if ch not in chapters:
                continue

            if ch not in attr_thisUser:
                attr_thisUser[ch] = {}

            evts_thisChap = chapters[ch]
            existing_types = func.get_existing_types(evts_thisChap)

            # todo: get number of bookmarks, notes and highlights from evts_thisChap ??? need to ask
            attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
            attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
            attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

            nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
            attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
            nav_evts = {user: nav_evts}

            window_evts = func.get_evt_from_list(evts_thisChap, m_window)
            windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode

            window_evts = {user: window_evts}


            if m_article in existing_types:

                art_evts = func.get_evt_from_list(evts_thisChap, m_article)
                numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
                art_evts = {user: art_evts}

                result = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
                art_compRate = result['averageCompletionRate'] # average for single user is just this user's rate
                art_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo["epub"]['numMax']
                numMinWindow = windowInfo["epub"]['numMin']

                attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                                'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                                'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up }


            if m_pdf in existing_types:

                pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)

                numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
                pdf_evts = {user: pdf_evts}
                result, dummy = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
                pdf_compRate = result['averageCompletionRate']
                pdf_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['pdf']["numMax"]
                numMinWindow = windowInfo['pdf']["numMin"]

                attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                    numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up, 'numScroll_down':numScroll_down}


            if m_slide in existing_types:

                storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
                storyLine_evts = filter_dup_Pause(storyLine_evts)

                storyLine_evts = {user: storyLine_evts}
                slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
                slideChanged = {user: slideChanged}
                slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
                slideComp = {user: slideComp}

                result = Measurements_storyline.get_measurements(storyLine_evts, nav_evts, slideChanged, slideComp,
                                                                 window_evts, all_lc)
                storyline_compRate = result['averageCompletionRate']
                storyline_timeSpent = result['totalTimeSpent']
                numMaxWindow = windowInfo['storyline']['numMax']
                numMinWindow = windowInfo['storyline']['numMin']

                numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

                attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                                  'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                                  'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow}



            for i in xrange(0, len(titles)):
                if titles[i] == ch:
                    tmp = scores[i]
                    for studentID in tmp:
                        if studentID == user:
                            attr_thisUser[ch]["EngagementScore"] = tmp[studentID]
                            break

                    break


        row = func.attr_dict_to_list(attr, attr_thisUser)

        for tmp in scores:
            if user in tmp:
                row.append(tmp[user])
            else:
                row.append(0)

        wr.writerow([user] + row)
    fileName.close()
    print('csv file done!!')

    for i in xrange(0, len(attr)):
        print(attr[i])



    return 0





def get_seq132( all_nav , user):

    num = 0

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-2:] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_paths.append(path)

    if len(path) < 3:
        return 0
    else:
        for i in xrange(0,len(path)-2):
            if path[i] == 1 and path[i] == 2 and path[i] == 3:
                num += 1

    return num


def get_slides_total_duration(events):
    storyline_events = func.get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output

########################################################################################################################



main()








#
# # get transition freq matrix
# nameSTR = 'transition_freq_matrix_WLC305_4.csv'
# fileName = open(nameSTR, 'wb')
# wr = csv.writer( fileName, dialect= 'excel')
# wr.writerow(["chapters/freq"] + allChapNums)
#
# # initialize trans_dict
# trans_dict = {}
# for i in allChapNums:
#     trans_dict[i] = {}
#     for j in allChapNums:
#         trans_dict[i][j] = 0
#
# # fill in trans_dict
# for path in all_paths:
#     for i in xrange(0, len(path)-1):
#         origin = path[i]
#         destin = path[i+1]
#         trans_dict[origin][destin] += 1
#
# # fill in the csv.
# for origin in allChapNums:
#     row = []
#
#     destinations = sorted( trans_dict[origin].keys() )
#     for dest in destinations:
#         row.append(trans_dict[origin][dest])
#
#     row = [origin] + row
#
#     wr.writerow(row)
#
# fileName.close()
# print('transition_freq_matrix.csv done!!')
#
#



# # write events to csv
# nameSTR = 'events_matrix_WLC305_4.csv'
# fileName = open(nameSTR, 'wb')
# wr = csv.writer( fileName, dialect= 'excel')
# wr.writerow(['course', 'session', 'user', 'm_type', 'action', 'timestamp', 'ID'])
# for evt in events:
#
#     row = []
#     row.append(evt['course'])
#     row.append(evt['session'])
#     row.append(evt['user'])
#     row.append(evt['m_type'])
#     if 'action' in evt:
#         row.append(evt['action'])
#     elif 'type' in evt:
#         row.append(evt['type'])
#     else:
#         row.append("none")
#     row.append(evt['timestamp'])
#     row.append(evt['_id'])
#
#     wr.writerow(row)
#
#
# fileName.close()
# print('events_matrix.csv done!!')



# for row in passInfo:
#     if "found" not in row:
#         print(row)