__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import codecs


raw_fileName = 'pbp_discussion_board_data/wlc305_dis_access_your_skills.json'
# raw_fileName = 'pbp_discussion_board_data/mah_102_reconvene_for_article_review.json'
# raw_fileName = 'pbp_discussion_board_data/wlc302_apply_what_you_learned.json'
# raw_fileName = 'pbp_discussion_board_data/mah_103_reconvene_for_article_review.json'
# raw_fileName = 'pbp_discussion_board_data/wlc303_apply_what_you_learnd.json'
# raw_fileName = 'pbp_discussion_board_data/wlc304_apply_what_you_learnd.json'
#raw_fileName = 'pbp_discussion_board_data/wlc305_apply_what_you_learnd.json'     # 2015_10_5    1444070075
#raw_fileName = 'pbp_discussion_board_data/wlc305_dis_access_your_skills.json'   # 2015_10_1    1443704921
#raw_fileName = 'pbp_discussion_board_data/wlc305_4_apply_what_you_learnd.json'    # 2016_2_12    1455293114
#raw_fileName = 'pbp_discussion_board_data/wlc305_4_dis_access_your_skills.json'     # 2016_2_11    1455211211

board_data = json.load(open(raw_fileName))

print(len(board_data['posts']))

for i in xrange(len(board_data['posts'])):
    post = board_data['posts'][i]
    timestamp = int(post['timestamp'] / 1000.0)
    user_name = post['user']['username']

    readTime = datetime.datetime.fromtimestamp(timestamp)
    y = unicode(readTime.year)
    m = unicode(readTime.month)
    day = unicode(readTime.day)
    y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
    m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
    day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
    date_str = y+'_'+m+'_'+day

    #print('%s   %s  %s' %(timestamp, date_str, user_name))

