__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline
import absolute_engagement_func as AB_eng
import functions as func


m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017


def get_names(userID, userInfo):

    first = 'given'
    last = 'family'
    email = ''
    for line in userInfo:
        if userID == line[3]:
            first = line[0]
            last = line[1]
            email = line[2]

    return first, last, email


def filter_dup_Pause(storyLine_evts):
    new = []
    ref = -1
    for evt in storyLine_evts:
        add = True
        if 'm_type' in evt and evt['m_type'] == m_slide:
            if 'type' in evt and evt['type'] == 'Pause':
                t = evt['timestamp']
                if t-ref < 0.2 and t-ref > -0.2: # duplicate event found
                    print('dup pause  %s  %s' %(evt['chapter'], evt['user']) )
                    add = False
                else:
                    ref = t
        if add:
            new.append(evt)
    return new




def core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos, userInfo, course_name, session_number, user_file_timespent):

    first, last, email = get_names(user, userInfo)
    if first == 'given' and last == 'family':
        return -1


    attr_thisUser = {"#session": 0, 'firstName':first, 'lastName':last, 'email':email, 'course_name': course_name ,
                     'session_number': session_number,'article_existing': 0, 'pdf_existing': 0, 'storyline_existing': 0}



    all_evts = uv[user]
    attr_thisUser["#sessionCourse"] = func.get_num_sessions(all_evts)

    chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts)

    all_lc = {user: all_lc}

    for ch in chap_names:
        if ch not in chapters:
            continue

        if ch not in attr_thisUser:
            attr_thisUser[ch] = {}

        evts_thisChap = chapters[ch]
        existing_types = func.get_existing_types(evts_thisChap)

        # todo: get number of bookmarks, notes and highlights from evts_thisChap ??? need to ask
        attr_thisUser[ch]["#Notes"] = func.get_num_notes(evts_thisChap)
        attr_thisUser[ch]["#Bookmarks"] = func.get_num_bookmarks(evts_thisChap)
        attr_thisUser[ch]["#Highlights"] = func.get_num_highlights(evts_thisChap)

        nav_evts = func.get_evt_from_list(evts_thisChap, m_navi)
        attr_thisUser[ch]["#sign_in"] = func.get_numSignIn(nav_evts)
        nav_evts = {user: nav_evts}

        window_evts = func.get_evt_from_list(evts_thisChap, m_window)
        windowInfo = func.get_numMaxMinWindow(window_evts, existing_types) # get number of window max/min for each mode
        window_evts = {user: window_evts}


        if m_article in existing_types:
            attr_thisUser['article_existing'] = 1

            art_evts = func.get_evt_from_list(evts_thisChap, m_article)

            numScroll_up, numScroll_down = func.get_scroll_count_article(art_evts)
            art_evts = {user: art_evts}

            result, hm_all_tSpent = Measurements_article.get_measurements(art_evts, nav_evts, window_evts, all_lc)
            article_id = result['article']
            articleLength = result['articleLength']
            art_compRate = result['averageCompletionRate']
            art_timeSpent = result['totalTimeSpent']
            numMaxWindow = windowInfo["epub"]['numMax']
            numMinWindow = windowInfo["epub"]['numMin']

            this_user_article = AB_eng.UserArticle(alpha_mode, user, ch, article_id, articleLength, hm_all_tSpent)
            eng = this_user_article.get_engagement_score()

            attr_thisUser[ch]['article'] = {"compRate": art_compRate, 'timeSpent': art_timeSpent,
                                            'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                            'numScroll_down':numScroll_down, 'numScroll_up':numScroll_up,
                                            'abs_engagement':eng}

            user_file_timespent[user][article_id] = art_timeSpent

        if m_pdf in existing_types:
            attr_thisUser['pdf_existing'] = 1

            pdf_evts = func.get_evt_from_list(evts_thisChap, m_pdf)

            numScroll_up, numScroll_down = func.get_scroll_count_pdf(pdf_evts)
            pdf_evts = {user: pdf_evts}


            numMaxWindow = windowInfo['pdf']["numMax"]
            numMinWindow = windowInfo['pdf']["numMin"]


            overall_result, result = Measurements_pdf.get_measurements(pdf_evts, nav_evts, window_evts, all_lc)
            pdf_compRate = overall_result['averageCompletionRate']
            pdf_timeSpent = overall_result['totalTimeSpent']
            pdf_id = overall_result['pdf']
            numPages = overall_result['numPages']


            fname = 'pdf_words_in_courses/%s_all_PDFs_words.json'%courseID
            in_file = open(fname,"r")
            words_in_PDFs_of_a_course = json.load(in_file) # words_in_PDFs_of_a_course[contentID][page_name] = [all words in the page]
            in_file.close()


            wordCounts_in_PDFs_of_a_course = {}
            for pageName in words_in_PDFs_of_a_course[pdf_id]:
                a = pageName.index('[')
                b = pageName.index(']')
                pageNumber = int(pageName[a+1:b])
                wordCounts_in_PDFs_of_a_course[pageNumber] = len( words_in_PDFs_of_a_course[pdf_id][pageName] )

            heatmap = []
            expected_timeSpent_on_pages = []
            for p in xrange(1, numPages+1):
                if p not in result[user]:
                    continue
                heatmap.append(  result[user][p]['timeSpent'])
                expected_timeSpent = wordCounts_in_PDFs_of_a_course[p-1] / 3.0
                if expected_timeSpent == 0:
                    expected_timeSpent = 1
                expected_timeSpent_on_pages.append(expected_timeSpent)


            for i in xrange(0, len(heatmap)):
                if heatmap[i] > pdf_timeSpent_cap_on_page:
                    heatmap[i] = pdf_timeSpent_cap_on_page


            this_user_pdf = AB_eng.UserPdf(alpha_mode, user, ch, pdf_id, heatmap, expected_timeSpent_on_pages)
            eng = this_user_pdf.get_engagement_score()

            attr_thisUser[ch]['pdf'] = {"compRate": pdf_compRate, 'timeSpent': pdf_timeSpent, 'numMaxWindow':
                                        numMaxWindow, 'numMinWindow':numMinWindow, 'numScroll_up': numScroll_up,
                                        'numScroll_down':numScroll_down, 'abs_engagement':eng}

            user_file_timespent[user][pdf_id] = pdf_timeSpent

        if m_slide in existing_types:
            attr_thisUser['storyline_existing'] = 1

            storyLine_evts = func.get_evt_from_list(evts_thisChap, m_slide)
            contentID = storyLine_evts[0]['content']
            storyLine_evts = filter_dup_Pause(storyLine_evts)

            storyLine_evts = {user: storyLine_evts}
            slideChanged = func.get_evt_from_list(evts_thisChap, m_slideChanged)
            slideChanged = {user: slideChanged}
            slideComp = func.get_evt_from_list(evts_thisChap, m_slideCompleted)
            slideComp = {user: slideComp}

            slide_infos = dict_storylineID_slideInfos[contentID]['slide_infos']
            slide_durations_list = dict_storylineID_slideInfos[contentID]['slide_durations_list']

            result, matrices_dic = Measurements_storyline.get_measurements(slideChanged, slideComp, storyLine_evts,
                                                   slide_infos, nav_evts, all_lc, {},
                                                   window_evts, end_timestamp)

            storyline_compRate = result['averageCompletionRate']
            storyline_timeSpent = result['totalTimeSpent']
            numMaxWindow = windowInfo['storyline']['numMax']
            numMinWindow = windowInfo['storyline']['numMin']
            numPa, numSb, numSf, numRe = func.get_counts(storyLine_evts)

            time_spent_matrix = matrices_dic['time_spent_matrix']
            time_spent_matrix = np.asarray(time_spent_matrix).ravel().tolist()
            expected_timeSpent_on_slides = slide_durations_list

            this_user_storyline = AB_eng.UserStoryline(alpha_mode, user, ch, contentID, time_spent_matrix, expected_timeSpent_on_slides)
            eng = this_user_storyline.get_engagement_score()

            attr_thisUser[ch]['storyline'] = {"compRate": storyline_compRate, 'timeSpent': storyline_timeSpent,
                                              'numPa': numPa, 'numSb': numSb, 'numSf': numSf, 'numRe': numRe,
                                              'numMaxWindow':numMaxWindow, 'numMinWindow':numMinWindow,
                                              'abs_engagement':eng}

            user_file_timespent[user][contentID] = pdf_timeSpent

    row = func.attr_dict_to_list(attr, attr_thisUser)

    return row, user_file_timespent