__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np


from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline
from pdf_words_in_courses import *
import absolute_engagement_func as AB_eng
import functions as func
import define_attributes_new_with_abs_engagement
import PBP_feature_parsing_for_one_user_abs_eng as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'




''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017

course_name = 'WLC_305'
session_number = 2
course_name_to_remove_in_feature_name = 'PBP_WLC_305_2_CHAPTER'

''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_wlc_305_2.json'
users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_2-from-2015-09-01-to-2015-12-18.csv'
pass_fail_extend_info = "pass_info/wlc305_pass_extend_info.csv"


events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])

print('number of events: ')
print(len(events))


uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
all_users = sorted(uv.keys())


courseID = func.get_course_id(events)


''' get the storyline contentIDs '''
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}



chap_content = {

    'PBP_WLC_305_2_CHAPTER00':[['PBP_WLC_305_2_CONTENT00'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER01':[['PBP_WLC_305_2_CONTENT10'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER02':[['PBP_WLC_305_2_CONTENT20'], ['epud']],
    'PBP_WLC_305_2_CHAPTER03':[['PBP_WLC_305_2_CONTENT30'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER04':[['PBP_WLC_305_2_CONTENT40'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER05':[['PBP_WLC_305_2_CONTENT50'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER06':[['PBP_WLC_305_2_CONTENT60'], ['epud']],
    'PBP_WLC_305_2_CHAPTER07':[['PBP_WLC_305_2_CONTENT70'], ['epud']],
    'PBP_WLC_305_2_CHAPTER08':[['PBP_WLC_305_2_CONTENT80', 'PBP_WLC_305_2_CONTENT81'], ['pdf', 'epud']],
    'PBP_WLC_305_2_CHAPTER09':[['PBP_WLC_305_2_CONTENT90'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER10':[['PBP_WLC_305_2_CONTENT100'], ['epud']]
}

clean_events = []
for evt in events:

    if 'chapter' in evt and evt['chapter'] in chap_content:

        if 'content' in evt and evt['content'] not in chap_content[ evt['chapter'] ][0]:
            continue
        if 'article' in evt and evt['article'] not in chap_content[ evt['chapter'] ][0]:
            continue

    clean_events.append(evt)

events = clean_events
del clean_events

clean_events = []
for evt in events:
    if 'chapter' in evt and evt['chapter'] not in chap_content:
        continue
    clean_events.append(evt)
events = clean_events
del clean_events



''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, dates, chap_existing_modes = define_attributes_new_with_abs_engagement.get(events)


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

csvfile = open(users_from_reporting_tool, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), users_from_reporting_tool))


passInfo = []

with open(pass_fail_extend_info) as f:
    content = f.readlines()
    content = content[0].split('\r')

    for row in content:
        line = row.split(',')
        passInfo.append(line)

print("there are %s users in %s" %(len(passInfo), pass_fail_extend_info))


def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            email = line[2]

            for row in passInfo:
                if row[0].lower() == email.lower():
                    if 'PASS' in row:
                        return 1

    return 0


def get_extend(userID):
    #print("\n user: %s " %userID)
    for line in userInfo:
        if userID == line[3]:

            email = line[2]

            found = False

            for row in passInfo:
                if row[0].lower() == email.lower():

                    found = True
                    row.append("found")

                    if 'Extend' in row:

                        return 1

            if not found:
                print("not found in csv")
                #print(email)
    return 0

'''//////////////////////////////////  the pass/extend info ///////////////////////////////////////////'''


def remove_course_name_from_chapterIDs(attr):
    feature_names = []
    for name in attr:
        if len(name) > 21 and name[0:21] == course_name_to_remove_in_feature_name:
            tmp = name[14:]
            feature_names.append(tmp)
        else:
            feature_names.append(name)

    return feature_names


def main(events, time_filter_datetime):


    # file name for output matrix
    output_file_name = 'day_by_day_matrix/wlc305/matrix_WLC305_2_filtered_%s.csv' %time_filter_datetime

    timestamp_filter = func.normail_time_to_unix_timestamp(time_filter_datetime)
    events = func.filter_with_timestamp(events, timestamp_filter)

    nameSTR = output_file_name
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')

    feature_names = remove_course_name_from_chapterIDs(attr)
    wr.writerow(["user"] + feature_names)
    print('number of features:   %s' %len(feature_names) )

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in all_users:

        user_pass = get_pass(user)
        user_extend = get_extend(user)
        user_nopass = 0

        row = parsing.core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos, userInfo,
                           user_pass, user_nopass, user_extend, 0, course_name, session_number, chap_existing_modes)

        if row == -1:
            continue

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!!')


    return 0





time_filter_datetimes = [
                            '2015_10_01_00_00',
                            '2015_10_02_00_00',
                            '2015_10_03_00_00',
                            '2015_10_04_00_00',
                            '2015_10_05_00_00',
                            '2015_10_06_00_00',
                            '2015_10_07_00_00',
                            '2015_10_08_00_00',
                            '2015_10_09_00_00',
                            '2015_10_10_00_00',
                            '2015_10_11_00_00',
                            '2015_10_12_00_00',
                            '2015_10_13_00_00',
                            '2015_10_14_00_00',
                            '2015_10_15_00_00'
                        ]

for time_filter_datetime in time_filter_datetimes:
    main(events, time_filter_datetime)


for c in chap_names:
    print(c)



csv_file = open("day_by_day_matrix/all_users_%s_%s.csv"%(course_name, session_number), "wb")
wr = csv.writer( csv_file, dialect= 'excel')
for user in all_users:
    full_name = []
    for item in userInfo:
        if item[3] == user:
            givenName = item[0]
            familyName = item[1]
            full_name = [user, givenName, familyName]
    wr.writerow(full_name)
csv_file.close()








