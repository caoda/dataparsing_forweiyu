__author__ = 'dacao'
import os
import datetime
import math
import json
import sys
import get_played_intervals


algo_debug = False

def print_debug(*args):
    if algo_debug is True:
        print(args);



'''
allPBEvents:        { userID : a list of playback events from specific user in specific chapter, ...}
allNavEvents:       { userID : a list of navigation events from specific user, ...}
slideChanged_unit:  { userID : a list of slideChanged events from specific user in specific chapter, ...}
slideComp_unit:     { userID : a list of slideCompeted events from specific user in specific chapter, ...}
windowLayout_unit:  { userID : a list of windowLayout events from specific user, ...}
lifeCycle_unit:     { userID : a list of life cycle events from specific user, ...}
'''

def get_measurements(allPBEvents, allNavEvents, slideChanged_unit, slideComp_unit, windowLayout_unit,
                              lifeCycle_unit):

#    results = {
#                      "totalTimeSpent": total_timespent,
#                      "averageCompletionRate": Average_completionRate,
#
#                      "views":[
#                              { slideIndex = 1,
#                                slide = abcd,
#                                slideTitle=title,
#                                duration = slideDuration
#                                 timeSpent = 99 ,   from get_slides_stats()   averaged to per user.
#                                timesViewed = 2,
#                                playback = {
#			                                    "totalTimeSpent": total_timespent,
#				                                "averageCompletionRate": Average_completionRate,
#                               		            views= [
#                              		 	                        { start = 0.3,  # position of slide playback, 1.0 is maximun
#                                 		  	                        end = 0.7,
#                                 		  	                        timeSpent = 99, from get_measurements_singleSlide_inStoryline()
                                                                 # video playing time
#                                		  	                        timesViewed = 2,
#                              			                        },
#                                 		 	                    ....
#
#                              			                    ]
#
#
#                              },
#                                  ....
#
#                              ]
#		}

    """
    print_debug "Storyline................................................"

    for key in allPBEvents.keys():
        for event in allPBEvents[key]:
            event.pop('_id')

    for key in allNavEvents.keys():
        for event in allNavEvents[key]:
            event.pop('_id')

    for key in slideChanged_unit.keys():
        for event in slideChanged_unit[key]:
            event.pop('_id')

    for key in slideComp_unit.keys():
        for event in slideComp_unit[key]:
            event.pop('_id')

    for key in windowLayout_unit.keys():
        for event in windowLayout_unit[key]:
            event.pop('_id')

    for key in lifeCycle_unit.keys():
        for event in lifeCycle_unit[key]:
            if event.get('_id') is not None:
                event.pop('_id')


    print_debug "allPBEvents %s" % json.dumps(allPBEvents)
    print_debug "allNavEvents %s" % json.dumps(allNavEvents)
    print_debug "slideChanged_unit %s" % json.dumps(slideChanged_unit)
    print_debug "slideComp_unit %s" % json.dumps(slideComp_unit)

    print_debug "windowLayout_unit %s" % json.dumps(windowLayout_unit)
    print_debug "lifeCycle_unit %s" % json.dumps(lifeCycle_unit)
    """


    # check:  make sure all inputs have the same keys
    inputs = [allPBEvents, allNavEvents, slideChanged_unit, slideComp_unit, windowLayout_unit,lifeCycle_unit]
    ref = sorted(inputs[0].keys())
    if len(ref) < 1:
        print_debug("numOfUsers < 1")
        return -1
    numOfUsers = len(ref)

    for i in range(1, len(inputs)):
        compare = sorted(inputs[i].keys())
        if len(compare) != len(ref):
            for key in ref:
                if key not in compare:
                    print_debug("key %s not consistent" %key)
            return -1
        for j in range(0, len(ref)):
            if ref[j] != compare[j]:
                return -1
    # check is finished. all inputs have the same keys


    # this is the output.
    final = {}


    slides_stats, stats_slideComp = get_slides_stats(allPBEvents, allNavEvents, slideChanged_unit, slideComp_unit,
                                                     lifeCycle_unit, numOfUsers)
    slideIndexes = sorted(list(slides_stats.keys())) #todo: we can only tell total number of slides this way.
    if len(slideIndexes) > 0:
        totalNumSlides = len(slideIndexes)
    else:
        return {} # if 0 slide existing, return enpty dict() as output


    totalTime = 0
    for index in slides_stats:
        totalTime += slides_stats[index]['timeSpent']
    totalTime = totalTime * len(allPBEvents.keys())

    compRates = []
    for user in stats_slideComp:
        tmp = 0
        for slide in stats_slideComp[user]:
            tmp += stats_slideComp[user][slide]
        compRates.append(  tmp/float(totalNumSlides)   )
    avg_compRate = int(    (sum(compRates) / float(numOfUsers))*100.0      )
    # number of users ensured to be non zero, therefore len(compRates)

    final["totalTimeSpent"] = totalTime
    final["averageCompletionRate"] = avg_compRate
    final["views"] = []


    allPBEvents_new = {} # break events list in allPBEvents into a dict with keys being slideIndexes
    for user in allPBEvents:
        tmp = {}
        event_list = allPBEvents[user]

        if len(event_list) < 1:
            continue

        for i in xrange( len(event_list)  ):
            if event_list[i]["slideIndex"] not in tmp:
                tmp[ event_list[i]["slideIndex"] ] = [ event_list[i] ]
            else:
                tmp[ event_list[i]["slideIndex"] ].append(event_list[i])

        allPBEvents_new[user] = tmp
        # allPBEvents_new  is a dict : { user: { slide:[events], ... } , ... }



    allPBEvents_list = []

    for i in xrange(len(slideIndexes)):  # len(slideIndexes) > 0 for sure at this line
        slideIndex = slideIndexes[i]
        tmp = {}
        for user in allPBEvents_new:
            if slideIndex in allPBEvents_new[user]:
                tmp[user] = allPBEvents_new[user][slideIndex]

        allPBEvents_list.append(tmp)

    results = []

    for i in range(len(allPBEvents_list)): # len(allPBEvents_list) > 0 for sure if len(slideIndexes) > 0
        vid_thisSlideIndex = allPBEvents_list[i]

        result = get_measurements_singleSlide_inStoryline(vid_thisSlideIndex, allNavEvents, slideChanged_unit,
                                                         slideComp_unit, windowLayout_unit, lifeCycle_unit)
        results.append(result)


    for i in range(0, len(slideIndexes)):
        tmp = slides_stats[slideIndexes[i]]
        # add 2 more keys to the existing summary dict
        tmp['slideIndex'] = slideIndexes[i]
        tmp['playback'] = results[i]

        final["views"].append(tmp)

    return final
# end of main




#################  supportive functions  ########################

def get_slides_stats(allPBEvents, allNavEvents, slideChanged_unit, slideComp_unit, lifeCycle_unit, numberOfU):
    stats = {}
    stats_slideComp = {}

    for user in allPBEvents:

        if user not in stats_slideComp:
            stats_slideComp[user] = {}

        for evt in allPBEvents[user]:
            evt["measure"] = 'playback'
        for evt in allNavEvents[user]:
            evt["measure"] = 'navi'
        for evt in slideChanged_unit[user]:
            evt["measure"] = 'slideChanged'
        for evt in slideComp_unit[user]:
            evt["measure"] = 'slideCompleted'
        for evt in lifeCycle_unit[user]:
            evt["measure"] = 'lifeCycle'


        new = allPBEvents[user] + allNavEvents[user] + slideChanged_unit[user] + slideComp_unit[user] + \
              lifeCycle_unit[user]
        if len(new) < 1:
            return stats, stats_slideComp
        new = sorted(new, key=lambda k:k["timestamp"])

        i = 0

        howManyTimes = 0
        while i < len(new):
            #prevent loop from never exiting in all cases!
            if howManyTimes >= 1000:
                print_debug("get_slides_stats: while loop hits 1000  i = %s;  %s " %( i, new[i] ))
                break
            howManyTimes = howManyTimes + 1

            if new[i]['measure'] == 'playback':

                # todo: change the threshold here.
                thresh_length = 2 * new[i]['duration']

                ind = new[i]['slideIndex']
                slideID = new[i]['slide']
                startTime = new[i]['timestamp']
                endTime = new[i]['timestamp'] # updated later

                stats_slideComp[user][ind] = 1

                if ind not in stats:
                    stats[ ind  ] = {'slide':new[i]['slide'],
                                       'slideTitle':new[i]['slideTitle'],
                                       'duration': new[i]['duration'],
                                       'timeSpent':0,
                                       'timesViewed':0
                                                       }
                for j in xrange(i+1, len(new)): #todo: in python 2, range takes memory, use xrange instead

                    if new[j]['measure'] == 'navi' and  new[j]['action'] == 'close':
                        endTime = new[j]['timestamp']
                        i = j+1
                        break
                    elif new[j]['measure'] == 'lifeCycle' and  new[j]['action'] == 'close':
                        endTime = new[j]['timestamp']
                        i = j+1

                        break
                    elif new[j]['measure'] == 'slideChanged':
                        endTime = new[j]['timestamp']
                        i = j+1

                        break
                    elif new[j]['measure'] == 'slideCompleted':
                        endTime = new[j]['timestamp']
                        i = j+1

                        break
                    # extreme case: sudden quit/browser failure/computer system failed
                    elif new[j]['measure'] == 'lifeCycle' and  new[j]['action'] == 'open':
                        # if j > i+1: # in very extreme case j == i+1 : endTime == startTime and time spent is 0
                        #     endTime = new[j-1]['timestamp']
                        i = j+1
                        break
                    elif j == len(new) - 1:
                        endTime = new[j]['timestamp']
                        i = j + 1
                        break

                if i+1 >= len(new)-1:
                    i = i + 1


                if (endTime - startTime) <= thresh_length:
                    stats[ ind  ]['timeSpent'] += math.ceil(endTime - startTime)
                    #print("check here:  %s     %s " %(int(endTime - startTime) ,  thresh_length))
                    if math.ceil(endTime - startTime) > 0:
                        stats[ ind  ]['timesViewed'] += 1


            else:
                i += 1

    # get the timespent per employee
    for ind in stats:
        stats[ ind  ]['timeSpent'] = math.ceil(  stats[ ind ]['timeSpent'] / float(numberOfU)  )

    return stats, stats_slideComp



def get_measurements_singleSlide_inStoryline(allPBEvents, allNavEvents, slideChanged_unit, slideComp_unit,
                                                         windowLayout_unit, lifeCycle_unit):


    if allPBEvents is None or len(allPBEvents) == 0:
        print_debug("Error in main: allVideoEvents is None or empty")
        return None;

    if allNavEvents is None or len(allNavEvents) == 0:
        print_debug("Error in main: allNavEvents is None or empty")
        return None;

    heatMap_allUsers = []
    timeSpentArray = []
    completionRateArray = []

    userNames = list(allPBEvents.keys())
    numberOfUsers = len(userNames)
    if numberOfUsers == 0:
        return {}
    videoLength = allPBEvents[userNames[0]][0]["duration"]
    slideInd = allPBEvents[userNames[0]][0]["slideIndex"]

    for i in range(0, len(userNames)):
        if allPBEvents[userNames[i]] == []:
            print_debug("%s has empty events" %userNames[i])
        else:
            print_debug("%s has video of duration %s" %(userNames[i] , allPBEvents[userNames[i]][0]["duration"]))

    # Iterate through each user to see the individual's  allVideoEvents, and get
    for i in range(0, len(userNames)):
        key = userNames[i]

        PBEvents = allPBEvents[key]; # get all the playback events for the current user and only the video of interest
        navEvents = allNavEvents[key]
        s_changed = slideChanged_unit[key]
        s_comp = slideComp_unit[key]
        window = windowLayout_unit[key]
        lifeCycle = lifeCycle_unit[key]

        if PBEvents is None:
            print_debug(" ERROR: VideoEvents is None")
            continue
        if len(PBEvents) == 0:
            print_debug("User %s has empty video events" %key)
            continue;


        # get all the played intervals for this user and this video
        startPositions, interruptPositions, videoID, EventTimeStamp_start, \
        EventTimeStamp_end, duration = get_played_intervals.main_storyline(PBEvents, navEvents, s_changed,
                                                                           s_comp, window,
                                                                           lifeCycle)

        # get this user's video heatmap
        heatMap_single = get_heatMap_forStoryline(startPositions, interruptPositions)



        timeSpent, completionRate = get_timeSpent_and_completionRate_storylineSlide(heatMap_single, duration)
        timeSpentArray.append(timeSpent)
        completionRateArray.append(completionRate)

        if i == 0:
            heatMap_allUsers = heatMap_single;
        else:
            heatMap_allUsers = [a+b for a,b in zip(heatMap_allUsers, heatMap_single)]
    # at this point, heatMap_allUsers contains the number of times each bit of video is viewed, across all users.

    if len(timeSpentArray) == 0:
        total_timespent = 0
        Average_completionRate = 0
    else:
        total_timespent = sum(timeSpentArray)/len(timeSpentArray)
        # Average_completionRate = sum(completionRateArray) / len(completionRateArray)
        Average_completionRate = sum(completionRateArray) / float(numberOfUsers)
        if Average_completionRate > 0 and Average_completionRate < 1:
            Average_completionRate = 1


    table = get_table_storyLine(heatMap_allUsers, numberOfUsers)

    for i in range(0,len(table)):

        timeSpent = (table[i]["end"] - table[i]["start"])*videoLength * table[i]['timesViewed']
        timeSpent = timeSpent / float(numberOfUsers)
        if timeSpent < 1 and timeSpent > 0:
            timeSpent = 1
        else:
            timeSpent = int(round(timeSpent))
        table[i]["timeSpent"] = timeSpent

    output = {
              "totalTimeSpent": int(total_timespent),
              "averageCompletionRate": int(Average_completionRate),
              "views":table
              }

    return output




def get_timeSpent_and_completionRate_storylineSlide(heatMap_single, duration):

    size = len(heatMap_single)
    numberOfZeros = 0
    for i in range(0, size):
        if heatMap_single[i] == 0:
            numberOfZeros = numberOfZeros + 1

    completionRate = int(   (1 - (numberOfZeros/float(size)))*100   ) #size in non-zero

    timeSpent = sum(heatMap_single)  *   float(duration/size)
    #print("///////////  %s   %s"   %(sum(heatMap_single), timeSpent))

    return timeSpent, completionRate



# about the last argument of precision: e.g. precision = 0.1; then there will be round(duration / 0.1) points in the heat map;
def get_heatMap_forStoryline(VideoPlay_StartPosition, VideoPlay_InterruptPosition):

    n = 250
    heatMap = [0] * int(n)  # create the list of zeros


    for i in range(0,len(VideoPlay_StartPosition), 1):


        #todo: why check this? i can never be   >= len(VideoPlay_StartPosition)
        if i >= len(VideoPlay_StartPosition):
            continue
        start =int(  math.floor( VideoPlay_StartPosition[i] * n )    )

        if i >= len(VideoPlay_InterruptPosition):
            continue

        end = int(   math.floor(VideoPlay_InterruptPosition[i] * n )  )

        for ind in range(start, end):
            if ind >= len(heatMap):
                break
            heatMap[ind] = heatMap[ind] + 1

    return heatMap



def get_table_storyLine(heatMap_allUsers, numberOfUsers):
    # using the heatMap_allUsers, we figure out a table with segment start position, segment end position, and number of views of this segment.
    table = []  # in the form of [{start: 100, end: 200, timesViewed: 3}, ...]
    size = len(heatMap_allUsers)
    i = 0
    limit = 0
    while i < size - 1:

        if limit < 1000:
            limit += 1
        else:
            print_debug("while loop hits 1000  i = %s;  size=%s;  %s" %( i,  size, heatMap_allUsers ))
            break

        startIndex = i
        timesViewed = heatMap_allUsers[i]
        endIndex = i
        for j in range(i, size): # this loop finds the end of an interval of the particular timesViewed.
            if heatMap_allUsers[j] != timesViewed:
                endIndex = j - 1
                i = j
                break
            elif j == size - 1:
                endIndex = j
                i = j
                print_debug("got to the end")
            else:
                i += 1


        start = startIndex / float(size);  # from index to position, 1.0 = 100%    # size is non-zero
        end = endIndex / float(size);  # from index to position, 1.0 = 100%
        start = round(start,3); end = round(end,3);
        entry = {"start":start, "end":end, "timesViewed":timesViewed } #numberOfUsers is non-zero
        if entry['end'] > entry['start']:
            table.append(entry)

    return table


#
# def Merge_Intervals(table): # this function checks all intervals in table and merge those with the same timesViewed, if they are such that one contains another.
#     table_tmp = sorted(table, key=lambda k: k['timesViewed']) # sort the intervals by timesViewed, in ascending order.
#
#     # for i in range(0, len(table)):
#     #     print_debug(table_tmp[i])
#
#     i = 0
#
#     limit = 0
#
#     while i < len(table_tmp):
#
#         if limit < 2000:
#             limit += 1
#         else:
#             break
#
#         this_timesViewed = table_tmp[i]['timesViewed']
#         j = i + 1
#         increment = bool(1)
#
#         while j < len(table_tmp):  # iterate through intervals of the same timesViewed
#             #print_debug("i is %s,    j is %s, " %(i,j))
#             if table_tmp[j]['timesViewed'] != this_timesViewed:
#                 #print_debug("skip")
#                 break;
#
#             interval1 = table_tmp[i];
#             interval2 = table_tmp[j];
#
#             if interval1['end'] >= interval2['start'] and interval2['end'] >= interval1['start']: # intervals overlap conditions
#                 newStart = min(interval1['start'], interval2['start']);
#                 newEnd = max(interval1['end'], interval2['end']);
#                 interval1['start'] = newStart;
#                 interval1['end'] = newEnd;
#                 table_tmp.pop(j);
#                 j = j - 1;
#                 increment = bool(0)  # if merge happened, start the outer loop from the i-th again since it is a new interval now.
#             j = j + 1;
#
#         if increment:
#             i = i + 1;
#
#     table = sorted(table_tmp, key=lambda k: k['start']) # sort the intervals by start position, in ascending order.
#     return table
#
