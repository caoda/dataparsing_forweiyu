__author__ = 'dacao'
import os
import datetime
import math
import json
import sys


algo_debug = False

def print_debug(*args):
    if algo_debug is True:
        print(args)



#///////////////////////////////////  define parameters  ///////////////////////////////////////////////////////////////
''' FOR HEAT-MAP: 1% of total number of characters per '1%'; 100 % in total '''
resolution = 0.01
''' if time spent on a section of article is more than this, time spent is set to 0 '''
thres_time = 600 # if time spent on a section of article is more than this, timespent is set to 0
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


''' get the measurements of single article over all users '''
def get_measurements(allArticleEvents, allNavEvents, allWindowLayout, allLifeCycle):
    # allArticleEvents = {
    #        # ID : events
    #        user1: [{}, {}, {},....,{}],   # list contains events from just the article in the one chapter.
    #        user2: [{}, {}, {},....,{}],
    #         ...
    #         }
    #
    #
    # allNavEvents = {
    #        # ID : events
    #        user1: [{}, {}, {},....,{}],   # list contains all nav events for entire course
    #        user2: [{}, {}, {},....,{}],
    #         ...
    #         }
    #
    #  allWindowLayout, allLifeCycle are same as allNavEvents, e.g. events are from the entire course


    # output = {
    #                 averageCompletionRate = 100,
    #                 totalTimeSpent = 121,
    #
    #                 views = [

    #                                 { start = 30,  #  characters
    #                                 end = 70,
    #                                 timeSpent = 99,
    #                                 timesViewed = 2,
    #                                 },
    #                                   ....
    #
    #                          ]
    #
    # }


    numberOfUsers = len(  allArticleEvents.keys()  )
    if numberOfUsers == 0:
        print_debug("there is no user's article events in input.")
        return {}

    # make sure all inputs have the same keys
    inputs = [allArticleEvents, allNavEvents, allWindowLayout, allLifeCycle]
    ref = sorted(inputs[0].keys())

    if len(ref) < 1:
        return -1

    for i in range(1, len(inputs)):
        compare = sorted(inputs[i].keys())
        if len(compare) != len(ref):
            return -1
        for j in range(0, len(ref)):
            if ref[j] != compare[j]:
                return -1




    timeSpentList = []
    completionRateList = []

    [article, chapter] = ["", ""]
    totalChar = -1
    for key in allArticleEvents.keys():

        if len(allArticleEvents[key]) > 0:
            totalChar = allArticleEvents[key][0]["totalCharacters"]
            article = allArticleEvents[key][0]["article"]
            chapter = allArticleEvents[key][0]["chapter"]
            break

    if totalChar == -1:
        return -1


    markSize = resolution * totalChar # num of char in 1 mark; 100 marks in total, represneting 100% of the article
    hm_all_numViews = [0]* int(1.0/resolution)
    hm_all_tSpent = [0]* int(1.0/resolution)


    numActiveUsers = 0
    # iterate thru users to get heatmap for this article
    for userID in allArticleEvents:

        articleEvts = allArticleEvents[userID]

        if len(articleEvts) > 0:
            numActiveUsers += 1

        navEvts = allNavEvents[userID]
        windowEvents = allWindowLayout[userID]
        lifeCycle = allLifeCycle[userID]

        for evt in articleEvts:
            evt['measure'] = 'article'
        for evt in navEvts:
            evt['measure'] = 'navi'
        for evt in windowEvents:
            evt['measure'] = 'windowLayout'
        for evt in lifeCycle:
            evt['measure'] = 'lifeCycle'


        hm_single_tSpent, hm_single_numViews, table_segmentInfo = get_heatMap_singleUser(articleEvts, navEvts,
                                                                                         windowEvents, lifeCycle,
                                                                                                markSize,totalChar)


        timeSpent, completionRate = get_timeSpent_completionRate(hm_single_numViews, table_segmentInfo)

        timeSpentList.append(timeSpent)
        completionRateList.append(completionRate)
        # completionRateList is in 89 (%) instead of 0.89

        hm_all_numViews = [a+b for a,b in zip(hm_all_numViews, hm_single_numViews)]
        hm_all_tSpent = [a+b for a,b in zip(hm_all_tSpent, hm_single_tSpent)]

    table = get_table(hm_all_numViews, numberOfUsers)

    table_timesViewed_tSpent= []


    for i in range(len(table)): # look at each interval of same numViews
        startInd = table[i]["start"]
        tSpent = hm_all_tSpent[startInd]
        for j in range(table[i]["start"]+1, table[i]["end"]+1): # find different timeSpent values and sub-divide
        # interval

            if hm_all_tSpent[j] != tSpent:

                tmp = {"start" : startInd,
                       "end" : j-1,
                       "timesViewed" : table[i]["timesViewed"],
                       "timeSpent" : math.ceil(tSpent)}

                table_timesViewed_tSpent.append(tmp)
                startInd = j
                tSpent = hm_all_tSpent[j]

            if j == table[i]["end"] and hm_all_tSpent[j] == hm_all_tSpent[startInd]:
                tmp = {"start" : startInd,
                       "end" : j,
                       "timesViewed" : table[i]["timesViewed"],
                       "timeSpent" : math.ceil(tSpent)}

                table_timesViewed_tSpent.append(tmp)


    # convert start/end from % to number of char
    # convert "timesViewed" and "timeSpent" from total into average per user
    for item in table_timesViewed_tSpent:
        item["start"] = int(item["start"] * float(totalChar) / 100)
        item["end"] = int(item["end"] * float(totalChar) / 100)


    if numActiveUsers == 0:
        avg_compRate = 0
    else:
        avg_compRate = int(  sum(completionRateList)/float(numberOfUsers)  )
        for item in table_timesViewed_tSpent:
            item['timesViewed'] = int( item['timesViewed'] )
            item['timeSpent'] = math.ceil( (item['timeSpent'] / float(numActiveUsers) ) )

    output = {
              "totalTimeSpent": int(sum(timeSpentList)),
              "averageCompletionRate": avg_compRate,
              "articleLength" : totalChar,
              "article": article,
              "chapter": chapter,
              "views":table_timesViewed_tSpent
              }


    return output

#  End of main function










########## supportive functions#############


def get_time_singleSegment(events):

    startIndex = events[0]['startIndex']
    endIndex = events[0]['startIndex'] + events[0]['visibleCharacters']

    time = events[0]['timestamp']
    time_list = [ time ]
    status = 'on'
    status_list = [status]
    for i in range(1, len(events)):

        if events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'backgroundTab':
            time = events[i]['timestamp']
            status = 'lc_back'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'foregroundTab':
            time = events[i]['timestamp']
            status = 'lc_fore'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'windowLayout':
            status = window_status_for_article(events[i])
            time = events[i]['timestamp']
            time_list.append(time)
            status_list.append(status)

        # these following only happen at the end
        elif events[i]['measure'] == 'navi' and events[i]['action'] == 'close':
            time = events[i]['timestamp']
            status = 'navi_close'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'open':
            time = events[i]['timestamp']
            status = 'lc_open'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'close':
            time = events[i]['timestamp']
            status = 'lc_close'
            time_list.append(time)
            status_list.append(status)


    if len(status_list) == 1:
        return startIndex, endIndex, -1

    check = ["on"]
    start_time = [time_list[0]]
    end_time = []
    for i in range(1, len(status_list)):

        if status_list[i] == 'lc_open' or 'lc_close' or 'navi_close':
            check.append(status_list[i])
            end_time.append(time_list[i])
            break
        else:
            if check_if_cancel(status_list[i], check[-1]):
                check.pop(-1)
            else:
                if status_list[i] == 'lc_back' or 'window_hide':
                    check.append(status_list[i])

            if len(check) == 1:
                start_time.append(time_list[i])
            elif len(check) == 2:
                end_time.append(time_list[i])



    timespent = 0
    for i in range(len(start_time)):
        if (end_time[i] - start_time[i]) >= thres_time:
            end_time[i] = start_time[i]
        timespent = timespent + ( end_time[i] - start_time[i]  )

    return startIndex, endIndex, timespent



def get_heatMap_singleUser(articleEvts, navEvts, windowEvts,  lifeCycleEvts , markSize, totalCharacters):
    # articleEvts = list of events
    # navEvts = list of events
    heatMap_single_tSpent = [0]*int(1.0/resolution)
    heatMap_single_numViews = [0]*int(1.0/resolution)
    table_allsegmentInfo = []

    # create a self-defined list
    new = articleEvts
    if len(articleEvts) > 0:
        for i in range(len(navEvts)):
            if navEvts[i]["timestamp"] > articleEvts[0]["timestamp"]:
                evt = navEvts[i]
                evt["startIndex"] = -1
                new.append(evt)
    for i in range(len(windowEvts)):
        status = window_status_for_article(windowEvts[i])
        windowEvts[i]['status'] = status

    new = new + windowEvts + lifeCycleEvts

    new = sorted(new, key=lambda k:k["timestamp"])

    if len(new) < 1:
        return heatMap_single_tSpent, heatMap_single_numViews, table_allsegmentInfo

    # divide the custom list into segments, seperated by an article event.
    segments = []
    ind = 0

    while ind < len(new) - 1:
        if new[ind]['measure'] == 'article':
            tmp = [new[ind]]
            for i in range(ind+1, len(new)):
                if new[i]['measure'] == 'article':
                    ind = i
                    break
                else:
                    tmp.append(new[i])
                    ind = ind + 1
            segments.append(tmp)
        else:
            ind = ind + 1

    if new[-1]['measure'] == 'article':
        segments.append([new[-1]])


    # go through the segment to figure out each "read" interval
    for i in range(len(segments)):

        startIndex, endIndex, timeSpent = get_time_singleSegment(segments[i])
        startIndex = int( (startIndex/float(totalCharacters)) * 100) # totalCharactors in non zero by this step
        endIndex = int( (endIndex/float(totalCharacters)) * 100 )

        if timeSpent == -1:
            if i != len(segments) - 1:
                timeSpent = segments[i+1][0]['timestamp'] - segments[i][0]['timestamp']
            else:
                timeSpent = 0

        #todo: timespent cannot be more than 1 second per letter.
        if timeSpent > totalCharacters:
            timeSpent = totalCharacters

        #print("%s th segment: start: %s   end: %s    timespent: %s" %(i, startIndex, endIndex, timeSpent) )

        table_allsegmentInfo.append({"start": startIndex, "end": endIndex, "timeSpent":timeSpent})
        for j in range(startIndex, endIndex):
            if len(heatMap_single_tSpent) <= j or len(heatMap_single_numViews) <= j:
                break
            heatMap_single_tSpent[j] = heatMap_single_tSpent[j] + timeSpent
            heatMap_single_numViews[j] = heatMap_single_numViews[j] + 1


    return heatMap_single_tSpent, heatMap_single_numViews, table_allsegmentInfo



def get_timeSpent_completionRate(hm_single_numViews, table_segmentInfo):

    size = len(hm_single_numViews)
    numberOfZeros = 0
    for i in range(0, size):
        if hm_single_numViews[i] == 0:
            numberOfZeros = numberOfZeros + 1
    completionRate = (1 - (numberOfZeros/float(size)))*100    # size is non-zero.

    timeSpent = 0
    for i in range(len(table_segmentInfo)):
        timeSpent = timeSpent + table_segmentInfo[i]["timeSpent"]

    return timeSpent, completionRate




def get_table(hm_all_numViews, numberOfUsers):
    # using the heatMap_allUsers, we figure out a table with segment start position, segment end position, and number of views of this segment.
    table = [];  # in the form of [{start: 100, end: 200, timesViewed: 3}, ...]
    size = len(hm_all_numViews);

    for i in xrange(0, size):

        if i > 0:
            if hm_all_numViews[i] == hm_all_numViews[i-1]: # make sure we skip i with same numViewed
                continue

        startIndex = i
        timesViewed = hm_all_numViews[i];
        endIndex = i;
        for j in range(i+1, size): # this loop finds the end of an interval of the particular timesViewed.
            if hm_all_numViews[j] != timesViewed:
                endIndex = j-1
                break
            elif j == size - 1:
                endIndex = j
                #("got to the end")


        entry = {"start":startIndex, "end":endIndex, "timesViewed":timesViewed}

        if entry['start'] < entry['end']:
            table.append(entry)

    return table


#
# def Merge_Intervals(table): # this function checks all intervals in table and merge those with the same timesViewed, if they are such that one contains another.
#     table_tmp = sorted(table, key=lambda k: k['timesViewed']) # sort the intervals by timesViewed, in ascending order.
#
#     i = 0;
#     while i < len(table_tmp):
#
#         this_timesViewed = table_tmp[i]['timesViewed'];
#         j = i + 1;
#         increment = bool(1)
#
#         while j < len(table_tmp):  # iterate through intervals of the same timesViewed
#             if table_tmp[j]['timesViewed'] != this_timesViewed:
#                 break;
#
#             interval1 = table_tmp[i];
#             interval2 = table_tmp[j];
#
#             if interval1['end'] >= interval2['start'] and interval2['end'] >= interval1['start']: # intervals overlap conditions
#                 newStart = min(interval1['start'], interval2['start']);
#                 newEnd = max(interval1['end'], interval2['end']);
#                 interval1['start'] = newStart;
#                 interval1['end'] = newEnd;
#                 table_tmp.pop(j);
#                 j = j - 1;
#                 increment = bool(0)  # if merge happened, start the outer loop from the i-th again since it is a new interval now.
#             j = j + 1;
#
#         if increment:
#             i = i + 1;
#
#     table = sorted(table_tmp, key=lambda k: k['start']) # sort the intervals by start position, in ascending order.
#     return table
#





def check_if_cancel(new, prev):

    is_cancel = False

    if new == 'lc_fore':
        if prev == 'lc_back':
            is_cancel = True

    elif new == 'window_show':
        if prev == 'window_hide':
            is_cancel = True

    return is_cancel




def window_status_for_article(event): # todo: 'article' is actually 'epub'

    status = 'window_show'
    total_width = event['width']
    total_height = event['height']

    components = event['components']

    for comp in components:
        if comp['componentType'] != 'epub':
            if comp['width'] > total_width - 5:
                if comp['height'] > total_height - 5:
                    return 'window_hide'

        elif comp['componentType'] == 'epub':
            height =  comp['height']
            if height < 40:
                return 'window_hide'


    return status