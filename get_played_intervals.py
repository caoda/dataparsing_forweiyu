__author__ = 'dacao'
import os
import datetime
import math
import json
import sys

range_x = 0.01

def in_close_proximity(value, center):
    if value >= center - range_x and value <= center + range_x:
        return True
    else:
        return False



algo_debug = False
def print_debug(*args):
    if algo_debug is True:
        print(args)




# def state_of_Scrubb(events):
#
#     if len(events) <= 3:
#         return 'pausing'
#
#     return "playing"



'''
allPBEvents:        a list of playback events from specific user in specific chapter
allNavEvents:       a list of navigation events from specific user
slideChanged:       a list of slideChanged events from specific user in specific chapter
slideComp:          a list of slideCompeted events from specific user in specific chapter
allwindowLayout:    a list of windowLayout events from specific user
alllifeCycle:       a list of life cycle events from specific user
'''
def main_storyline(allPBEvents, allNavEvents, allSlideChanged, allSlideComp, allWindowLayout, allLifeCycle):

    PB_sessions = []
    ref_sessionID = ":"
    this = []
    for i in range(len(allPBEvents)):
        if allPBEvents[i]['session'] != ref_sessionID:
            PB_sessions.append(this)
            ref_sessionID = allPBEvents[i]['session']
            this = [allPBEvents[i]]
        else:
            this.append(allPBEvents[i])

    PB_sessions.append(this)
    PB_sessions.pop(0)


    slideID = []
    startPos = []
    interruptPos = []
    start_time = []
    interrupt_time = []
    duration = 0
    for i in range(len(PB_sessions)):

        startPos_tmp, interruptPos_tmp, slideID_tmp, start_time_tmp, interrupt_time_tmp, duration_tmp = \
            intervals_storyline_OneSession(PB_sessions[i], allNavEvents, allSlideChanged, allSlideComp,
                                           allWindowLayout, allLifeCycle)

        slideID = slideID + slideID_tmp
        startPos = startPos + startPos_tmp
        interruptPos = interruptPos + interruptPos_tmp
        start_time = start_time + start_time_tmp
        interrupt_time = interrupt_time + interrupt_time_tmp
        duration = duration_tmp



    return startPos, interruptPos, slideID, start_time, interrupt_time, duration



def intervals_storyline_OneSession(allPBEvents, allNavEvents, allSlideChanged, allSlideComp, allWindowLayout,
                              allLifeCycle): #
# allVideoEvents are ensured to be non-empty and not None

    # the method will handle empty allVideoEvents by returning empty lists and zero values.
    VideoPlay_StartPosition = []
    VideoPlay_InterruptPosition = []
    EventTimeStamp_start = []
    EventTimeStamp_end = []
    videoID = []

    if len(allPBEvents) == 0 or allPBEvents[0]["duration"] == 0:
        return VideoPlay_StartPosition, VideoPlay_InterruptPosition, videoID, EventTimeStamp_start, \
               EventTimeStamp_end, 0

    duration = allPBEvents[0]["duration"]
    initialTimeStamp = allPBEvents[0]["timestamp"]


    PBevents = simplify_PBevents_forStoryline(allPBEvents)


    customEventsList = overall_events_list(PBevents, allNavEvents, allSlideChanged, allSlideComp, allWindowLayout,
                                           allLifeCycle, initialTimeStamp)

    customEventsList.append({ 'type':'fake' })  # if Play/Scrubb/Replay happen to be the last event, ignore it since
    # we cannot tell any information about played interval

    # iterate through the list of events to find all played intervals.

    numEvents = len(customEventsList)
    for index in range(0, numEvents-1):

        evt1 = customEventsList[index]
        evt2 = customEventsList[index+1]
        if evt1["type"] == "Play":
            if evt2["type"] == "Pause":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['position'])  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "End":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['position'])  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(1.0)

            elif evt2["type"] == "Replay":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(0.0)  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "open":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event

                t1 = (1.0 - evt1["position"]) * duration
                t2 = evt2["timestamp"] - evt1["timestamp"]
                if t1 < t2: # assume the user has finished the entire video
                    EventTimeStamp_end.append( evt1["timestamp"] + t1 ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['position'])
                    VideoPlay_InterruptPosition.append(1.0)

                else: # assume the user did not watch at all
                    EventTimeStamp_end.append( evt1["timestamp"] ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['position'])
                    VideoPlay_InterruptPosition.append(evt1['position'])

            elif evt2["type"] == "close":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['position'])  # return position as fraction number, 0 - 1.0
                pos2 = evt1["position"] + ((evt2["timestamp"] - evt1["timestamp"])/duration)
                if pos2 > 1:
                    print_debug("error!!  pos2 > duration")
                VideoPlay_InterruptPosition.append(evt1['position'])

            elif evt2['type'] == 'slideChanged':
                videoID.append(evt1["slide"])
                if evt2['oldSlide'] != evt1['slide']:
                    print_debug("error!!  old Slide: %s  ;  slide ID: %s" %(evt1['slide'], evt2['oldSlide']))
                    print_debug("problem with slide CHanged event: %s" %evt2)
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(evt1['position'])
                VideoPlay_InterruptPosition.append(evt2['oldSlidePos'])


            elif evt2["type"] == "Play":
                print_debug("ERROR #1 !  two continuous Play events with no other events in between!! ")
                # even though this should not happen in theory, in reality it is possible for the web app
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(evt1['position'])
                VideoPlay_InterruptPosition.append(evt2['position'])

        elif evt1["type"] == "Scrubb" and evt1['state'] == 'playing':
            if evt2["type"] == "Pause":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['toPosition'])  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "End":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(evt1['toPosition'])
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "Replay":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['toPosition'])
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "open":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event

                t1 = 1 - evt1["position"]
                t2 = evt2["timestamp"] - evt1["timestamp"]
                if t1 < t2: # assume the user has finished the entire video
                    EventTimeStamp_end.append( evt1["timestamp"] + t1 ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['toPosition'])
                    VideoPlay_InterruptPosition.append(1.0)

                else: # assume the user did not watch at all
                    EventTimeStamp_end.append( evt1["timestamp"] ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['toPosition'])
                    VideoPlay_InterruptPosition.append(evt1['toPosition'])

            elif evt2["type"] == "close":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['toPosition'])
                pos2 = evt1["toPosition"] + ((evt2["timestamp"] - evt1["timestamp"]) / duration)
                if pos2 > 1:
                    print_debug("error!!  pos2 > duration")
                VideoPlay_InterruptPosition.append(evt1['position'])

            elif evt2['type'] == 'slideChanged':
                videoID.append(evt1["slide"])
                if evt2['oldSlide'] != evt1['slide']:
                    print_debug("Error!!  old Slide: %s ;  slide ID: %s" %(evt1['slide'], evt2['oldSlide']))
                    print_debug("problem with slide CHanged event: %s" %evt2)
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(evt1['toPosition'])
                VideoPlay_InterruptPosition.append(evt2['oldSlidePos'])

            elif evt2['type'] == 'Play':
                print_debug("ERROR #2 !  two continuous Play events with no other events in between!! ")
                # even though this should not happen in theory, in reality it is possible for the web app
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(evt1['toPosition'])
                VideoPlay_InterruptPosition.append(evt2['position'])


        elif evt1["type"] == "Replay":
            if evt2["type"] == "Pause":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(0)  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "End":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(0)  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "Replay":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(0)  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position'])

            elif evt2["type"] == "open":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event

                t1 = 1 - evt1["position"]
                t2 = evt2["timestamp"] - evt1["timestamp"]
                if t1 < t2: # assume the user has finished the entire video
                    EventTimeStamp_end.append( evt1["timestamp"] + t1 ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(0)
                    VideoPlay_InterruptPosition.append(1.0)

                else: # assume the user did not watch at all
                    EventTimeStamp_end.append( evt1["timestamp"] ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(0)
                    VideoPlay_InterruptPosition.append(0)

            elif evt2["type"] == "close":
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(0)  # return position as fraction number, 0 - 1.0
                pos2 = evt1["position"] + ((evt2["timestamp"] - 0)/duration)
                if pos2 > 1:
                    print_debug("error!!  pos2 > duration")
                VideoPlay_InterruptPosition.append(0)

            elif evt2['type'] == 'slideChanged':
                videoID.append(evt1["slide"])
                if evt2['oldSlide'] != evt1['slide']:
                    print_debug("error!  old Slide: %s ; slide ID: %s" %(evt1['slide'], evt2['oldSlide']))
                    print_debug("problem with slide CHanged event: %s" %evt2)
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(0)
                VideoPlay_InterruptPosition.append(evt2['oldSlidePos'])
            elif evt2['type'] == 'Play':
                print_debug("ERROR #3 !  two continuous Play events with no other events in between!! ")
                # even though this should not happen in theory, in reality it is possible for the web app
                videoID.append(evt1["slide"])
                EventTimeStamp_start.append(evt1["timestamp"])
                EventTimeStamp_end.append(evt2["timestamp"])
                VideoPlay_StartPosition.append(0)
                VideoPlay_InterruptPosition.append(evt2['position'])


    return VideoPlay_StartPosition, VideoPlay_InterruptPosition, videoID, EventTimeStamp_start, EventTimeStamp_end, duration;


def simplify_PBevents_forStoryline(allPBEvents): # instead having 3 events for each scrubbing, just have one
# scrubbing event with a key-value indicating the state of the scrubbing, 'playing' or 'pausing'

    # allPBEvents is a list of events
    new = allPBEvents
    for i in range(0, len(new)):
        if new[i]["type"] == "Scrubb":
            new[i]['state'] = 'pausing'
            for j in range(i-2, i+3):
                if j < 0 or j > len(new)-1:
                    continue
                if new[j]['type'] == 'Play' and new[j]['position'] == new[i]['toPosition']:
                    new[i]['state'] = 'playing'
                    new[j]["toRemove"] = 1
                #todo: if new[j]['type'] == 'Pause' and new[j]['position'] == new[i]['Position']:
                    #todo: new[j]["toRemove"] = 1
            # state = state_of_Scrubb(evts_sim)
            # new[i]["state"] = state

    ind = 0
    while ind < len(new):
        if "toRemove" in new[ind]:
            new.pop(ind)
        else:
            ind = ind + 1

    return new





def overall_events_list(allmainEvents, allNavEvents, allSlideChanged, allSlideComp, allWindowLayout, allLifeCycle, \
                                                                        initialTimeStamp):
    overall = allmainEvents

    for i in range(0, len(allNavEvents)):
        if allNavEvents[i]["timestamp"] > initialTimeStamp:
            action = allNavEvents[i]["action"]
            timestamp = allNavEvents[i]["timestamp"]
            overall.append(     {"type":action, "timestamp":timestamp, "measure":"nav"}     )

    for i in range(len(allSlideChanged)):
        if allSlideChanged[i]["timestamp"] > initialTimeStamp:
            t = allSlideChanged[i]["timestamp"]
            oldSlideID = allSlideChanged[i]["oldSlide"]
            newSlideID = allSlideChanged[i]["newSlide"]
            oldSlidePos = allSlideChanged[i]['oldSlidePosition']
            overall.append(   {"timestamp":t, 'oldSlide':oldSlideID, 'newSlide':newSlideID,
                               'oldSlidePos':oldSlidePos, 'type':'slideChanged'}   )

    for i in range(len(allSlideComp)):
        if allSlideComp[i]['timestamp'] > initialTimeStamp:
            overall.append( {'timestamp':allSlideComp[i]['timestamp'] , 'slide':allSlideComp[i]['slide'],
                             'type': 'End', 'position':1.0, 'type': 'slideCompleted'}  )


    for i in range(len(allLifeCycle)):
        if allLifeCycle[i]['timestamp'] > initialTimeStamp:
            if allLifeCycle[i]['action'] == 'open' or allLifeCycle[i]['action'] == 'close':
                overall.append( {'timestamp':allLifeCycle[i]['timestamp'] , 'type': allLifeCycle[i]['action'],
                             'measure': 'lifeCycle', 'slide': 'lc_same'}  )


    return sorted(overall, key=lambda k:k["timestamp"])





'''
Above is for storyline
////////////////////////////////////////////////////////////////////////////////
Below is for video
'''


def simplify_events_forVid(allVidEvents):
    # allVidEvents is a list of events
    new = allVidEvents
    # for i in range(0, len(new)):
    #     if new[i]["action"] == "Scrubb":
    #         new[i]['state'] = 'pausing'
    #         for j in range(i-2, i+3):
    #             if j < 0 or j > len(new)-1:
    #                 continue
    #             if new[j]['action'] == 'Play' and in_close_proximity(new[j]['position'], new[i]['toPosition']):
    #                 if j < i:
    #                     new[i]['state'] = 'playing'
    #                     new[j]["toRemove"] = 1
    #             elif new[j]['action'] == 'Pause' and in_close_proximity(new[j]['position'], new[i]['toPosition']):
    #                 new[j]["toRemove"] = 1
    #             elif new[j]['action'] == 'Pause' and in_close_proximity(new[j]['position'], new[i]['position']):
    #                 new[j]["toRemove"] = 1
    #     # delete ShowFullscreen, CloseFullscreen events as they do not affect the intervals.
    #     elif new[i]["action"] == "ShowFullscreen" or new[i]["action"] == "CloseFullscreen":
    #         new[i]['toRemove'] = 1


    for i in range(0, len(new)):
        if new[i]["action"] == "ShowFullscreen" or new[i]["action"] == "CloseFullscreen":
            new[i]['toRemove'] = 1

    new2 = []
    for i in range(len(new)):
        if "toRemove" not in new[i]:
            new2.append(new[i])

    return new2


def intervals_video(allVideoEvents, allNavEvents, allWindow, allLifeCycle): # allVideoEvents are ensured to be
# non-empty and not None
    # allVideoEvents = {"videoID":   , "position":   , "duration":  ,  "action":  , "error":  , "toPosition":   , "chapterID":   }

    # the method will handle empty allVideoEvents by returning empty lists and zero values.
    VideoPlay_StartPosition = []
    VideoPlay_InterruptPosition = []
    EventTimeStamp_start = []
    EventTimeStamp_end = []

    if len(allVideoEvents) == 0 or allVideoEvents[0]["duration"] == 0:
        return VideoPlay_StartPosition, VideoPlay_InterruptPosition, EventTimeStamp_start, EventTimeStamp_end, 0

    duration = allVideoEvents[0]["duration"]
    initialTimeStamp = allVideoEvents[0]["timestamp"]

    #vidEvents = simplify_events_forVid(allVideoEvents)
    vidEvents = allVideoEvents

    # create custom event list that include videoEvents , navEvents and life Cycle Events
    customEventsList = vidEvents
    for i in range(0, len(allLifeCycle)):
        if allLifeCycle[i]['action'] == 'open' or allLifeCycle[i]['action'] == 'close':
            if allLifeCycle[i]['timestamp'] > initialTimeStamp:
                action = allLifeCycle[i]["action"]
                timestamp = allLifeCycle[i]["timestamp"]
                customEventsList.append(     {"action":action, "timestamp":timestamp, }     )

    for i in range(0, len(allNavEvents)):
        if allNavEvents[i]["timestamp"] > initialTimeStamp:
            action = allNavEvents[i]["action"]
            timestamp = allNavEvents[i]["timestamp"]
            customEventsList.append(     {"action":action, "timestamp":timestamp, }     )

    customEventsList = sorted(customEventsList, key=lambda k:k["timestamp"])


    # iterate through the list of events to find all played intervals.
    numEvents = len(customEventsList)
    state = "not started"
    for index in range(0, numEvents-1):

        evt1 = customEventsList[index]
        evt2 = customEventsList[index+1]


        if evt1["action"] == "Play":
            state = "playing"

            if evt2["action"] in ['Pause', 'JumpBookmarkOrNoteDriven', 'Scrubb']:
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['position']/duration)  # return position as fraction number, 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position']/duration)

            elif evt2["action"] == "open":
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event

                t1 = duration - evt1["position"]
                t2 = evt2["timestamp"] - evt1["timestamp"]
                if t1 < t2: # assume the user has finished the entire video
                    EventTimeStamp_end.append( evt1["timestamp"] + t1 ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['position']/duration)
                    VideoPlay_InterruptPosition.append(1.0)

                else: # assume the user did not watch at all
                    EventTimeStamp_end.append( evt1["timestamp"] ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['position']/duration)
                    VideoPlay_InterruptPosition.append(evt1['position']/duration)

            elif evt2["action"] == "close":
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['position']/duration)  # return position as fraction number, 0 - 1.0
                pos2 = evt1["position"] + (evt2["timestamp"] - evt1["timestamp"])
                if pos2 > duration:
                    print_debug("pos2 > duration, assume user watched entire video")
                VideoPlay_InterruptPosition.append(1.0)

            elif evt2["action"] == "Play":
                print_debug("ERROR!  two continuous Play events with no other events in between!!\n%s\n%s" %(evt1, evt2))

        elif evt1["action"] == "Scrubb" and state == 'playing':
            if evt2["action"] in ['Pause', 'JumpBookmarkOrNoteDriven', 'Scrubb']:
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['toPosition']/duration)  # return position as fraction number,
                # 0 - 1.0
                VideoPlay_InterruptPosition.append(evt2['position']/duration)

            elif evt2["action"] == "open":
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event

                t1 = duration - evt1["toPosition"]
                t2 = evt2["timestamp"] - evt1["timestamp"]
                if t1 < t2: # assume the user has finished the entire video
                    EventTimeStamp_end.append( evt1["timestamp"] + t1 ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['toPosition']/duration)
                    VideoPlay_InterruptPosition.append(1.0)

                else: # assume the user did not watch at all
                    EventTimeStamp_end.append( evt1["timestamp"] ) # unix time of the end of the current play
                    VideoPlay_StartPosition.append(evt1['toPosition']/duration)
                    VideoPlay_InterruptPosition.append(evt1['toPosition']/duration)

            elif evt2["action"] == "close":
                EventTimeStamp_start.append(evt1["timestamp"]) # unix time of start of play event
                EventTimeStamp_end.append(evt2["timestamp"]) # unix time of the end of the current play
                VideoPlay_StartPosition.append(evt1['toPosition']/duration)  # return position as fraction number, 0 - 1.0
                pos2 = evt1["toPosition"] + (evt2["timestamp"] - evt1["timestamp"])
                if pos2 > duration:
                    print_debug("pos2 > duration, assume user watched entire video")
                VideoPlay_InterruptPosition.append(1.0)

            elif evt2["action"] == "Play":
                print_debug("ERROR!  two continuous Play events !!\n%s\n%s" %(evt1, evt2))

        elif evt1['action'] == 'Pause':
            state = 'pausing'



    # check that VideoPlay_InterruptPosition[i] >= VideoPlay_StartPosition[i]  for all i
    for i in range(0,len(VideoPlay_StartPosition)):
        if VideoPlay_InterruptPosition[i] < VideoPlay_StartPosition[i]:
            print_debug("ERROR: i = %s: VideoPlay_InterruptPosition[i] < VideoPlay_StartPosition[i]" %i)
    # the above check can be removed later in formal edition.

    for i in xrange(0,len(VideoPlay_StartPosition)):
        print_debug("Start: %s   End: %s" %(VideoPlay_StartPosition[i], VideoPlay_InterruptPosition[i] ))

    return VideoPlay_StartPosition, VideoPlay_InterruptPosition, EventTimeStamp_start, EventTimeStamp_end, duration

