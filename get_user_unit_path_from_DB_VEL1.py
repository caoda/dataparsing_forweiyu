import sys
import os
import MySQLdb
import json
import csv

import functions as func


'''
db name: miicdb_v3
host: 208.112.43.114
usr: miicdb_v3read
pwd: miicdb_v3_r_e_a
'''

SKIP_THRESHOLD = 1.5

DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
cursor = DB.cursor()



def show_full_tables_in_DB():
    tables = []
    cursor.execute("show full tables")
    data = cursor.fetchall()
    for d in data:
        tables.append(d[0])
        print(d[0])
    print("////////////////////////////////  \n\n")


def show_column_names_of_table(table_name):  # 'algo_navigation_currentchapterinfo'
    cursor.execute( "SHOW COLUMNS FROM `%s`" %table_name)
    data = cursor.fetchall()
    for d in data:
        print(d)

    return 0


def get_unit_path_one_user_oldchapterinfo(userID):

    course_id = 'VEL_1'
    cursor.execute( "select * from `algo_navigation_oldchapterinfo` WHERE `courseID` = '%s' and `miic_user_id` = '%s' "  %(course_id, userID) )
    data = cursor.fetchall()
    # 2 - courseID ; 3 - segment_number ;  4 - timestamp ; 5 - index ; 6 - miic_user_id
    # for d in data:
    #     print(d)
    return data


def get_unit_path_one_user_currentchapterinfo(userID):

    course_id = 'VEL_1'
    cursor.execute( "select * from `algo_navigation_currentchapterinfo` WHERE `courseID` = '%s' and `miic_user_id` = '%s' "  %(course_id, userID) )
    data = cursor.fetchall()
    # 2 - courseID ; 3 - segment_number ;  4 - timestamp ; 5 - index ; 6 - miic_user_id
    # for d in data:
    #     print(d)
    return data


def get_unit_path_one_user(userID):

    visited_units = get_unit_path_one_user_oldchapterinfo(userID) + get_unit_path_one_user_currentchapterinfo(userID)
    # 1 - chapterID ; 2 - courseID ; 3 - segment_number ;  4 - timestamp ; 5 - index ; 6 - miic_user_id

    seq_chapterIDs = []
    seq_segNum = []
    seq_timestamp = []
    for item in visited_units:
        seq_chapterIDs.append(  item[1]  )
        seq_segNum.append( item[3] )
        seq_timestamp.append( item[4] )

    return seq_chapterIDs, seq_segNum, seq_timestamp


def filter_out_skips_by_timestamp(seq_chapterIDs, seq_segNum, seq_timestamp):

    if len(seq_segNum) == 0:
        return []

    filtered_segNum = [seq_segNum[0]]
    filtered_timestamp = [seq_timestamp[0]]

    for i in xrange(1, len(seq_chapterIDs)):
        if seq_timestamp[i] - filtered_timestamp[-1] < SKIP_THRESHOLD:
            filtered_segNum[-1] = seq_segNum[i]
            filtered_timestamp[-1] = seq_timestamp[i]
        else:
            filtered_segNum.append( seq_segNum[i] )
            filtered_timestamp.append( seq_timestamp[i] )

    return filtered_segNum



raw_fileName = 'raw_clickstream/measurements-velocity-chess-vel_1.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])

chap_content = {

    'VEL_1_CHAPTER0':[['VEL_1_CONTENT0'], ['pdf']],
    'VEL_1_CHAPTER1':[['VEL_1_CONTENT1'], ['epub']],
    'VEL_1_CHAPTER2':[['VEL_1_CONTENT2'], ['storyline']],
    'VEL_1_CHAPTER3':[['VEL_1_CONTENT3'], ['storyline']],
    'VEL_1_CHAPTER4':[['VEL_1_CONTENT4'], ['storyline']],
    'VEL_1_CHAPTER5':[['VEL_1_CONTENT5'], ['storyline']],
    'VEL_1_CHAPTER6':[['VEL_1_CONTENT6'], ['epud']]
}

clean_events = []
for evt in events:
    if 'chapter' in evt:
        if 'content' in evt and evt['content'] not in chap_content[ evt['chapter'] ][0]:
            continue
        if 'article' in evt and evt['article'] not in chap_content[ evt['chapter'] ][0]:
            continue
    clean_events.append(evt)

events = clean_events
events = sorted(events, key=lambda k: k['timestamp'])
del clean_events

uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
users = sorted(uv.keys())


def main():

    nameSTR = 'user_path.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(['user', 'path'])

    for user in users:
        seq_chapterIDs, seq_segNum, seq_timestamp = get_unit_path_one_user(user)
        filtered_segNum = filter_out_skips_by_timestamp(seq_chapterIDs, seq_segNum, seq_timestamp)
        wr.writerow([user] + filtered_segNum)

    fileName.close()
    print('csv file done!! \n')

    return 0


main()