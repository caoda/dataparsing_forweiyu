
import json
import numpy
import csv



def get(fileName):

    raw = json.load(open(fileName))
    engmt_overall = {}

    title = []
    score = []


    for line in raw['students']:
        if line['id'] not in engmt_overall:
            engmt_overall[ line['id'] ] = line['engagement']['average']

    print('\n\n')


    title.append(raw['id'])
    tmp = {}
    for student in raw['students']:
        tmp[student['id']] = student['engagement']['average']
    score.append(tmp)

    for child in raw['children']:
        title.append(child['id'])
        tmp = {}
        for student in child['students']:
            tmp[student['id']] = student['engagement']['average']
        score.append(tmp)


        for grand_child in child['children']:
            title.append(grand_child['id'])
            tmp = {}
            for student in grand_child['students']:
                tmp[student['id']] = student['engagement']['average']
            score.append(tmp)



    quantiled_scores = []
    for i in xrange(0, len(score)):
        target = score[i].values()

        # get the threshold for top 25% of the scores
        thresh = numpy.percentile(numpy.array(target), 75)

        for j in xrange(0, len(target)):
            if target[j] >= thresh:
                target[j] = 100.0
            else:
                target[j] = (target[j]/float(thresh)) * 100.0

        quantiled_scores.append(target)

    return title, score, quantiled_scores



def main(inFile, outFile):

    title, score, quantiled_scores = get(inFile)
    fileName = open(outFile, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')

    for i in xrange(0, len(title)):
        tmp = []
        tmp.append(title[i])
        for j in xrange(0, len(quantiled_scores[i])):
            tmp.append(quantiled_scores[i][j])

        wr.writerow( tmp )

    fileName.close()

    return 0



fileName102 = "engmt_data_from_dashboard/MAH102_engagement_data.txt"
fileName103 = "engmt_data_from_dashboard/MAH103_engagement_data.txt"
fileName302 = "engmt_data_from_dashboard/WLC302_engagement_data.txt"
fileName305 = "engmt_data_from_dashboard/WLC305_engagement_data.txt"

output102 = 'quantiled_engmt_scores_mah102.csv'
output103 = 'quantiled_engmt_scores_mah103.csv'
output302 = 'quantiled_engmt_scores_wlc302.csv'
output305 = 'quantiled_engmt_scores_wlc305.csv'

main(fileName102, output102)
main(fileName103, output103)
main(fileName302, output302)
main(fileName305, output305)



