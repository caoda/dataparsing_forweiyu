
import json
import csv
import datetime
import unicodedata
import codecs

import functions as func


m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''

def get(events, chap_questions):
    # get list of all chapters, sorted by name
    # get all dates of the course
    chap_names = []

    for evt in events:
        if 'chapter' in evt and evt['chapter'] not in chap_names:
            chap_names.append(evt['chapter'])


    chap_names = sorted(chap_names)

    attr = [  ]
    for ch in chap_names:

        for mode in ['article', 'pdf', 'storyline']:
            str_tmp = ch + '$$' + mode
            attr.append( str_tmp + "$$compRate"  )
            attr.append( str_tmp + "$$timeSpent" )
            attr.append( str_tmp + '$$abs_engagement')
            attr.append( str_tmp + "$$numMaxWindow" )
            attr.append( str_tmp + "$$numMinWindow" )

            if mode == 'storyline':
                attr.append( str_tmp + '$$numPa' )
                attr.append( str_tmp + '$$numSb')
                attr.append( str_tmp + '$$numSf')
                attr.append( str_tmp + '$$numRe')
            if mode == 'pdf' or mode == 'article':
                attr.append( str_tmp + "$$numScroll_up" )
                attr.append( str_tmp + "$$numScroll_down" )

        attr.append( ch + "$$#sign_in")
        attr.append( ch + "$$#Notes" )
        attr.append( ch + "$$#Bookmarks" )
        attr.append( ch + "$$#Highlights")
        attr.append( ch + "$$abs_engagement")
        attr.append( ch + "$$article_existing")
        attr.append( ch + "$$pdf_existing")
        attr.append( ch + "$$storyline_existing")

    attr.append('#sessionCourse')

    return attr, chap_names