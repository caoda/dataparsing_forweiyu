import sys
import os
try:
    user_paths = os.environ['PYTHONPATH']
except KeyError:
    user_paths = []

import MySQLdb

'''
db name: miicdb_v3
host: 208.112.43.114
usr: miicdb_v3read
pwd: miicdb_v3_r_e_a
'''


def get_notes_user_course_content(userID, courseID, contentID):

    DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
    cursor = DB.cursor()

    cursor.execute("select id from `notes_note` WHERE course_id = '%s' AND content_id = '%s' AND user_id = '%s'"
                   %(courseID, contentID, userID) )
    data = cursor.fetchall()

    DB.close()

    return data


def get_text_of_note(noteID):

    DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
    cursor = DB.cursor()

    cursor.execute("select text from `notes_notetext` WHERE `note_id` = '%s' " %noteID)
    text = cursor.fetchall()

    DB.close()
    return text



def get_type_and_path_from_contentID(contentID):

    DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
    cursor = DB.cursor()
    cursor.execute("select * from `course_manager_content` WHERE `id` = '%s'"  %contentID)
    data = cursor.fetchall()
    type = data[0][-3]
    file_path = data[0][-2]
    return type, file_path


def get_contentIDs_in_chapter(chapterID):

    contentIDs_in_chapter = []

    DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
    cursor = DB.cursor()
    cursor.execute("select * from `course_manager_chaptercontent` WHERE `chapter_id` = '%s'"  %chapterID)
    data = cursor.fetchall()

    for item in data:
        contentIDs_in_chapter.append(item[-1])

    return contentIDs_in_chapter



def get_PDF_id_from_storylineID(id):

    DB = MySQLdb.connect(host='208.112.43.114', user='miicdb_v3read', passwd='miicdb_v3_r_e_a' , db = 'miicdb_v3')
    cursor = DB.cursor()
    cursor.execute("select `chapter_id` from `course_manager_chaptercontent` WHERE `content_id` = '%s'"  %id)
    data = cursor.fetchall()
    chapterID = data[0][0]
    print chapterID
    cursor.execute("select * from `course_manager_chaptercontent` WHERE `chapter_id` = '%s'"  %chapterID)
    data = cursor.fetchall()

    pdf_id = 'dummy'
    for item in data:
        if item[2] != id:
            pdf_id = item[2]
            print(pdf_id)

    return pdf_id