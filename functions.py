import csv
import json
import numpy as np
import datetime
import time
import unicodedata
import absolute_engagement_func as AB_eng

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


'''/////////////////////////////////////  supportive func to parse ///////////////////////////////////////////////'''



def user_info_list(users_from_reporting_tool, print_row):

    csvfile = open(users_from_reporting_tool, 'rU')
    reader = csv.reader(csvfile, dialect='excel')

    userInfo = []
    for row in reader:
        givenName = row[0]

        if givenName == 'Tania ':
            row[0] = 'Tania'

        familyName = row[1]
        email = row[2]
        id = row[3]
        userInfo.append(row)

        if print_row:
            print(row)

    if print_row:
        print("\nThere are %s users in %s" %(len(userInfo), users_from_reporting_tool))
        print('**********\n')
    else:
        print("\nThere are %s users in %s. \nExample:" %(len(userInfo), users_from_reporting_tool))
        print(userInfo[0])
        print(userInfo[1])
        print('**********\n')

    return userInfo




def user_pass_fail_info_list(pass_fail_extend_info, print_row):

    passInfo = []
    with open(pass_fail_extend_info) as f:
        content = f.readlines()
        content = content[0].split('\r')

        for row in content:
            line = row.split('\t')
            passInfo.append(line)

            if print_row:
                print(line)

    if print_row:
        print("\nThere are %s users in %s" %(len(passInfo), pass_fail_extend_info))
        print('**********\n')
    else:
        print("\nThere are %s users in %s. \nExample:" %(len(passInfo), pass_fail_extend_info))
        print(passInfo[0])
        print(passInfo[1])
        print('**********\n')

    return passInfo



def get_pass(userID, userInfo, passInfo, keyword_list):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]

            for row in passInfo:
                if family_name in row and given_name in row:
                    if any(i.lower() in keyword_list for i in row):
                        return 1

    return 0


def get_NoPass(userID, userInfo, passInfo, keyword_list):

    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]

            for row in passInfo:
                if family_name in row and given_name in row:
                    if any(i.lower() in keyword_list for i in row):
                        return 1

    return 0


def get_extend(userID, userInfo, passInfo, keyword_list):

    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]

            for row in passInfo:
                if family_name in row:
                    if given_name in row:
                        if any(i.lower() in keyword_list for i in row):
                            return 1

    return 0


def get_expired(userID, userInfo, passInfo, keyword_list):

    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]

            for row in passInfo:
                if family_name in row and given_name in row:
                    if any(i.lower() in keyword_list for i in row):
                        return 1

    return 0






def normail_time_to_unix_timestamp(string_time):   # string_time = '2016_06_14_12_00'

    time_list = string_time.split('_')
    year = int( time_list[0] )
    month = int( time_list[1] )
    day = int( time_list[2] )
    hour = int( time_list[3] )
    minute = int( time_list[4] )

    dt = datetime.datetime(year, month, day, hour, minute)
    timestamp = time.mktime(dt.timetuple())

    return timestamp


def get_course_id(events):
    for evt in events:
        if 'course' in evt:
            print( 'Working on course:  %s'  %evt['course'])
            courseID = evt['course']
            break
    return courseID


def get_evt_from_list(events, measure):
    out = []
    for evt in events:
        if evt['m_type'] == measure:
            out.append(evt)
    return out


def get_uv_dict(events):
    uv = {}
    for evt in events:
        if evt['user'] not in uv:
            uv[ evt['user'] ] = [evt]
        else:
            uv[ evt['user'] ].append(evt)

    for user in uv:
        uv[user] = sorted(uv[user], key=lambda k: k['timestamp'])

    return uv



def make_valid(uv):

    for user in uv: # make each user's clickstream events valid
        results = uv[user]
        results = filter_duplicates(results)
        # results = filter_nonsense(results)
        uv[user] = results

    return uv


def filter_duplicates(events):  # a list of events from the same person

    previous = {'m_type': 'none', 'type':'none', 'timestamp':0.0, 'position':-1000}

    new = []

    for event in events:
        dup = False
        if 'm_type' in event and event['m_type'] == previous['m_type']:


            if 'type' in event:
                if event['type'] == 'Pause' and previous['type'] == 'Play':
                    if event['position'] == previous['position']:
                        if len(new) > 0:
                            new.pop()
                            previous = event
                            continue


            if 'type' in event and event['type'] == previous['type']:
                if 'position' in event and event['position'] == previous['position']:
                    dup = True


        if not dup:
            new.append(event)


        previous = event

    del events

    return new



def filter_nonsense(events):  # a list of events from the same person

    return events



def get_chapEvt_dict_and_lifeCycle(evts): # for just one user.  evts = all events of one user

    chaps = {}
    all_lc = []
    for evt in evts:

        if evt['m_type'] == 'SearchMeasurement':
            continue

        if evt['m_type'] == 'LifeCycleMeasurement':
            all_lc.append(evt)
            continue

        if 'chapter' not in evt:
            continue

        if evt["chapter"] not in chaps:
            chaps[ evt['chapter'] ] = [evt]
        else:
            chaps[ evt['chapter'] ].append( evt )

    return chaps, all_lc



def get_existing_types(events):
    types = []

    for evt in events:
        if evt['m_type'] not in types:
            types.append(evt['m_type'])

    return types


def get_num_sessions(all_evts):

    sessionIDs = []

    for evt in all_evts:
        if evt['session'] not in sessionIDs:
            sessionIDs.append(evt['session'])

    return len(sessionIDs)


def get_numSignIn(nav_evts):

    num = 0
    for evt in nav_evts:
        if evt['action'] == "open":
            num += 1

    return num



def get_counts(storyLine_evts):

    user = storyLine_evts.keys()[0]
    storyLine_evts = storyLine_evts[user]

    [numPa, numSb, numSf, numRe] = [0, 0, 0, 0]

    for evt in storyLine_evts:
        if evt["type"] == "Pause":
            numPa += 1
        elif evt["type"] == "Replay":
            numRe += 1
        elif evt["type"] == "Scrubb":
            if evt["toPosition"] > evt["position"]:
                numSf += 1
            elif evt["toPosition"] < evt["position"]:
                numSb += 1
            #else:
                #print("position == toPosition: %s" %evt)

    return numPa, numSb, numSf, numRe



def get_numMaxMinWindow(window_evts, existing_types):  # window_evts: from just one user and one chapter

    windowInfo = {}

    if m_article in existing_types:
        windowInfo['epub'] = []
    if m_pdf in existing_types:
        windowInfo['pdf'] = []
    if m_video in existing_types:
        windowInfo['video'] = []
    if m_slide in existing_types:
        windowInfo['storyline'] = []

    for evt in window_evts:

        total_width = evt['width']
        total_height = evt['height']

        for comp in evt['components']:

            if comp['width'] > total_width - 5 and comp['height'] > total_height - 5: # this mode is maximized
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['max']
                else:
                    windowInfo[ comp['componentType'] ].append('max')

            elif comp['height'] < 40:  # this mode is minimized
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['min']
                else:
                    windowInfo[ comp['componentType'] ].append('min')

            else: # nothing happens
                if comp['componentType'] not in windowInfo:
                    windowInfo[ comp['componentType'] ] = ['none']
                else:
                    windowInfo[ comp['componentType'] ].append('none')



    for mode in windowInfo:

        if len(windowInfo[mode]) == 0:
            windowInfo[mode] = {}
            windowInfo[mode]['numMax'] = 0
            windowInfo[mode]['numMin'] = 0

        elif len(windowInfo[mode]) == 1:
            if windowInfo[mode][0] == 'max':
                windowInfo[mode] = {'numMax': 1, 'numMin': 0}
            elif windowInfo[mode][0] == 'min':
                windowInfo[mode] = {'numMax': 0, 'numMin': 1}
            else:
                windowInfo[mode] = {'numMax': 0, 'numMin': 0}


        elif len(windowInfo[mode]) > 1:
            numMax, numMin = 0, 0
            if windowInfo[mode][0] == 'max':
                numMax += 1
            elif windowInfo[mode][0] == 'min':
                numMin += 1

            for i in xrange(1, len(windowInfo[mode])):
                if windowInfo[mode][i] == 'max' and windowInfo[mode][i-1] != 'max':
                    numMax += 1
                elif windowInfo[mode][i] == 'min' and windowInfo[mode][i-1] != 'min':
                    numMin += 1

            windowInfo[mode] = {'numMax': numMax, 'numMin': numMin}

    return windowInfo



def get_scroll_count_article(art_evts):
    numScroll_up, numScroll_down = 0, 0

    if len(art_evts) >= 2:
        for i in xrange(1, len(art_evts)):
            if art_evts[i]['startIndex'] > art_evts[i-1]['startIndex']:
                numScroll_down += 1
            elif art_evts[i]['startIndex'] < art_evts[i-1]['startIndex']:
                numScroll_up += 1

    return numScroll_up, numScroll_down



def get_scroll_count_pdf(pdf_evts):

    numScroll_up, numScroll_down = 0, 0

    if len(pdf_evts) >= 2:
        for i in xrange(1, len(pdf_evts)):
            if pdf_evts[i]['page'] > pdf_evts[i-1]['page']:
                numScroll_down += 1
            elif pdf_evts[i]['page'] < pdf_evts[i-1]['page']:
                numScroll_up += 1

    return numScroll_up, numScroll_down



def get_num_notes(evts_thisChap):

    return 0

def get_num_bookmarks(evts_thisChap):

    return 0

def get_num_highlights(evts_thisChap):

    return 0


def attr_dict_to_list(attr, attr_dict):  # attr_dict of this user

    row = []
    for item in attr:
        val = 0 # initiate it to be zero
        x = item.split("$$")

        if x[0][0:10] == 'engagement':
            break

        if len(x) == 3:
            [ch, mode, meas] = x[0:3]
            if ch in attr_dict and mode in attr_dict[ch] and meas in attr_dict[ch][mode]:
                val = attr_dict[ch][mode][meas]
            else:
                val = 0

        elif len(x) == 2:
            [ch, meas] = x[0:2]
            if ch in attr_dict:
                if meas in attr_dict[ch]:
                    val = attr_dict[ch][meas]
                else:
                    val = 0
        elif len(x) == 1:
            meas = x[0]
            if meas in attr_dict:
                val = attr_dict[meas]
            else:
                val = 0
        else:
            print("error with attr:  %s" %item)

        row.append(val)

    return row





def get_content_type(contentID):
    found = False

    with open('course_info/course_manager_content.csv', 'rb') as csvfile:
        spamreader1 = csv.reader(csvfile)

        for row in spamreader1:
            if row[0] == contentID:
                type = row[4]
                found = True
                break

    if not found:
        print('this content ID %s does not have a matching type'  %contentID)
        return -1

    return type



def get_content_title_segNum(contentID):

    found = False

    with open('course_info/course_manager_chapter.csv', 'rb') as csvfile:
        spamreader2 = csv.reader(csvfile)
        # for row in spamreader2:
        #     print('%s  %s  %s' %(row[0], row[1], row[6]))
        for row in spamreader2:
            if row[0] == contentID:
                title = row[1]
                segNum = row[6]
                found = True
                break

    if not found:
        print('this content ID %s does not have a matching titles/segNumber'  %contentID)
        return -1, -1

    return title, int(segNum)


def get_chapter_contents(chapterID):

    found = False
    tmp = []
    with open('course_info/course_manager_chaptercontent.csv', 'rb') as csvfile:
        spamreader3 = csv.reader(csvfile)

        for row in spamreader3:
            if row[1] == chapterID:
                c = row[2]
                tmp.append(c)
                found = True

    if not found:
        print('this chapter ID %s does not have any matching contents'  %chapterID)
        return -1, -1

    return tmp


def get_topic_title_index(topicID):

    found = False
    with open('course_info/course_manager_topic.csv', 'rb') as csvfile:
        spamreader4 = csv.reader(csvfile)
        for row in spamreader4:
            if row[0] == topicID:
                title = row[1]
                index = row[-1]
                found = True

    if not found:
        print('this topic ID %s does not exist'  %topicID)
        return -1, -1

    return title, index


def get_topic_chapters(topicID):

    found = False
    tmp = []

    with open('course_info/course_manager_topicchapter.csv', 'rb') as csvfile:
        spamreader5 = csv.reader(csvfile)
        for row in spamreader5:
            if row[2] == topicID:
                chap = row[1]
                tmp.append(chap)
                found = True

    if not found:
        print('this topic ID %s has no matching chapters'  %topicID)
        return -1

    return tmp


def get_course_topics(courseID):

    found = False
    tmp = []
    with open('course_info/course_manager_coursetopic.csv', 'rb') as csvfile:
        spamreader6 = csv.reader(csvfile)
        for row in spamreader6:
            if row[1] == courseID:
                topic = row[2]
                tmp.append(topic)
                found = True

    if not found:
        print('this course ID %s has no matching topics'  %courseID)
        return -1

    return tmp




def get_quizResets(events, users_list, chapterID):
    new = []
    for evt in events:
        if 'm_type' in evt and evt['m_type'] == 'QuizAnswerMeasurement':
            if evt['user'] in users_list:
                if 'chapter' in evt and evt['chapter'] == chapterID:
                    new.append(evt)

    return new





def get_unique_questions(chapter, events):

    uniqueQ = []
    for evt in events:
        if 'chapter' in evt and evt['chapter'] == chapter:
            if evt['m_type'] == 'QuizAnswerMeasurement':
                if evt['question'] not in uniqueQ:
                    uniqueQ.append(evt['question'])

    return uniqueQ




def get_slides_infos_and_durations(all_slides_play_events_of_one_storyline_from_all_users):

    all_evts = all_slides_play_events_of_one_storyline_from_all_users
    slide_infos = {}
    slideID_durations = {}
    slide_durations_list = []
    dict_index_slideID = {}
    dict_index_slideTitle = {}

    for evt in all_evts:
        if evt['slideIndex'] not in dict_index_slideID:
            dict_index_slideID[ evt['slideIndex'] ] = evt['slide']
        elif evt['slide'] != dict_index_slideID[ evt['slideIndex'] ]:
            print('Error! slideIndex %s points to 2 slideIDs: %s, %s ' %(evt['slideIndex'], evt['slide'], dict_index_slideID[ evt['slideIndex'] ]))


        if evt['slideIndex'] not in dict_index_slideTitle:
            dict_index_slideTitle[ evt['slideIndex'] ] = evt['slideTitle']


        if evt['slide'] not in slideID_durations:
            slideID_durations[ evt['slide'] ] = evt['duration']
        elif evt['duration'] != slideID_durations[ evt['slide'] ]:
            slideID_durations[ evt['slide'] ] = max(slideID_durations[ evt['slide'] ], evt['duration'])
            #print('Error! slide %s has 2 durations: %s, %s ' %(evt['slide'], evt['duration'], slideID_durations[ evt['slide'] ]))

    if len(dict_index_slideID.keys()) > 0:
        sorted_slideIndex = sorted( dict_index_slideID.keys() )
        for i in xrange(0, len(sorted_slideIndex)):
            title = dict_index_slideTitle[ sorted_slideIndex[i] ]
            slide_infos[  dict_index_slideID[sorted_slideIndex[i]]  ] = {'index':i, 'slideTitle':title}

            slide_durations_list.append(  slideID_durations[  dict_index_slideID[sorted_slideIndex[i]]   ]  )


    return slide_infos, slide_durations_list



def get_slides_total_duration(events):
    storyline_events = get_evt_from_list(events, m_slide)
    slidesLens = {}
    for evt in storyline_events:
        ch = evt['chapter']
        ind = evt['slideIndex']
        duration = evt['duration']

        if ch not in slidesLens:
            slidesLens[ ch ] = {}

        if ind not in slidesLens[ ch ]:
            slidesLens[ch][ind] = duration

    output = {}
    for ch in slidesLens:
        if ch not in output:
            output[ch] = 0

        total = 0
        for ind in slidesLens[ch]:
            total += slidesLens[ch][ind]

        output[ch] = total

    return output


def filter_with_timestamp(events, timestamp_filter):
    new = []
    for evt in events:
        if evt['timestamp'] < timestamp_filter:
            new.append(evt)
    events = new
    del new
    return events




def unix_timestamp_to_YYMMDD(timestamp):
    readTime = datetime.datetime.fromtimestamp(int(timestamp))
    y = unicode(readTime.year)
    m = unicode(readTime.month)
    day = unicode(readTime.day)
    y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
    m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
    day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
    date_str = y+'_'+m+'_'+day

    return date_str



def get_num_active_days_and_timespread(all_evts):

    login_dates = []
    active_in_unit_dates = []

    lifecycle_open_timestamps = []
    nav_open_timestamps = []

    for evt in all_evts:
        # login event
        if evt['m_type'] == m_lifecycle and evt['action'] == 'open':
            date_str = unix_timestamp_to_YYMMDD(evt['timestamp'])
            login_dates.append(date_str)
            lifecycle_open_timestamps.append(evt['timestamp'])

        # unit activity #todo: other ways to define activity in unit??
        if evt['m_type'] == m_navi and evt['action'] == 'open':
            date_str = unix_timestamp_to_YYMMDD(evt['timestamp'])
            active_in_unit_dates.append(date_str)
            nav_open_timestamps.append(evt['timestamp'])

    num_days_login = len(set(login_dates))
    num_days_active_in_units = len(set(active_in_unit_dates))

    if len(nav_open_timestamps) > 1:
        timespread_activity_in_units = nav_open_timestamps[-1] - nav_open_timestamps[0]
    else:
        timespread_activity_in_units = 0

    if len(lifecycle_open_timestamps) > 1:
        timespread_login = lifecycle_open_timestamps[-1] - lifecycle_open_timestamps[0]
    else:
        timespread_login = 0

    return num_days_active_in_units, num_days_login, timespread_activity_in_units, timespread_login





