__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np


from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline
from pdf_words_in_courses import *
import absolute_engagement_func as AB_eng
import functions as func
import define_attributes_new_with_abs_engagement
import PBP_feature_parsing_for_one_user_abs_eng as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'




''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017

course_name = 'WLC_302'
session_number = 6
course_name_to_remove_in_feature_name = 'PBP_WLC_302_6_CHAPTER'

''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_wlc_302_6.json'
users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_302_6-from-2016-01-01-to-2016-07-28.csv'
# pass_fail_extend_info = "pass_info/wlc303_4_passInfo.txt"


events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
all_users = sorted(uv.keys())
del uv

courseID = func.get_course_id(events)


''' get the storyline contentIDs '''
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}



''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, dates, chap_existing_modes = define_attributes_new_with_abs_engagement.get(events)


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

csvfile = open(users_from_reporting_tool, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), users_from_reporting_tool))



'''//////////////////////////////////  the pass/extend info ///////////////////////////////////////////'''


def remove_course_name_from_chapterIDs(attr):
    feature_names = []
    for name in attr:
        if len(name) > 21 and name[0:21] == course_name_to_remove_in_feature_name:
            tmp = name[14:]
            feature_names.append(tmp)
        else:
            feature_names.append(name)

    return feature_names


def main(events, time_filter_datetime):

    # file name for output matrix
    output_file_name = 'day_by_day_matrix/wlc302/matrix_WLC302_6_filtered_%s.csv' %time_filter_datetime

    timestamp_filter = func.normail_time_to_unix_timestamp(time_filter_datetime)
    events = func.filter_with_timestamp(events, timestamp_filter)

    nameSTR = output_file_name
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    feature_names = remove_course_name_from_chapterIDs(attr)
    wr.writerow(["user"] + feature_names)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    # users = sorted(uv.keys())


    for user in all_users:

        user_pass = 0
        user_extend = 0
        user_nopass = 0
        user_expired = 0

        row = parsing.core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos, userInfo,
                           user_pass, user_nopass, user_extend, user_expired, course_name, session_number, chap_existing_modes)


        if row == -1:
            continue

        wr.writerow([user] + row)
        print('number of features: %s' %len(row))

    # fileName.close()
    print('%s done!' %output_file_name)


    return 0





time_filter_datetimes = [
                            '2016_07_28_00_00',
                        ]

for time_filter_datetime in time_filter_datetimes:
    main(events, time_filter_datetime)



csv_file = open("day_by_day_matrix/all_users_%s_%s.csv"%(course_name, session_number), "wb")
wr = csv.writer( csv_file, dialect= 'excel')
for user in all_users:
    full_name = []
    for item in userInfo:
        if item[3] == user:
            givenName = item[0]
            familyName = item[1]
            full_name = [user, givenName, familyName]

    if full_name == []:
        continue

    wr.writerow(full_name)

csv_file.close()





