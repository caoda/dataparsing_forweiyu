

import datetime
import time



def normal_time_to_unix_timestamp(string_time):   # string_time = '2016_06_14_12_00'

    time_list = string_time.split('_')
    print(time_list)
    year = int( time_list[0] )
    month = int( time_list[1] )
    day = int( time_list[2] )
    hour = int( time_list[3] )
    minute = int( time_list[4] )

    dt = datetime.datetime(year, month, day, hour, minute)
    print(dt)
    stamp = time.mktime(dt.timetuple())
    print(stamp)

    return stamp



#normal_time_to_unix_timestamp( '2016_06_13_06_00' )



# a = ['a', 'pass', 'b']
# b = ['dan', 'cobb', 'sa@gm', 'PAss']
# print any(i.lower() in a for i in b)


# import parse_MAH102_4_abs_eng_day_by_day, parse_MAH102_6_abs_eng_day_by_day, parse_MAH102_5_abs_eng_day_by_day
#
# parse_MAH102_4_abs_eng_day_by_day.day_by_day()
# parse_MAH102_5_abs_eng_day_by_day.day_by_day()
# parse_MAH102_6_abs_eng_day_by_day.day_by_day()


# expected_time_spent_on_segments = [100, 200, 300, 400, 500, 600, 700]
# print(sum(expected_time_spent_on_segments))


__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np


m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


course_name = 'WLC_305'
session_number = 5
course_name_to_remove_in_feature_name = 'PBP_WLC_305_5_CHAPTER'


def unix_timestamp_to_YYMMDD(timestamp):
    readTime = datetime.datetime.fromtimestamp(int(timestamp))
    y = unicode(readTime.year)
    m = unicode(readTime.month)
    day = unicode(readTime.day)
    y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
    m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
    day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
    date_str = y+'_'+m+'_'+day

    return date_str


''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_wlc_305_5.json'
users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_5-from-2016-01-01-to-2016-06-29.csv'
pass_fail_extend_info = 'pass_info/WLC305_5_Pass_Info.txt'

events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])

print('number of events: ')
print(len(events))

for evt in events:
    if evt['m_type'] == m_lifecycle and evt['action'] == 'open':
        date_str = unix_timestamp_to_YYMMDD(evt['timestamp'])
        print date_str