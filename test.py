import Measurements_article, Measurements_pdf, Measurements_storyline, Measurements_video
import json





def main_pdf():

    chap = "PBP_MAH_102_4_CHAPTER04"
    content = "PBP_MAH_102_4_CONTENT04-1"

    events = json.load(open("dump_oct_14_2.txt"))
    userIDs = []
    for evt in events:
        if evt['user'] not in userIDs:
            userIDs.append(evt['user'])
    #print(userIDs)

    all_events = sorted(events, key=lambda k: k['timestamp'])

    allpdfEvents = {}
    allNavEvents= {}
    allWindow = {}
    allLifeCycle = {}

    for evt in all_events:

        if evt['m_type'] == 'PDFMeasurement'and evt['chapter'] == chap:

            if evt['user'] in allpdfEvents:
                allpdfEvents[ evt['user'] ].append(evt)
            else:
                allpdfEvents[ evt['user'] ] = [evt]

    for evt in all_events:
        if evt['m_type'] == 'NavigationMeasurement' and evt['chapter'] == chap and evt['user'] in allpdfEvents.keys():

            if evt['user'] in allNavEvents:
                allNavEvents[ evt['user'] ].append(evt)
            else:
                allNavEvents[ evt['user'] ] = [evt]



        if evt['m_type'] == 'WindowLayoutMeasurement' and evt['chapter'] == chap and evt['user'] in allpdfEvents.keys():

            if evt['user'] in allWindow:
                allWindow[ evt['user'] ].append(evt)
            else:
                allWindow[ evt['user'] ] = [evt]

    for evt in all_events:
        if evt['m_type'] == 'LifeCycleMeasurement' and evt['user'] in allpdfEvents.keys():

            if evt['user'] in allLifeCycle:
                allLifeCycle[ evt['user'] ].append(evt)
            else:
                allLifeCycle[ evt['user'] ] = [evt]

    for user in allpdfEvents.keys():
        if user not in allLifeCycle:
            allLifeCycle[user] = []
        if user not in allNavEvents:
            allNavEvents[user] = []
        if user not in allWindow:
            allWindow[user] = []

    final = Measurements_pdf.get_measurements(allpdfEvents, allNavEvents, allWindow, allLifeCycle)

    print(final)

    return 0


def main_article():
    brokaw = "ab998f5b-fefa-4f57-ab02-1b95e183abda"
    chap = "PBP_MAH_102_4_CHAPTER03"

    events = json.load(open("dump_oct_14_2.txt"))
    userIDs = []
    for evt in events:
        if evt['user'] not in userIDs:
            userIDs.append(evt['user'])

    #print("%s users: %s" %(len(userIDs), userIDs))

    new = []
    for evt in events:
        if evt["user"] == brokaw:
            new.append(evt)

    all_events = sorted(events, key=lambda k: k['timestamp'])



    allArticleEvents = {}
    allNavEvents= {}
    allWindow = {}
    allLifeCycle = {}

    for evt in all_events:

        if evt['m_type'] == 'ArticleViewPositionMeasurement'and evt['chapter'] == chap:# and evt['article'] == article:

            if evt['user'] in allArticleEvents:
                allArticleEvents[ evt['user'] ].append(evt)
            else:
                allArticleEvents[ evt['user'] ] = [evt]


    for evt in all_events:

        if evt['m_type'] == 'NavigationMeasurement' and evt['chapter'] == chap and evt['user'] in \
                allArticleEvents.keys():

            if evt['user'] in allNavEvents:
                allNavEvents[ evt['user'] ].append(evt)
            else:
                allNavEvents[ evt['user'] ] = [evt]


        if evt['m_type'] == 'WindowLayoutMeasurement' and evt['chapter'] == chap and evt['user'] in \
                allArticleEvents.keys():

            if evt['user'] in allWindow:
                allWindow[ evt['user'] ].append(evt)
            else:
                allWindow[ evt['user'] ] = [evt]

        if evt['m_type'] == 'LifeCycleMeasurement' and evt['user'] in allArticleEvents.keys():

            if evt['user'] in allLifeCycle:
                allLifeCycle[ evt['user'] ].append(evt)
            else:
                allLifeCycle[ evt['user'] ] = [evt]



    # #todo: to delete:  print the events
    # for evt in all_events:
    #
    #     if evt['m_type'] == 'ArticleViewPositionMeasurement'and evt['chapter'] == chap and \
    #             evt['article'] == article:
    #         print("article  %s" %(evt["timestamp"]))
    #
    #     elif evt['m_type'] == 'NavigationMeasurement' and evt['chapter'] == chap and evt['user'] in \
    #             allArticleEvents.keys():
    #
    #         print("navi  %s  %s" %(evt["action"], evt["timestamp"]))
    #
    #     elif evt['m_type'] == 'WindowLayoutMeasurement' and evt['chapter'] == chap and evt['user'] in \
    #             allArticleEvents.keys():
    #
    #         print("window   %s" %(evt["timestamp"]))
    #
    #     elif evt['m_type'] == 'LifeCycleMeasurement' and evt['user'] in allArticleEvents.keys():
    #
    #         print("life  %s  %s" %(evt["action"], evt["timestamp"]))
    #
    #     elif evt['chapter'] == chap and evt['user'] in allArticleEvents.keys():
    #         print("%s  %s"  %(evt["m_type"],  evt["timestamp"]))
    # #todo: delete



    for user in allArticleEvents.keys():
        if user not in allLifeCycle:
            allLifeCycle[user] = []
        if user not in allNavEvents:
            allNavEvents[user] = []
        if user not in allWindow:
            allWindow[user] = []

    final = Measurements_article.get_measurements(allArticleEvents, allNavEvents, allWindow, allLifeCycle)
    print("da")
    print(final)

    return 0


# test video
def main_video():
    events = json.load(open("LTSDEMODUMP_Oct_7.txt"))
    userIDs = []
    for evt in events:
        if evt['user'] not in userIDs:
            userIDs.append(evt['user'])

    print(userIDs)

    checkUser = userIDs[1]

    all_events = sorted(events, key=lambda k: k['timestamp'])



    allVideoEvents = {}
    allNavEvents= {}
    allWindow = {}
    allLifeCycle = {}

    for evt in all_events:

        if evt['m_type'] == 'VideoMeasurement':

            if evt['user'] in allVideoEvents:
                allVideoEvents[ evt['user'] ].append(evt)
            else:
                allVideoEvents[ evt['user'] ] = [evt]


        if evt['m_type'] == 'NavigationMeasurement':

            if evt['user'] in allNavEvents:
                allNavEvents[ evt['user'] ].append(evt)
            else:
                allNavEvents[ evt['user'] ] = [evt]



        if evt['m_type'] == 'WindowLayoutMeasurement':

            if evt['user'] in allWindow:
                allWindow[ evt['user'] ].append(evt)
            else:
                allWindow[ evt['user'] ] = [evt]

    for evt in all_events:
        if evt['m_type'] == 'LifeCycleMeasurement':

            if evt['user'] in allLifeCycle:
                allLifeCycle[ evt['user'] ].append(evt)
            else:
                allLifeCycle[ evt['user'] ] = [evt]

    for user in allVideoEvents.keys():
        if user not in allLifeCycle:
            allLifeCycle[user] = []
        if user not in allNavEvents:
            allNavEvents[user] = []
        if user not in allWindow:
            allWindow[user] = []

    # #print(allVideoEvents.keys())
    # print(allNavEvents.keys())
    # print(allWindow.keys())
    # print(allSlidePBEvents.keys())
    # print(allSlideChanged.keys())
    # print(allSlideComp.keys())
    # print(allLifeCycle.keys())

    final = Measurements_video.get_measurements(allVideoEvents, allNavEvents, allWindow, allLifeCycle)

    print(final)

    return 0

# test storyline
def main_storyline():

    chapter = "PBP_MAH_102_4_CHAPTER04"
    content = "PBP_MAH_102_4_CONTENT04-0"


    events = json.load(open("dump_oct_14_2.txt"))

    all_events = sorted(events, key=lambda k: k['timestamp'])

    allSlidePBEvents = {}
    allSlideChanged = {}
    allSlideComp = {}
    allNavEvents= {}
    allWindow = {}
    allLifeCycle = {}


    for evt in all_events:

        if evt['m_type'] == 'SlidePlaybackEventMeasurement' and evt['chapter'] == chapter:
            if evt['user'] in allSlidePBEvents:
                allSlidePBEvents[ evt['user'] ].append(evt)
            else:
                allSlidePBEvents[ evt['user'] ] = [evt]

    for evt in all_events:

        if evt['m_type'] == 'SlideChangedMeasurement' and evt['chapter'] == chapter and evt['user']\
                in allSlidePBEvents:
            if evt['user'] in allSlideChanged:
                allSlideChanged[ evt['user'] ].append(evt)
            else:
                allSlideChanged[ evt['user'] ] = [evt]


        if evt['m_type'] == 'SlideCompletedMeasurement' and evt['chapter'] == chapter and evt['user'] in allSlidePBEvents:
            if evt['user'] in allSlideComp:
                allSlideComp[ evt['user'] ].append(evt)
            else:
                allSlideComp[ evt['user'] ] = [evt]



        if evt['m_type'] == 'NavigationMeasurement' and evt['chapter'] == chapter and evt['user'] \
                in allSlidePBEvents:
            if evt['user'] in allNavEvents:
                allNavEvents[ evt['user'] ].append(evt)
            else:
                allNavEvents[ evt['user'] ] = [evt]



        if evt['m_type'] == 'WindowLayoutMeasurement' and evt['chapter'] == chapter and evt['user']\
                in allSlidePBEvents:
            if evt['user'] in allWindow:
                allWindow[ evt['user'] ].append(evt)
            else:
                allWindow[ evt['user'] ] = [evt]

    for evt in all_events:
        if evt['m_type'] == 'LifeCycleMeasurement' and evt['user'] in allSlidePBEvents:
            if evt['user'] in allLifeCycle:
                allLifeCycle[ evt['user'] ].append(evt)
            else:
                allLifeCycle[ evt['user'] ] = [evt]


    for user in allSlidePBEvents.keys():
        if user not in allSlideComp:
            allSlideComp[user] = []
        if user not in allSlideChanged:
            allSlideChanged[user] = []
        if user not in allWindow:
            allWindow[user] = []


    final = Measurements_storyline.get_measurements(allSlidePBEvents,
                                                             allNavEvents,
                                                            allSlideChanged,
                                                            allSlideComp,
                                                            allWindow,
                                                            allLifeCycle)


    print("\n")
    print(final)


def main():
    events = json.load(open("LTSDEMODUMP_Oct_7.txt"))
    userIDs = []
    for evt in events:
        if evt['user'] not in userIDs:
            userIDs.append(evt['user'])

    print(userIDs)

    checkUser = userIDs[1]

    all_events = sorted(events, key=lambda k: k['timestamp'])



    allVideoEvents = {}
    allNavEvents= {}
    allWindow = {}
    allLifeCycle = {}

    for evt in all_events:

        if evt['m_type'] == 'VideoMeasurement' and evt['user'] == checkUser:

            print("%s  %s  %s      %s" %(evt['action'],  evt['position'], evt['toPosition'],  evt['duration']))

            if evt['user'] in allVideoEvents:
                allVideoEvents[ evt['user'] ].append(evt)
            else:
                allVideoEvents[ evt['user'] ] = [evt]


        if evt['m_type'] == 'NavigationMeasurement' and evt['user'] == checkUser:

            print("%s  %s " %(evt['m_type'],  evt['action']))

            if evt['user'] in allNavEvents:
                allNavEvents[ evt['user'] ].append(evt)
            else:
                allNavEvents[ evt['user'] ] = [evt]



        if evt['m_type'] == 'WindowLayoutMeasurement' and evt['user'] == checkUser:

            print("%s " %(evt['m_type']))

            if evt['user'] in allWindow:
                allWindow[ evt['user'] ].append(evt)
            else:
                allWindow[ evt['user'] ] = [evt]

    for evt in all_events:
        if evt['m_type'] == 'LifeCycleMeasurement' and evt['user'] == checkUser:

            print("%s  %s " %(evt['m_type'],  evt['action']))

            if evt['user'] in allLifeCycle:
                allLifeCycle[ evt['user'] ].append(evt)
            else:
                allLifeCycle[ evt['user'] ] = [evt]

    for user in allVideoEvents.keys():
        if user not in allLifeCycle:
            allLifeCycle[user] = []
        if user not in allNavEvents:
            allNavEvents[user] = []
        if user not in allWindow:
            allWindow[user] = []

    # #print(allVideoEvents.keys())
    # print(allNavEvents.keys())
    # print(allWindow.keys())
    # print(allSlidePBEvents.keys())
    # print(allSlideChanged.keys())
    # print(allSlideComp.keys())
    # print(allLifeCycle.keys())

    final = Measurements_video.get_measurements(allVideoEvents, allNavEvents, allWindow, allLifeCycle)


    print(final)

    return 0



main_storyline()
print("\n\n\n")
main_pdf()
print("\n\n\n")
main_article()






