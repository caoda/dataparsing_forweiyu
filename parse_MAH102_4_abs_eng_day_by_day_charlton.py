__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

import functions as func
import define_attributes_new_with_abs_engagement
import PBP_feature_parsing_for_one_user_abs_eng_delete as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017

course_name = 'MAH_102'
session_number = 4
course_name_to_remove_in_feature_name = 'PBP_MAH_102_4_CHAPTER'


''' input files '''
raw_events_from_reporting_tool = "raw_clickstream/measurements-pbp-pbp_mah_102_4.txt"
users_from_reporting_tool = "first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_102_4-from-2015-09-01-to-2015-12-03.csv"
pass_fail_extend_info = "pass_info/MAH102_4_Pass.txt"


events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])
uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
all_users = sorted(uv.keys())
courseID = func.get_course_id(events)


# get the storyline contentIDs
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}


# define the attributes
attr, chap_names, allChapNums, dates, chap_existing_modes = define_attributes_new_with_abs_engagement.get(events)


'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary

csvfile = open(users_from_reporting_tool, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), users_from_reporting_tool))

passInfo = []

with open(pass_fail_extend_info) as f:
    content = f.readlines()
    content = content[0].split('\r')
    content.pop(0)

    for row in content:
        line = row.split('\t')
        passInfo.append(line)
        print(line)

print("there are %s users in %s" %(len(passInfo), pass_fail_extend_info))


def get_pass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'PASS' in row:
                        return 1

    return 0


def get_NoPass(userID):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]


            for row in passInfo:
                if row[1] == family_name and row[0] == given_name:
                    if 'NO PASS' in row:
                        return 1

    return 0


'''//////////////////////////////////  the pass/extend info ///////////////////////////////////////////'''




def remove_course_name_from_chapterIDs(attr):
    feature_names = []
    for name in attr:
        if len(name) > 21 and name[0:21] == course_name_to_remove_in_feature_name:
            tmp = name[14:]
            feature_names.append(tmp)
        else:
            feature_names.append(name)

    return feature_names


def main(events, time_filter_datetime):


    timestamp_filter = func.normail_time_to_unix_timestamp(time_filter_datetime)
    events = func.filter_with_timestamp(events, timestamp_filter)



    output_file_name = 'user_vs_chap_timespent_matrixes/%s_%s.csv' %(course_name, session_number)
    fileName = open(output_file_name, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    features = ['user', 'first_name', 'last_name']
    for ch in chap_names:
        features.append( '%s_timespent' %ch )
    features = features + ['Pass', 'No_Pass', 'Extend']
    wr.writerow(features)

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in all_users:

        user_pass = get_pass(user)
        user_nopass = get_NoPass(user)
        print('%s  %s'  %(user, user_nopass))
        user_extend = 0

        row = parsing.core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos, userInfo,
                           user_pass, user_nopass, user_extend, course_name, session_number, chap_existing_modes)


        if row == -1:
            continue

        wr.writerow(row)

    fileName.close()
    print('csv file done!!')

    return 0






time_filter_datetimes = [

                            '2016_10_28_00_00',
                        ]


for time_filter_datetime in time_filter_datetimes:
    main(events, time_filter_datetime)











