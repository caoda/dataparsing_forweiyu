__author__ = 'dacao'
import os
import datetime
import math
import json
import sys
import get_played_intervals

algo_debug = False

def print_debug(*args):
    if algo_debug is True:
        print(args)
        
        

#///////////////////////////////////  define parameters  ///////////////////////////////////////////////////////////////
precision = 0.25  # 0.25 second;  To create heatMap for a video, the video is divided into small bits. A bit for every
# 0.25 seconds.
Thres_minimumSegment = 1 # seconds; segments smaller than this will not be counted in the table.




#/////////////////////////////  The function which will be called ///////////////////////////////////////////////////
def get_measurements(allVideoEvents, allNavEvents, allWindow, allLifeCycle):
    # allVideoEvents = {
    #                   # ID : list of events from just the video of interest
    #                   user1: [{}, {}, {},....,{}],
    #                   user2: [{}, {}, {},....,{}],
    #                       ...
    #                  }
    #
    # allNavEvents = {
    #        # ID : events
    #        user1: [{}, {}, {},....,{}],   # list contains all nav events for entire course
    #        user2: [{}, {}, {},....,{}],
    #         ...
    #         }
    #
    #  allWindowLayout, allLifeCycle are same as allNavEvents, e.g. events are from the entire course
    #
    # output = {
    #
    #                 averageCompletionRate = 100,
    #                 totalTimeSpent = 121212,
    #
    #                 views = [
    #                                 { start = 0,
    #                                 end = 1,
    #                                 timeSpent = 99,
    #                                 timesViewed = 2,
    #                                 }
    #                                 ]
    # }


    # make sure all inputs have the same keys
    inputs = [allVideoEvents, allNavEvents, allWindow, allLifeCycle]
    ref = sorted(inputs[0].keys())
    for i in range(1, len(inputs)):
        compare = sorted(inputs[i].keys())
        if len(compare) != len(ref):
            return -1
        for j in range(0, len(ref)):
            if ref[j] != compare[j]:
                return -1


    heatMap_allUsers = []
    timeSpentArray = []
    completionRateArray = []

    userNames = list(allVideoEvents.keys())
    numberOfUsers = len(userNames)

    if numberOfUsers == 0:  # numberOfUsers must be non-zero
        return {}

    videoLength = 0
    video = ''
    for user in allVideoEvents:
        for event in allVideoEvents[user]:
            if "duration" in event:
                videoLength = event["duration"]
                video = event['video']
                break
        break


    if videoLength == 0:
        return {}


    for i in range(0, numberOfUsers):
        if allVideoEvents[userNames[i]] == []:
            print_debug("%s has empty events" %userNames[i])
        else:
            print_debug("%s has video of duration %s" %(userNames[i] , allVideoEvents[userNames[i]][0]["duration"]))

    # Iterate through each user to see the individual's  allVideoEvents, and get
    for i in range(0, numberOfUsers):
        key = userNames[i]

        videoEvents = allVideoEvents[key]; # get all the playback events for the current user and only the video of interest
        navEvents = allNavEvents[key]
        windowEvents = allWindow[key]
        lifeCycle = allLifeCycle[key]

        if videoEvents is None:
            print_debug(" ERROR: VideoEvents is None")
            continue
        if len(videoEvents) == 0:
            print_debug("User %s has empty video events" %key)
            continue

        # get all the played intervals for this user and this video
        VideoPlay_StartPosition, VideoPlay_InterruptPosition, EventTimeStamp_start, \
        EventTimeStamp_end, duration = get_played_intervals.intervals_video(videoEvents, navEvents, windowEvents, lifeCycle)

        # get this user's video heatmap
        heatMap_single = get_heatMap(VideoPlay_StartPosition, VideoPlay_InterruptPosition, EventTimeStamp_start,
                                     EventTimeStamp_end, videoLength, precision)

        timeSpent, completionRate = get_timeSpent_and_completionRate(heatMap_single)
        timeSpentArray.append(timeSpent)
        completionRateArray.append(completionRate)

        if i == 0:
            heatMap_allUsers = heatMap_single
        else:
            heatMap_allUsers = [a+b for a,b in zip(heatMap_allUsers, heatMap_single)]
    # at this point, heatMap_allUsers contains the number of times each bit of video is viewed, across all users.


    print_debug("\n completionRateArray: %s" %completionRateArray)
    print_debug("\n timeSpentArray: %s" %timeSpentArray)



    if len(timeSpentArray) == 0:
        total_timespent = 0
        average_completionRate = 0
    else:
        numActive1 = 0
        for number in timeSpentArray:
            if number > 0:
                numActive1+=1

        numActive2 = 0
        for number in completionRateArray:
            if number > 0:
                numActive2 += 1

        if numActive2 != numActive1:
            print_debug("ERROR!:  %s    %s"  %(numActive1, numActive2))

        if numActive1 > 0:
            total_timespent = sum(timeSpentArray)/float(numActive1)
        else:
            total_timespent = 0
        	
        if numActive2 > 0:	
            average_completionRate = sum(completionRateArray) / float(numberOfUsers)
        else:
            average_completionRate = 0


    table = get_table(heatMap_allUsers, numberOfUsers)
    i = 0

    limit = 0
    while i < len(table):

        if limit < 1000:
            limit += 1
        else:
            print_debug(("while loop hits 1000  i = %s;  %s;  %s " %( i,   table[i], table )) )
            break

        timeSpent = (table[i]["end"] - table[i]["start"])*videoLength * table[i]['timesViewed']
        table[i]["timeSpent"] = math.ceil(timeSpent)

        table[i]["end"] = int(table[i]["end"] * videoLength)
        table[i]["start"] = int(table[i]["start"] * videoLength)

        if table[i]["start"] == table[i]["end"]:
            table.pop(i)
            i = i - 1

        i = i + 1

    output = {
              "totalTimeSpent": int(total_timespent),
              "averageCompletionRate": int(average_completionRate),
              "duration": videoLength,
              "video": video,
              "views":table
              }

    return output




#///////////////////////   supportive functions   ///////////////////////////////////////////

def get_timeSpent_and_completionRate(heatMap_single):

    size = len(heatMap_single)
    numberOfZeros = 0
    for i in range(0, size):
        if heatMap_single[i] == 0:
            numberOfZeros = numberOfZeros + 1

    completionRate =  int(   (1 - (numberOfZeros/float(size)))*100   )

    timeSpent = sum(heatMap_single) * precision

    return timeSpent, completionRate


def get_table(heatMap_allUsers, numberOfUsers):
    # using the heatMap_allUsers, we figure out a table with segment start position, segment end position, and number of views of this segment.
    table = []  # in the form of [{start: 100, end: 200, timesViewed: 3}, ...]
    #minSeperation = int( math.floor(Thres_minimumSegment / precision) )
    size = len(heatMap_allUsers)
    i = 0

    limit = 0


    while i < size - 1: #- minSeperation:

        if limit < 1000:
            limit += 1
        else:
            print_debug("while loop hits 1000  i = %s;  size=%s;  %s" %( i,  size, heatMap_allUsers ))
            break

        if i > 0:
            if heatMap_allUsers[i] == heatMap_allUsers[i-1]:
                continue

        # if heatMap_allUsers[i] == 0: # skip bits of the video where there is no views by anyone.
        #     continue

        startIndex = i
        timesViewed = heatMap_allUsers[i]
        endIndex = i
        for j in range(i, size): # this loop finds the end of an interval of the particular timesViewed.
            if heatMap_allUsers[j] != timesViewed:
                endIndex = j - 1
                i = j
                break
            elif j == size - 1:
                endIndex = j
                i = j
                print_debug("got to the end")
            else:
                i += 1

        # if (endIndex - startIndex) < minSeperation: # if the segment is smaller than threshold, then do not count it.
        #     continue


        start = startIndex / float(size)  # from index to position, 1.0 = 100%
        end = endIndex / float(size)  # from index to position, 1.0 = 100%
        start = round(start,3)
        end = round(end,3)
        #entry = {"start":start, "end":end, "timesViewed":timesViewed/float(numberOfUsers)}
        entry = {"start":start, "end":end, "timesViewed":timesViewed}
        if entry['end'] > entry['start']:
            table.append(entry)

    return table


# about the last argument of precision: e.g. precision = 0.1; then there will be round(duration / 0.1) points in the heat map;
def get_heatMap(VideoPlay_StartPosition, VideoPlay_InterruptPosition, EventTimeStamp_start, EventTimeStamp_end, duration, precision):

    n = math.floor(duration / precision)  # duration is made sure to be non-zero.
    heatMap = [0] * int(n)  # create the list of zeros

    for i in range(0,len(VideoPlay_StartPosition), 1):
        start =int(  math.floor( VideoPlay_StartPosition[i] * n )    )
        end = int(   math.floor(VideoPlay_InterruptPosition[i] * n )  )
        for ind in range(start, end):
            heatMap[ind] = heatMap[ind] + 1

    return heatMap


# def Merge_Intervals(table): # this function checks all intervals in table and merge those with the same timesViewed, if they are such that one contains another.
#     table_tmp = sorted(table, key=lambda k: k['timesViewed']) # sort the intervals by timesViewed, in ascending order.
#
#
#     i = 0
#
#     limit = 0
#     while i < len(table_tmp):
#
#         if limit < 1000:
#             limit += 1
#         else:
#             print("while loop hit 1000")
#             break
#
#         this_timesViewed = table_tmp[i]['timesViewed']
#         j = i + 1
#         increment = bool(1)
#
#         while j < len(table_tmp):  # iterate through intervals of the same timesViewed
#             if table_tmp[j]['timesViewed'] != this_timesViewed:
#                 #print_debug("skip")
#                 break
#
#             interval1 = table_tmp[i]
#             interval2 = table_tmp[j]
#
#             if interval1['end'] >= interval2['start'] and interval2['end'] >= interval1['start']: # intervals overlap conditions
#                 newStart = min(interval1['start'], interval2['start'])
#                 newEnd = max(interval1['end'], interval2['end'])
#                 interval1['start'] = newStart
#                 interval1['end'] = newEnd
#                 table_tmp.pop(j)
#                 j = j - 1
#                 increment = bool(0)  # if merge happened, start the outer loop from the i-th again since it is a new interval now.
#             j = j + 1
#
#         if increment:
#             i = i + 1
#
#     table = sorted(table_tmp, key=lambda k: k['start']) # sort the intervals by start position, in ascending order.
#     return table
