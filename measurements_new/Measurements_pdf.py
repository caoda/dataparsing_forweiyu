__author__ = 'dacao'
import os
import datetime
import math
import json
import sys


algo_debug = False

def print_debug(*args):
    if algo_debug is True:
        print(args)


#///////////////////////////////////  define parameters  /////////////////////////////////////////
''' if time spent on a section of article is more than this, time spent is set to 0 '''
thres_time = 600 # if time spent on a page of pdf is more than this, timespent is set to 0



'''
allpdf:         { userID : a list of pdf events from specific user in specific chapter , ...}
allNavEvents:     { userID : a list of navigation events from specific user in entire course , ...}
allwindowLayout:     { userID : a list of windowLayout events from specific user in entire course , ...}
alllifeCycle:        { userID : a list of life cycle events from specific user in entire course , ...}
'''
def get_measurements(allpdf, allNavEvents, allWindowLayout, allLifeCycle):
#  result =   {
#               userID:   {
#                                   1    : {"timeSpent": 32.456, "timesViewed": 3},
#                                   2    : {"timeSpent": 45.036, "timesViewed": 5},
#                                       ...
#                                   n    : {"timeSpent": xxx, "timesViewed": x},
#                           },
#
#                 ...
#
# }


# overall_result :{"totalTimeSpent":
# 	               "averageCompletionRate":averageCompletionRate,
#                   "views":[
#                               "page":0,
#                               "timeSpent":10,
#                               "timesViewed":10,
#                           ]
#                  }


    # make sure all inputs have the same keys
    inputs = [allpdf, allNavEvents, allWindowLayout, allLifeCycle]
    ref = sorted(inputs[0].keys())
    if len(ref) < 1:
        return -1, -1
    for i in range(1, len(inputs)):
        compare = sorted(inputs[i].keys())
        if len(compare) != len(ref):
            return -1, -1
        for j in range(0, len(ref)):
            if ref[j] != compare[j]:
                return -1, -1

    numOfUsers = len(allpdf.keys())

    total_pages = -1
    chapter, pdf = '', ''
    for user in allpdf:
        if len(allpdf[user]) > 0:
            for userPDF in allpdf[user]:
                if 'totalPages' in userPDF:
                    total_pages = userPDF['totalPages']
                    chapter = userPDF['chapter']
                    pdf = userPDF['content']
                    if total_pages > 0:
                        break
    if total_pages == -1:
        return -1, -1

    results = {}

    numActiveUsers = 0

    for userID in allpdf:

        if len(allpdf[userID]) > 0:
            numActiveUsers += 1

        for evt in allpdf[userID]:
            evt['measure'] = 'pdf'
        for evt in allNavEvents[userID]:
            evt['measure'] = 'navi'
        for evt in allWindowLayout[userID]:
            evt['measure'] = 'windowLayout'
        for evt in allLifeCycle[userID]:
            evt['measure'] = 'lifeCycle'


        new = allpdf[userID] + allNavEvents[userID] + allWindowLayout[userID] + allLifeCycle[userID]
        new = sorted(new, key=lambda k:k["timestamp"])


        segments = []
        ind = 0

        while ind < len(new)-1:
            if new[ind]['measure'] == 'pdf':
                tmp = [new[ind]]
                for i in range(ind+1, len(new)):
                    if new[i]['measure'] == 'pdf':
                        ind = i
                        break
                    else:
                        tmp.append(new[i])
                        ind = ind + 1
                segments.append(tmp)
            else:
                ind = ind + 1

        if len(new) > 0 and new[-1]['measure'] == 'pdf':
            segments.append([new[-1]])


        output = {}
        for i in range(1, total_pages+1):
            output[i] = {"timeSpent":0, "timesViewed":0}


        for i in range(len(segments)):

            pageNumber, timeSpent = get_time_singlePage(segments[i])

            if timeSpent == -1:
                if i != len(segments)-1:
                    timeSpent = segments[i+1][0]['timestamp'] - segments[i][0]['timestamp']

                else:
                    timeSpent = 0

            if timeSpent > thres_time:
                timeSpent = 0

            if timeSpent > 0:
                try:
                    output[pageNumber]["timeSpent"] = output[pageNumber]["timeSpent"] + timeSpent
                    output[pageNumber]["timesViewed"] = output[pageNumber]["timesViewed"] + 1
                except KeyError:
                    print(total_pages, pageNumber)
        results[userID] = output # this may seem redundant but it is useful in future to get individual's result



    # - averageCompletionRate (PDF measurements have totalPages)
    tmp_list = []
    for user in results:
        num_completedPages = 0
        for page in results[user]:
            if results[user][page]['timesViewed'] > 0:
                num_completedPages += 1
        compRate = num_completedPages / float(total_pages) # total_pages has to be non zero when it get to this step
        tmp_list.append(compRate)




    temp_list = []
    for userID in results:
        temp_list.append(results[userID])
    overall_views = merge_dicts(temp_list)


    # - totalTimeSpent
    totalTimeSpent = 0

    for page in overall_views:
        totalTimeSpent = totalTimeSpent + overall_views[page]['timeSpent']

        overall_views[page]['timeSpent'] = math.ceil(overall_views[page]['timeSpent'])

    if numActiveUsers > 0:
        averageCompletionRate = math.ceil(   ( sum(tmp_list) /  float(numOfUsers)  )*100  )
        for page in overall_views:
            overall_views[page]['timeSpent'] = math.ceil(overall_views[page]['timeSpent'] / float(numActiveUsers))
            overall_views[page]['timesViewed'] =  int( overall_views[page]['timesViewed'] )
    else:
        averageCompletionRate = 0


    overall_result = {"totalTimeSpent":int(totalTimeSpent), "averageCompletionRate":averageCompletionRate,
                      "numPages": total_pages, "pdf": pdf, "chapter":chapter, "views":overall_views}

    if len(overall_views) > 0:
        page_numbers = sorted(  list(overall_result['views'].keys())  )
        lst = []
        for page in page_numbers:
            # add a key "page" to the existing dict overall_views
            tmp = overall_result['views'][page]
            tmp["page"] = page
            lst.append(tmp)
        overall_result['views'] = lst # a list of pages

    #sort the list of pages by page number
    overall_result['views'] = sorted(overall_result['views'], key=lambda k: k['page'])

    return overall_result, results



def merge_dicts(list):
    #returning -1 here would crash the code further above
    if len(list) == 0:
        return []

    overall_result = {}

    for i in range(0, len(list)):
        for page in list[i]:

            if page not in overall_result:
                overall_result[page] = {'timeSpent':0, 'timesViewed':0}

            overall_result[page]['timeSpent'] = overall_result[page]['timeSpent'] + list[i][page]['timeSpent']
            overall_result[page]['timesViewed'] = overall_result[page]['timesViewed'] + list[i][page]['timesViewed']

    return overall_result



def get_time_singlePage(events):
    pageNumber = events[0]['page']

    time = events[0]['timestamp']
    time_list = [ time ]
    status = 'on'
    status_list = [status]
    for i in range(1, len(events)):

        if events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'backgroundTab':
            time = events[i]['timestamp']
            status = 'lc_back'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'foregroundTab':
            time = events[i]['timestamp']
            status = 'lc_fore'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'windowLayout':
            new_status = status_windowLayout(events[i])
            time = events[i]['timestamp']
            status = new_status
            time_list.append(time)
            status_list.append(status)

        # these following only happen at the end
        elif events[i]['measure'] == 'navi' and events[i]['action'] == 'close':
            time = events[i]['timestamp']
            status = 'navi_close'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'open':
            time = events[i]['timestamp']
            status = 'lc_open'
            time_list.append(time)
            status_list.append(status)

        elif events[i]['measure'] == 'lifeCycle' and events[i]['action'] == 'close':
            time = events[i]['timestamp']
            status = 'lc_close'
            time_list.append(time)
            status_list.append(status)


    if len(status_list) == 1:
        return pageNumber, -1


    check = ["on"]
    start_time = [time_list[0]]
    end_time = []
    for i in range(1, len(status_list)):

        if status_list[i] == 'lc_open' or 'lc_close' or 'navi_close':
            check.append(status_list[i])
            end_time.append(time_list[i])
            break
        else:
            if check_if_cancel(status_list[i], check[-1]):
                check.pop(-1)
            else:
                if status_list[i] == 'lc_back' or 'window_hide':
                    check.append(status_list[i])

            if len(check) == 1:
                start_time.append(time_list[i])
            elif len(check) == 2:
                end_time.append(time_list[i])



    timeSpent = 0
    for i in range(len(start_time)):
        timeSpent = timeSpent + ( end_time[i] - start_time[i]  )

    return pageNumber, timeSpent





def check_if_cancel(new, prev):

    is_cancel = False

    if new == 'lc_fore':
        if prev == 'lc_back':
            is_cancel = True

    elif new == 'window_show':
        if prev == 'window_hide':
            is_cancel = True

    return is_cancel



def status_windowLayout(event):

    status = 'window_show'
    total_width = event['width']
    total_height = event['height']

    components = event['components']

    for comp in components:
        if comp['componentType'] != 'pdf':
            if comp['width'] > total_width - 5:
                if comp['height'] > total_height - 5:
                    return 'window_hide'

        elif comp['componentType'] == 'pdf':
            height =  comp['height']
            if height < 40:
                return 'window_hide'

    return status
