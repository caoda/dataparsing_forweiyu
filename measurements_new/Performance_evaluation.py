__author__ = 'dacao'
import os
import datetime
import math
import json
import sys




algo_debug = True

def print_debug(*args):
    if algo_debug is True:
        print(args);
        
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////  define parameters  ///////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////  The function which Ruediger will call ///////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def get_measurements(allAnsweringQuizEvents, numOfQ): # for one chapter
    # allAnsweringQuizEvents = {
    #                           # ID : allAnsweringQuizEvents
    #                           user1: [{}, {}, {},....,{}],   # each {} is a single quiz event. The list contains events from just the quiz answering events of this user.
    #                           user2: [{}, {}, {},....,{}],
    #                               ...
    #                           }
    #
    #
    #  scoreD = {
    #                    userID_1 : {
    #                                 questionID1 : performance score,
    #                                 questionID2 : performance score,
    #                                   ...
    #                                }
    #
    #                    userID_2 : {
    #                                 questionID1 : performance score,
    #                                 questionID2 : performance score,
    #                                   ...
    #                                }
    #
    #                     ....
    #
    #
    #                  }
    #

    # return output = {
    #
    #                 averageCompletionRate = ,
    #                 totalTimeSpent = 121212,
    #
    #                 views = [
    #                                 {
    #                                 questionID = questionID,
    #
    #                                 timeSpent = 99,
    #                                 timesViewed = 2,
    #                                 avgScore = 90,
    #                                 absoluteScore = 99,
    #                                 }
    #                                 ]
    # }
    #


    scoreD = {}
    scoreA = {}
    timeSpentD = {}
    numberOfVisits = {}


    for key in allAnsweringQuizEvents: # go through each user
        allEvents = allAnsweringQuizEvents[key]
        if len(allEvents) > 0:
            if not(key in scoreD):
                scoreD[key] = {}   # initiate the userID in the score dict
            if not (key in scoreA):
                scoreA[key] = {}

            for i in range(0, len(allEvents)): # go through each event of this user
                event = allEvents[i]
                timestamp = event["timestamp"]
                questionID = event["question"]

                timeSpent = event["timeSpent"]
                confidence = event["isSure"]
                selectedAnswers = event["selectedAnswer"]
                correctAnswers = event["correctAnswer"]
                score = event["score"]

                if not(questionID in scoreD[key]): # if this quiz ID is not in the user's dict:
                    # calculate the performance score for this quiz of this user
                    perf, absolute = get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score)
                    scoreD[key][questionID] = [perf, timestamp]
                    scoreA[key][questionID]  = absolute
                else:
                    # if questionID already there, then check which is earlier.
                    if timestamp < scoreD[key][questionID][1]:
                        perf, absolute = get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score)
                        scoreD[key][questionID] = [perf, timestamp]
                        scoreA[key][questionID]  = absolute

                ################################################
                if not(questionID in timeSpentD):
                    timeSpentD[questionID] = timeSpent
                else:
                    timeSpentD[questionID] = timeSpentD[questionID] + timeSpent

                if not(questionID in numberOfVisits):
                    numberOfVisits[questionID] = 1
                else:
                    numberOfVisits[questionID] += 1
                ################################################

    # delete the timestamp info
    for uID in scoreD:
        for qID in scoreD[uID]:
            perf = scoreD[uID][qID][0]
            scoreD[uID][qID] = perf


    views = []
    totalTimeSpent = 0
    for questionID in timeSpentD:
        totalTimeSpent = totalTimeSpent + timeSpentD[questionID]

        totalScore_thisQuiz = 0
        totalAbsoluteScore = 0

        num = 0.0
        for name in scoreD:
            if questionID in scoreD[name]:
                num += 1.0
                totalScore_thisQuiz = totalScore_thisQuiz + scoreD[name][questionID]
                totalAbsoluteScore = totalAbsoluteScore + scoreA[name][questionID]

        avgScore_thisQuiz = totalScore_thisQuiz / num
        totalAbsoluteScore = totalAbsoluteScore / num

        averageTimeSpent = timeSpentD[questionID] / float(numberOfVisits[questionID])

        tmp = {"quizID": questionID, "averageTimeSpent":averageTimeSpent, "avgScore":avgScore_thisQuiz,
               "absoluteScore":totalAbsoluteScore}
        views.append(tmp)


    if numOfQ < 1 or len(scoreA) < 1:
        return {"averageCompletionRate" : 0, "totalTimeSpent":0, "views":views }

    compRates = []
    for user in scoreA:
        numAnswered = len(scoreA[user]) # number of answered Q
        compRate = numAnswered / float(numOfQ)
        compRates.append(compRate)
    averageCompletionRate = int((sum(compRates) / float(len(compRates)) * 100))

    output = {"averageCompletionRate" : averageCompletionRate, "totalTimeSpent":totalTimeSpent,  "views":views}

    return output



def get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score):

    # if confidence:
    #     if selectedAnswers == correctAnswers:
    #         perf = 1.0
    #         absolute = score
    #     else:
    #         perf = 0.0
    #         absolute = 0
    # else:
    #     perf = 0.5
    #     absolute = score / 2.0

    if selectedAnswers == correctAnswers:
        perf = 1.0
        absolute = score
    else:
        perf = 0.0
        absolute = 0

    return perf, absolute