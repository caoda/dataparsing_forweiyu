from operator import itemgetter
from pprint import pprint as pprint
from numpy import sum as np_sum

def values_from_heatmap_timespent_and_views(heat_map_time_spent, heat_map_views, user_count, content_resolution):
    # returns
    # total_timespent,
    # completion,
    # completion_array,
    # views_sum_along_content_axis,
    # time_spent_sum_along_content_axis,
    # user_viewed_array


    if heat_map_time_spent is not None:
        time_spend_sum_along_user_axis = np_sum(heat_map_time_spent, axis=0)
        total_timespent = np_sum(time_spend_sum_along_user_axis, axis=0)

    else:
        total_timespent = None


    # number of items that are not 0
    # check if values are 0 or not, sets non-zero to 1 and then the sum along the 0-axis

    completion_array = (heat_map_views != 0).sum(0)
    # print "completion_array %s" % completion_array

    completion = (completion_array.sum(0) * 100) / (content_resolution * user_count)

    # print "completion %s" % completion

    views_sum_along_content_axis = np_sum(heat_map_views, axis=1)
    # print "views_sum_along_slide_axis %s" % views_sum_along_slide_axis

    if heat_map_time_spent is not None:
        time_spent_sum_along_content_axis = np_sum(heat_map_time_spent, axis=1)
    else:
        time_spent_sum_along_content_axis = None

    # print "time_spent_sum_along_slide_axis %s" % time_spent_sum_along_slide_axis

    # to get the number of users who viewed a particular page
    user_viewed_array = (heat_map_views != 0).sum(1)

    return total_timespent,completion, completion_array, views_sum_along_content_axis, time_spent_sum_along_content_axis, user_viewed_array




def timedics_with_visibility_of_time_interval_dics(timedics, quiz_evts, window_evts, life_cycle_events, navi_events, content_type):
    # returns timedics ({'start':1, 'end':2}) where the content was visible during the interval
    # additionaly returns preprocessed intervalls that can be reused for speedup
    # timedics NEED session!

    background_evts = []

    for item in life_cycle_events:
        if item['action'] == 'backgroundTab' or item['action'] == 'foregroundTab':
            background_evts.append(item)
        else:
            navi_events.append(item)

    # navi and life cycle together give us 'chapter' visits/sessions
    chapter_visits = sorted(navi_events, key=itemgetter('timestamp'))

    quiz_list = create_quiz_interruption_time_dic(quiz_evts)
    fore_back_list = create_background_interruption_time_dic(background_evts)
    win_list = create_window_interruption_time_dic(window_evts, content_type)

    open_navi_evt = None
    open_navi_timestamp = None

    resume_index_timedics = 0
    resume_index_quiz = 0
    resume_index_background = 0
    resume_index_window = 0

    new_timedics = []

    found = False
    for evt in chapter_visits:
        if open_navi_evt is None:
            if evt['m_type'] == 'NavigationMeasurement' and evt['action'] == 'open':
                open_navi_evt = evt
                open_navi_timestamp = evt['timestamp']

            continue

        evt_timestamp = evt['timestamp']
        evt_type = evt['m_type']
        evt_action = evt['action']

        if (evt_type == 'NavigationMeasurement' or evt_type == 'LifeCycleMeasurement') and (evt_action == 'open' or evt_action == 'close'):
            found = True

            relevant_timedics, resume_index_timedics = get_relevant_dics(open_navi_timestamp, evt_timestamp, timedics, resume_index_timedics)

            if len(quiz_list) > 0:
                relevant_quiz, resume_index_quiz = get_relevant_dics(open_navi_timestamp, evt_timestamp, quiz_list, resume_index_quiz)
                relevant_timedics = process_time_dics_for_quiz_back_interruptions(relevant_timedics, relevant_quiz)

            if len(fore_back_list) > 0:
                relevant_background, resume_index_background = get_relevant_dics(open_navi_timestamp, evt_timestamp, fore_back_list, resume_index_background)
                relevant_timedics = process_time_dics_for_quiz_back_interruptions(relevant_timedics, relevant_background)

            if len(win_list) > 0:
                relevant_window, resume_index_window = get_relevant_dics(open_navi_timestamp, evt_timestamp, win_list, resume_index_window)
                relevant_timedics = process_time_dics_for_quiz_back_interruptions(relevant_timedics, relevant_window)

            new_timedics.extend(relevant_timedics)
            open_navi_evt = None

    if found is False:
        # nothing up there matched so we need to work with the original timedics
        new_timedics = timedics

    return new_timedics



def create_quiz_interruption_time_dic(quiz_evts):
    start_evt = None
    quiz_list = []

    if quiz_evts is not None:
        for evt in quiz_evts:
            # here an open is the start of 'invisibility'

            if start_evt is None:
                if evt['action'] == 'open':
                    start_evt = evt
                elif evt['action'] == 'close':
                    # a singular quiz close event
                    quiz_list.append({'end': evt['timestamp']})

            else:
                if evt['action'] == 'open':
                    # a singular quiz open evt
                    if start_evt['session'] != evt['session']:
                        quiz_list.append({'start': start_evt['timestamp']})
                        start_evt = evt

                elif evt['action'] == 'close':
                    if start_evt['session'] == evt['session']:
                        quiz_list.append({'start': start_evt['timestamp'], 'end': evt['timestamp']})
                    else:
                        quiz_list.append({'start': start_evt['timestamp']})
                        quiz_list.append({'end': evt['timestamp']})
                    start_evt = None

    if start_evt is not None:
        quiz_list.append({'start': start_evt['timestamp']})

    return quiz_list



def create_background_interruption_time_dic(background_evts):
    start_evt = None
    fore_back_list = []

    if background_evts is not None:
        for evt in background_evts:
            # here a backgroundTab is a start of 'invisibility'

            if start_evt is None:
                if evt['action'] == 'backgroundTab':
                    start_evt = evt
                elif evt['action'] == 'foregroundTab':
                    # a singular background event
                    fore_back_list.append({'end': evt['timestamp']})

            else:
                if evt['action'] == 'backgroundTab':
                    # a singular background open evt
                    # fore_back_list.append({'start': start_evt['timestamp']})
                    # start_evt = evt
                    continue

                elif evt['action'] == 'foregroundTab':
                    fore_back_list.append({'start': start_evt['timestamp'], 'end': evt['timestamp']})
                    start_evt = None

    if start_evt is not None:
        fore_back_list.append({'start': start_evt['timestamp']})

    return fore_back_list



def create_window_interruption_time_dic(window_evts, content_type):
    # assume visible at the start

    curr_visible = None
    curr_timestamp = None
    win_list = []

    if window_evts is not None:
        for window_idx in xrange(len(window_evts)):
            evt = window_evts[window_idx]
            visible = hidden_or_visible(content_type, evt)

            if curr_visible is False:
                if visible is True:
                    win_list.append({'start': curr_timestamp, 'end': evt['timestamp']})
                    curr_visible = None
                    curr_timestamp = None

            else:
                curr_visible = visible
                curr_timestamp = evt['timestamp']
    if curr_visible is False:
        win_list.append({'start': curr_timestamp})

    return win_list



def get_relevant_dics(start_time, end_time, list_of_dics, start_index):
    # returns only dictionaries that fall into the time interval we need

    filtered_dics = []

    # index to resume future iterations from, becomes startindex

    resume_index = start_index

    if start_index < len(list_of_dics):
        for i in xrange(start_index, len(list_of_dics)):
            item = list_of_dics[i]

            start = item.get('start', None)
            end = item.get('end', None)

            if start is not None and end is not None:
                if start >= end_time:
                    resume_index = i
                    break

                elif start >= start_time and start < end_time:
                    filtered_dics.append(item)
                    resume_index = i + 1

                elif start < start_time and end <= start_time:
                    resume_index = i

            elif start is not None:
                if start >= end_time:
                    resume_index = i
                    break

                elif start >= start_time and start < end_time:
                    filtered_dics.append(item)
                    resume_index = i + 1

                elif start < start_time:
                    resume_index = i + 1

            elif end is not None:
                if end > end_time:
                    resume_index = i
                    break

                elif end < start_time:
                    resume_index = i

                elif end >= start_time and end < end_time:
                    filtered_dics.append(item)
                    resume_index = i + 1

    idx = 0
    while idx < len(filtered_dics):
        interval = filtered_dics[idx]
        if 'start' not in interval:
            if idx == 0:
                filtered_dics[idx]['start'] = start_time
            else:
                filtered_dics[idx-1]['end'] = interval['end']
                del filtered_dics[idx]
                idx -= 1
        elif 'end' not in interval:
            if idx == len(filtered_dics)-1:
                filtered_dics[idx]['end'] = end_time
            else:
                filtered_dics[idx+1]['start'] = interval['start']
                del filtered_dics[idx]
                idx -= 1
        idx += 1

    return filtered_dics, resume_index



def process_time_dics_for_quiz_back_interruptions(timedics, interruptions):
    # only for quiz and background/foreground!

    index = 0
    interruption_start_index = 0

    # while-loop because we are modifying the timedics list!
    while index < len(timedics):
        timedic = timedics[index]
        start = timedic['start']
        end = timedic['end']

        for interruption_index in xrange(interruption_start_index, len(interruptions)):
            invisible = interruptions[interruption_index]
            invisible_start = invisible.get('start', None)
            invisible_end = invisible.get('end', None)

            # skip any interruptions that don't have a start and end or have an end before its start
            if invisible_start is None or invisible_end is None or invisible_end <= invisible_start:
                continue

            # invisible event is ahead of the timeDic window -- s e iS iE
            elif invisible_start >= end:
                interruption_start_index = interruption_index
                break

            # invisible event is fully before the timeDic window -- iS iE s e
            elif invisible_end <= start:
                interruption_start_index = interruption_index + 1
                continue

            # invisible event surrounds the timeDic start -- iS s iE e
            elif invisible_start <= start and invisible_end >= start and invisible_end <= end:
                timedics[index]['start'] = invisible_end
                start = invisible_end
                interruption_start_index = interruption_index + 1

            # invisible event is inside the timeDic -- s iS iE e
            elif invisible_start >= start and invisible_end <= end:
                split_timedic = timedic.copy()
                timedics[index]['end'] = invisible_start
                end = invisible_start
                split_timedic['start'] = invisible_end
                timedics.insert(index+1, split_timedic)
                interruption_start_index = interruption_index + 1

            # invisible event surrounds the timeDic end -- s iS e iE
            elif invisible_start >= start and invisible_start <= end and invisible_end >= end:
                timedics[index]['end'] = invisible_start
                break

            # invisible event surrounds the entire timeDic -- iS s e iE
            elif invisible_start <= start and invisible_end >= end:
                del timedics[index]
                index -= 1
                break

        index += 1

    return timedics



def hidden_or_visible(content_type, window_evt):
    total_width = window_evt['width']
    total_height = window_evt['height']

    for comp in window_evt.get('components', []):
        if comp['componentType'] != content_type:
            if comp['width'] > total_width - 5:
                if comp['height'] > total_height - 5:
                    return False

        else:
            if comp['height'] < 40:
                return False

    return True
