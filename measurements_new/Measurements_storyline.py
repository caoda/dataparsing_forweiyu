
from numpy import zeros as np_zeros

from operator import itemgetter

from measurements_shared import timedics_with_visibility_of_time_interval_dics, values_from_heatmap_timespent_and_views

from time import time

# times the duration of a slide the user can spend
SLIDE_DURATION_FACTOR = 10
DEFAULT_SLIDE_DURATION = 5

def get_measurements(slide_changed_events, slide_completed_events, slide_play_events, slide_infos, nav_events, life_cycle_events,\
                     quiz_open_close_events, window_events, end_timestamp):
    # slide_info contains sorted (by slide_index) list of slide infos [{'slideID':index}] (index: NOT SLIDE INDEX) (used for sorting later)

    keys_to_remove = []

    for usr in slide_play_events:
        # slide play take priority because slides start with it
        if usr is None or slide_play_events[usr] is None or len(slide_play_events[usr]) == 0:
            keys_to_remove.append(usr)

            if usr in slide_completed_events:
                del slide_completed_events[usr]

            if usr in nav_events:
                del nav_events[usr]

            if usr in life_cycle_events:
                del life_cycle_events[usr]

            if usr in slide_changed_events:
                del slide_changed_events[usr]


    for usr in keys_to_remove:
        del slide_play_events[usr]

    user_count = len(slide_completed_events)

    if user_count == 0:
        return None, None

    number_of_slides = len(slide_infos)

    if number_of_slides == 0:
        return None, None

    heat_map_views = np_zeros([number_of_slides, user_count], dtype=int)
    heat_map_time_spent = np_zeros([number_of_slides, user_count], dtype=float)

    for usr_idx, usr in enumerate(slide_play_events):

        u_slide_play_events = slide_play_events[usr]

        if usr in slide_changed_events and slide_changed_events[usr] is not None:
            u_slide_changed_events = slide_changed_events[usr]
        else:
            u_slide_changed_events = []

        if usr in slide_completed_events and slide_completed_events[usr] is not None:
            u_slide_completed_events = slide_completed_events[usr]
        else:
            u_slide_completed_events = []

        if usr in nav_events and nav_events[usr] is not None:
            u_nav_events = nav_events[usr]
        else:
            u_nav_events = []

        if usr in life_cycle_events and life_cycle_events[usr] is not None:
            u_life_cycle_events = life_cycle_events[usr]
        else:
            u_life_cycle_events = []

        if usr in quiz_open_close_events and quiz_open_close_events[usr] is not None:
            u_quiz_open_close_events = quiz_open_close_events[usr]
        else:
            u_quiz_open_close_events = []

        if usr in window_events and window_events[usr] is not None:
            u_window_events = window_events[usr]
        else:
            u_window_events = []

        time_spend_dics = process_events(u_slide_changed_events, u_slide_completed_events, u_slide_play_events,\
                                         u_nav_events, u_life_cycle_events, u_quiz_open_close_events, u_window_events, end_timestamp)

        # apply the internal index
        for dic in time_spend_dics:
            if dic['slide'] in slide_infos:
                dic['index'] = slide_infos[dic['slide']]['index']

        process_in_heat_map(heat_map_views, heat_map_time_spent, number_of_slides, time_spend_dics, usr_idx)



    total_timespent, completion, completion_array, views_sum_along_content_axis, time_spent_sum_along_content_axis, user_viewed_array = \
        values_from_heatmap_timespent_and_views(heat_map_time_spent, heat_map_views, user_count, number_of_slides)



    time_spent_matrix = heat_map_time_spent.tolist()
    view_matrix = heat_map_views.tolist()

    matrices_dic = {'time_spent_matrix':time_spent_matrix, 'view_matrix':view_matrix, 'type':'storyline'}

    output = output_from_values(total_timespent, completion, completion_array, views_sum_along_content_axis,\
                       time_spent_sum_along_content_axis, user_viewed_array, slide_infos, number_of_slides, user_count)

    return output, matrices_dic


def output_from_values(total_timespent, completion, completion_array, views_sum_along_content_axis,\
                       time_spent_sum_along_content_axis, user_viewed_array, slide_infos, number_of_slides, user_count):

    slide_index_title_association_dic = {}
    for slide_id in slide_infos:
        index = slide_infos[slide_id]['index']
        slide_index_title_association_dic[index] = slide_infos[slide_id]['slideTitle']

    views = []

    for i in xrange(0, number_of_slides):

        dic = {}
        dic['start'] = i
        dic['end'] = i + 1

        users_visited = 1
        if i < user_viewed_array.size:
            users_visited = max(user_viewed_array.item(i), 1)

        dic['timeSpent'] = time_spent_sum_along_content_axis.item(i) / float(users_visited)

        dic['timesViewed'] = views_sum_along_content_axis.item(i) #is the same for 1s interval
        dic['title'] = slide_index_title_association_dic[i]
        views.append(dic)

    output = {
            "totalTimeSpent": total_timespent,
            "averageCompletionRate": completion,
            "duration": number_of_slides,
            "views":views,
            "user_count":user_count,
          }

    return output

def process_in_heat_map(heat_map_views, heat_map_time_spent, heat_map_length, time_spend_dics, x):

    for a in time_spend_dics:
        if 'index' in a:
            idx = a['index']
            if idx < heat_map_length:
                heat_map_views[idx, x] += 1
                heat_map_time_spent[idx,x] += a['end'] - a['start']



def process_events(slide_changed_events, slide_completed_events, slide_play_events, nav_events, life_cycle_events,\
                   quiz_open_close_events, window_events, end_timestamp):
    # returns a list of dictionaries with timespent per slide that are used for heatmap

    all_events_list = list(slide_changed_events)
    all_events_list.extend(slide_completed_events)

    all_events_list.extend(slide_play_events)

    all_events_list.extend(nav_events)
    all_events_list.extend(life_cycle_events)

    all_events_list = sorted(all_events_list, key=itemgetter('timestamp'))


    time_spend_dics = []

    play_evt = None
    play_evt_session = None
    play_evt_timestamp = None
    play_evt_duration = None
    play_evt_index = -1

    for index in xrange(0, len(all_events_list)):

        if play_evt is None:
            evt = all_events_list[index]

            # we only have SlidePlayback play events to start from!
            if evt['m_type'] == 'SlidePlaybackEventMeasurement' and evt['type'] == 'Play':
                play_evt = evt
                play_evt_timestamp = evt['timestamp']

                if play_evt_timestamp > end_timestamp:
                    break

                play_evt_session = evt['session']
                play_evt_index = index
                duration = evt['duration']

                if duration == 0:
                    duration = DEFAULT_SLIDE_DURATION

                play_evt_duration = duration
                if index == len(all_events_list) - 1:
                    time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, \
                                            play_evt_timestamp + (play_evt_duration * SLIDE_DURATION_FACTOR),\
                                            play_evt['slide'], play_evt['slideIndex'], play_evt_session))
                continue

            continue


        evt = all_events_list[index]
        evt_timestamp = evt['timestamp']
        evt_type = evt['m_type']

        # if sessions are not the same and if the slide order is not as expected

        if play_evt_session != evt['session']:
            # session is highly inaccurate, so we look for the last slide event
            t = find_timestamp_of_last_relevant_slide_event(play_evt, all_events_list, play_evt_index, index, evt_timestamp)

            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, t,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))

            play_evt = None

        elif evt_type.startswith('Slide') and evaluate_events_for_same_slide(play_evt, evt) == False:
            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, evt_timestamp,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))



            # use background-quiz calculation
            play_evt = None


        elif evt_type == 'SlideCompletedMeasurement':
            # we're recording the time already spent on the slide because it is guaranteed!

            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, evt_timestamp,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))


            # now we can treat continued playing from here
            play_evt = evt
            play_evt_index = index
            play_evt_timestamp = evt_timestamp


        elif evt_type == 'SlideChangedMeasurement':
            # we no longer need to ensure that the slide ids are matching, we did that above
            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, evt_timestamp,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))
            play_evt = None


        elif evt_type == 'NavigationMeasurement' and 'action' in evt and (evt['action'] == 'open' or evt['action'] == 'close'):
            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, evt_timestamp,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))
            play_evt = None



        # in Ruediger's opinion this is the worst possible option
        elif evt_type == 'LifeCycleMeasurement' and  'action' in evt and  (evt['action'] == 'open' or evt['action'] == 'close'):
            t = find_timestamp_of_last_relevant_slide_event(play_evt, all_events_list, play_evt_index, index, evt_timestamp)


            time_spend_dics.append(timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, t,\
                                                                          play_evt['slide'], play_evt['slideIndex'], play_evt_session))


            play_evt = None


        if play_evt is not None and play_evt_timestamp > end_timestamp:
            break

    if play_evt is not None and play_evt_timestamp <= end_timestamp:
        timedic = timedic_based_on_timestamp_differences(play_evt_duration, play_evt_timestamp, end_timestamp,\
                                               play_evt['slide'], play_evt['slideIndex'], play_evt_session)

        if timedic is not None:
            time_spend_dics.append(timedic)


    return timedics_with_visibility_of_time_interval_dics(time_spend_dics, \
                    quiz_open_close_events, window_events, life_cycle_events, nav_events, 'storyline')



def timedic_based_on_timestamp_differences(duration, t1, t2, slide, slideIndex, session):
    # returns dic with timestamps: start, end
    now = time()

    if t2 > now:
        t2 = max(t1+duration, now)

    timedic = { 'slide': slide,
                'slideIndex': slideIndex,
                'session': session,
                'start': t1
                }

    delta = t2 - t1
    max_timespent = duration * SLIDE_DURATION_FACTOR

    if delta > max_timespent:
        timedic['end'] = t1 + max_timespent
    else:
        timedic['end'] = t2
    return timedic




# checks if new slideID is the same or not
def evaluate_events_for_same_slide(evt1, evt2):

    #we want this to return False if keys don't work
    slide1 = None
    slide2 = 1

    if 'slide' in evt1:
        slide1 = evt1['slide']
    elif 'newSlide' in evt1:
        slide1 = evt1['newSlide']

    if 'slide' in evt2:
        slide2 = evt2['slide']

    elif 'oldSlide' in evt2:
        slide2 = evt2['oldSlide']

    return slide1 == slide2





def find_timestamp_of_last_relevant_slide_event(play_evt, evts, play_evt_index, end_index, end_evt_timestamp):


    last_found_evt = None

    for index in xrange(play_evt_index + 1, end_index):
        evt = evts[index]
        evt_type = evt['m_type']

        if evt_type != 'SlidePlaybackEventMeasurement' and evt_type != 'SlideShowLayerMeasurement' and \
                        evt_type != 'SlideLayerCompletedMeasurement' and evt_type != 'SlideShowPopupMeasurement' and\
                        evt_type != 'SlideClosePopupMeasurement':
            continue


        last_found_evt = evt

    if last_found_evt is None:
        return end_evt_timestamp


    return min(last_found_evt['timestamp'], end_evt_timestamp)

