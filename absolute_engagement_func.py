import json
import numpy as np
import math


IMF = 1.0 # instructor mercy level

class UserPdf():

    def __init__(self, alpha_mode, user, chapter, content, heatmap, expected_time_spent_on_segments):

        self.engagementScore = 0
        self.alpha_mode = alpha_mode
        self.alpha_note = 0.01
        self.user = user
        self.chapter = chapter
        self.mode = 'pdf'
        self.contentName = content
        self.heatmap = heatmap
        self.completionRate = 0
        self.noteMeasure_result_value = 1
        self.timeSpent_result_value = 1

        self.actual_time_spent_on_segments = heatmap
        self.expected_time_spent_on_segments = expected_time_spent_on_segments

        self.expected_value_on_note_aspect_text = 1
        self.actual_value_on_note_aspect_text = 1

        self.expected_value_on_note_aspect_image = 1
        self.actual_value_on_note_aspect_image = 1

        self.expected_value_on_note_aspect_audio = 1
        self.actual_value_on_note_aspect_audio = 1

        self.noteText = []
        self.noteImage = []
        self.noteAudio_recording = []


    def weight_of_file(self):
        return sum(self.expected_time_spent_on_segments)


    # def update_heatmap(self, inList):
    #     for i in inList:
    #         if i < len(self.heatmap):
    #             self.heatmap[i] += 1


    def update_notes_measurements(self, value_text, value_image, value_audio):
        self.actual_value_on_note_aspect_text = value_text
        self.actual_value_on_note_aspect_image = value_image
        self.actual_value_on_note_aspect_audio = value_audio


    def calculate_timeSpent_measurements(self):
        tmp = 1
        for i, actual_time_spent in enumerate(self.actual_time_spent_on_segments):
            tmp *= math.pow( (1 + actual_time_spent/float(self.expected_time_spent_on_segments[i])) , self.alpha_mode)
        self.timeSpent_result_value = tmp

    def calculate_timeSpent_measurements_normalizer(self):
        reslut = 1
        for i in xrange(0, len(self.expected_time_spent_on_segments)):
            reslut *= math.pow( 2 , self.alpha_mode )
        return reslut


    def calculate_note_measurements(self):
        tmp1 = math.pow((1 + self.actual_value_on_note_aspect_text / float(self.expected_value_on_note_aspect_text)),
                        self.alpha_note)
        tmp2 = math.pow((1 + self.actual_value_on_note_aspect_image / float(self.expected_value_on_note_aspect_image)),
                        self.alpha_note)
        tmp3 = math.pow((1 + self.actual_value_on_note_aspect_audio / float(self.expected_value_on_note_aspect_audio)),
                        self.alpha_note)
        self.noteMeasure_result_value = tmp1 * tmp2 * tmp3

    def calculate_note_measurements_normalizer(self):
        tmp1 = math.pow(2, self.alpha_note)
        tmp2 = math.pow(2, self.alpha_note)
        tmp3 = math.pow(2, self.alpha_note)
        return tmp1 * tmp2 * tmp3

    def calculate_completion_rate(self):
        viewed = 0
        for i in self.heatmap:
            if i > 0:
                viewed += 1
        self.completionRate = viewed / float(len(self.heatmap))

    def get_engagement_score(self):

        self.calculate_timeSpent_measurements()
        self.calculate_note_measurements()
        self.calculate_completion_rate()
        self.engagementScore = self.completionRate * self.timeSpent_result_value * self.noteMeasure_result_value

        t_normalizer = self.calculate_timeSpent_measurements_normalizer()
        n_normalizer = self.calculate_note_measurements_normalizer()
        normalizer = t_normalizer * n_normalizer * 1.0

        self.engagementScore = (self.engagementScore / float(normalizer)) * 100.0 * IMF
        if self.engagementScore > 100:
            self.engagementScore = 100

        return self.engagementScore


class UserArticle():

    def __init__(self, alpha, user, chapter, content, totalCharacters, heatmap):

        self.completionRate = 0
        self.alpha = alpha
        self.alpha_note = 0.01
        self.user = user
        self.chapter = chapter
        self.mode = 'article'
        self.contentName = content
        self.heatmap = heatmap
        self.default_viewWindow_size = 50 # todo: ??
        self.totalCharacters = totalCharacters
        self.noteMeasure_result_value = 0
        self.timeSpent_result_value = 0

        self.expected_value_on_note_aspect_text = 1
        self.actual_value_on_note_aspect_text = 1

        self.expected_value_on_note_aspect_image = 1
        self.actual_value_on_note_aspect_image = 1

        self.expected_value_on_note_aspect_audio = 1
        self.actual_value_on_note_aspect_audio = 1

        # first calculate the segment size in terms of number of pieces
        frac = self.default_viewWindow_size / float(self.totalCharacters)
        if frac >= 1.0:
            self.segment_size = 100 # all 100 pieces in the heatmap belong to one segment
        else:
            self.segment_size = int(frac * 100)

        self.num_segments = int(math.ceil(100.0 / self.segment_size))

        self.actual_time_spent_on_segments = [0] * self.num_segments
        num_char_in_one_segment = self.totalCharacters * (self.segment_size / 100.0)
        expected_time_spent_on_segment = num_char_in_one_segment / 6.66
        self.expected_time_spent_on_segments = [expected_time_spent_on_segment] * self.num_segments



    def weight_of_file(self):
        return self.totalCharacters / 6.66

    # def update_heatmap(self, inList):
    #     self.heatmap = inList

    def heatmap_to_actual_timeSpent_on_segments(self):

        heatmap_for_segments = []
        for i in xrange(0, self.num_segments):
            heatmap_for_segments.append([])

        for i in xrange(0, len(self.heatmap)):
            segment_ind = i/self.segment_size
            heatmap_for_segments[segment_ind].append( self.heatmap[i] )

        for segment_ind in xrange(0, self.num_segments):
            actual_time_spent = sum( heatmap_for_segments[segment_ind] ) / float(len(heatmap_for_segments[segment_ind]))
            self.actual_time_spent_on_segments[segment_ind] = actual_time_spent


    def calculate_timeSpent_result_value(self):
        tmp = 1
        for i, actual_time_spent in enumerate(self.actual_time_spent_on_segments):

            tmp *= math.pow( (1 + actual_time_spent/float(self.expected_time_spent_on_segments[i])) , self.alpha)

        self.timeSpent_result_value = tmp


    def calculate_note_measurement_result_value(self):
        tmp1 = math.pow((1 + self.actual_value_on_note_aspect_text / float(self.expected_value_on_note_aspect_text)),
                        self.alpha_note)
        tmp2 = math.pow((1 + self.actual_value_on_note_aspect_image / float(self.expected_value_on_note_aspect_image)),
                        self.alpha_note)
        tmp3 = math.pow((1 + self.actual_value_on_note_aspect_audio / float(self.expected_value_on_note_aspect_audio)),
                        self.alpha_note)
        self.noteMeasure_result_value = tmp1 * tmp2 * tmp3



    def calculate_timeSpent_measurements_normalizer(self):
        reslut = 1
        for i in xrange(0, len(self.expected_time_spent_on_segments)):
            reslut *= math.pow( 2 , self.alpha )
        return reslut

    def calculate_note_measurements_normalizer(self):
        tmp1 = math.pow(2, self.alpha_note)
        tmp2 = math.pow(2, self.alpha_note)
        tmp3 = math.pow(2, self.alpha_note)
        return tmp1 * tmp2 * tmp3



    def calculate_completionRate(self):
        viewed = 0
        for i in self.heatmap:
            if i > 0:
                viewed += 1
        self.completionRate = viewed / float(len(self.heatmap))


    def get_engagement_score(self):
        self.heatmap_to_actual_timeSpent_on_segments()
        self.calculate_timeSpent_result_value()
        self.calculate_note_measurement_result_value()
        self.calculate_completionRate()
        self.engagementScore = self.completionRate * self.timeSpent_result_value * self.noteMeasure_result_value

        t_normalizer = self.calculate_timeSpent_measurements_normalizer()
        n_normalizer = self.calculate_note_measurements_normalizer()
        normalizer = t_normalizer * n_normalizer * 1.0

        self.engagementScore = ( self.engagementScore / float(normalizer) ) * 100.0 * IMF

        if self.engagementScore > 100:
            self.engagementScore = 100

        return self.engagementScore


class UserVideo():

    def __init__(self, alpha, user, chapter, mode, content, heatmap):

        self.completionRate = 0
        self.alpha = alpha
        self.user = user
        self.chapter = chapter
        self.mode = mode
        self.contentName = content
        self.heatmap = heatmap

        self.noteMeasure_result_value = 0
        self.timeSpent_result_value = 0


        self.actual_time_spent_on_segments = []
        self.expected_time_spent_on_segments = []

        self.expected_value_on_note_aspect_text = []
        self.actual_value_on_note_aspect_text = []

        self.expected_value_on_note_aspect_image = []
        self.actual_value_on_note_aspect_image = []

        self.expected_value_on_note_aspect_audio = []
        self.actual_value_on_note_aspect_audio = []


    # def update_heatmap(self, inList):
    #     self.heatmap = inList
    #     for i in inList:
    #         if i < len(self.heatmap):
    #             self.heatmap[i] += 1


    def weight_of_file(self):
        return sum(self.expected_time_spent_on_segments)


    def heatmap_to_actual_timeSpent_on_segments(self):

        for i in xrange(0, len(self.heatmap)):

            segment_ind = i/60

            self.actual_time_spent_on_segments[segment_ind] += self.heatmap[i]



    def get_timeSpent_result_value(self):
        tmp = 1

        for i, actual_time_spent in enumerate(self.actual_time_spent_on_segments):

            tmp *= math.pow( (1 + actual_time_spent/float(self.expected_time_spent_on_segments[i])) , self.alpha)

        self.timeSpent_result_value = tmp


    def get_noteMeasurement_result_value(self):

        tmp1 = math.pow((1 + self.actual_value_on_note_aspect_text / float(self.expected_value_on_note_aspect_text)),
                        self.alpha_note)
        tmp2 = math.pow((1 + self.actual_value_on_note_aspect_image / float(self.expected_value_on_note_aspect_image)),
                        self.alpha_note)
        tmp3 = math.pow((1 + self.actual_value_on_note_aspect_audio / float(self.expected_value_on_note_aspect_audio)),
                        self.alpha_note)
        self.noteMeasure_result_value = tmp1 * tmp2 * tmp3

    def calculate_timeSpent_measurements_normalizer(self):
        reslut = 1
        for i in xrange(0, len(self.expected_time_spent_on_segments)):
            reslut *= math.pow( 2 , self.alpha_mode )
        return reslut

    def calculate_note_measurements_normalizer(self):
        tmp1 = math.pow(2, self.alpha_note)
        tmp2 = math.pow(2, self.alpha_note)
        tmp3 = math.pow(2, self.alpha_note)
        return tmp1 * tmp2 * tmp3


    def calculate_completionRate(self):
        viewed = 0
        for i in self.heatmap:
            if i > 0:
                viewed += 1
        self.completionRate = viewed / float(len(self.heatmap))


    def get_engagement_score(self):
        self.calculate_timeSpentMeasurement()
        self.calculate_noteMeasurement()
        self.calculate_completionRate()
        self.engagementScore = self.completionRate * self.timeSpent_result_value * self.noteMeasure_result_value

        t_normalizer = self.calculate_timeSpent_measurements_normalizer()
        n_normalizer = self.calculate_note_measurements_normalizer()
        normalizer = t_normalizer * n_normalizer * 1.0

        self.engagementScore = ( self.engagementScore / float(normalizer) ) * 100.0 * IMF

        if self.engagementScore > 100:
            self.engagementScore = 100

        return self.engagementScore


class UserStoryline():

    def __init__(self, alpha_mode, user, chapter, content, heatmap, expected_time_spent_on_segments):

        self.engagementScore = 0
        self.alpha_mode = alpha_mode
        self.alpha_note = 0.01
        self.user = user
        self.chapter = chapter
        self.mode = 'storyline'
        self.contentName = content
        self.heatmap = heatmap
        self.completionRate = 0
        self.noteMeasure_result_value = 1
        self.timeSpent_result_value = 1

        self.actual_time_spent_on_segments = heatmap
        self.expected_time_spent_on_segments = expected_time_spent_on_segments

        self.expected_value_on_note_aspect_text = 1
        self.actual_value_on_note_aspect_text = 1

        self.expected_value_on_note_aspect_image = 1
        self.actual_value_on_note_aspect_image = 1

        self.expected_value_on_note_aspect_audio = 1
        self.actual_value_on_note_aspect_audio = 1

        self.noteText = []
        self.noteImage = []
        self.noteAudio_recording = []


    # def update_heatmap(self, inList):
    #     for i in inList:
    #         if i < len(self.heatmap):
    #             self.heatmap[i] += 1


    def weight_of_file(self):
        return sum(self.expected_time_spent_on_segments)


    def update_notes_measurements(self, value_text, value_image, value_audio):
        self.actual_value_on_note_aspect_text = value_text
        self.actual_value_on_note_aspect_image = value_image
        self.actual_value_on_note_aspect_audio = value_audio


    def calculate_timeSpentMeasurement(self):
        tmp = 1
        for i, actual_time_spent in enumerate(self.actual_time_spent_on_segments):
            tmp *= math.pow( (1 + actual_time_spent/float(self.expected_time_spent_on_segments[i])) , self.alpha_mode)
        self.timeSpent_result_value = tmp


    def calculate_noteMeasurement(self):
        tmp1 = math.pow((1 + self.actual_value_on_note_aspect_text / float(self.expected_value_on_note_aspect_text)),
                        self.alpha_note)
        tmp2 = math.pow((1 + self.actual_value_on_note_aspect_image / float(self.expected_value_on_note_aspect_image)),
                        self.alpha_note)
        tmp3 = math.pow((1 + self.actual_value_on_note_aspect_audio / float(self.expected_value_on_note_aspect_audio)),
                        self.alpha_note)
        self.noteMeasure_result_value = tmp1 * tmp2 * tmp3


    def calculate_timeSpent_measurements_normalizer(self):
        reslut = 1
        for i in xrange(0, len(self.expected_time_spent_on_segments)):
            reslut *= math.pow( 2 , self.alpha_mode )
        return reslut

    def calculate_note_measurements_normalizer(self):
        tmp1 = math.pow(2, self.alpha_note)
        tmp2 = math.pow(2, self.alpha_note)
        tmp3 = math.pow(2, self.alpha_note)
        return tmp1 * tmp2 * tmp3


    def calculate_completionRate(self):
        viewed = 0
        for i in self.heatmap:
            if i > 0:
                viewed += 1
        self.completionRate = viewed / float(len(self.heatmap))


    def get_engagement_score(self):

        self.calculate_timeSpentMeasurement()
        self.calculate_noteMeasurement()
        self.calculate_completionRate()
        self.engagementScore = self.completionRate * self.timeSpent_result_value * self.noteMeasure_result_value

        t_normalizer = self.calculate_timeSpent_measurements_normalizer()
        n_normalizer = self.calculate_note_measurements_normalizer()
        normalizer = t_normalizer * n_normalizer * 1.0

        self.engagementScore = (self.engagementScore / float(normalizer)) * 100.0 * IMF
        if self.engagementScore > 100:
            self.engagementScore = 100
        return self.engagementScore


class UserCourse():

    def __init__(self, course_id, course_structure):
        self.course_id = course_id
        self.course_structure = course_structure

        self.unit_to_topic_relation = {}
        for topic_id in self.course_structure['topics']:
            for unit_id in self.course_structure['topics'][topic_id]['units']:
                self.unit_to_topic_relation[unit_id] = topic_id

    def aggregate_one_level_up(self, engagement_score_list, weight_list):

        aggr_weight = sum(weight_list)
        aggr_engagement_score = 0

        for i, eng_score in enumerate(engagement_score_list):
            weight = weight_list[i]
            aggr_engagement_score += weight * eng_score

        if aggr_weight == 0:
            aggr_weight = 1
        aggr_engagement_score = aggr_engagement_score / float(aggr_weight)

        return aggr_engagement_score, aggr_weight


    def get_engagement_scores_and_weights_content_level(self, topic_id, unit_id):
        eng_score_list = []
        weight_list = []
        for content_id in self.course_structure['topics'][topic_id]['units'][unit_id]['contents']:
            eng_score_list.append(self.course_structure['topics'][topic_id]['units'][unit_id]['contents'][content_id]['engagement'])
            weight_list.append(self.course_structure['topics'][topic_id]['units'][unit_id]['contents'][content_id]['weight'])

        return eng_score_list, weight_list


    def get_engagement_scores_and_weights_unit_level(self, topic_id):
        eng_score_list = []
        weight_list = []
        for unit_id in self.course_structure['topics'][topic_id]['units']:
            eng_score_list.append(self.course_structure['topics'][topic_id]['units'][unit_id]['engagement'])
            weight_list.append(self.course_structure['topics'][topic_id]['units'][unit_id]['unit_weight'])

        return eng_score_list, weight_list



    def aggregate_contents_to_unit(self, unit_id):
        topic_id = self.unit_to_topic_relation[unit_id]
        eng_score_list, weight_list = self.get_engagement_scores_and_weights_content_level(topic_id, unit_id)
        eng_score, weight = self.aggregate_one_level_up(eng_score_list, weight_list)

        self.course_structure['topics'][topic_id]['units'][unit_id]['unit_weight'] = weight
        self.course_structure['topics'][topic_id]['units'][unit_id]['engagement'] = eng_score

        return eng_score, weight


    def aggregate_units_to_module(self, topic_id):
        eng_score_list, weight_list = self.get_engagement_scores_and_weights_unit_level(topic_id)
        eng_score, weight = self.aggregate_one_level_up(eng_score_list, weight_list)

        self.course_structure['topics'][topic_id]['topic_weight'] = weight
        self.course_structure['topics'][topic_id]['engagement'] = eng_score

        return eng_score, weight


    def aggregate_module_to_course(self):
        eng_score_list = []
        weight_list = []
        for topic_id in self.course_structure['topics']:
            eng_score_list.append( self.course_structure['topics'][topic_id]['engagement'] )
            weight_list.append( self.course_structure['topics'][topic_id]['topic_weight'] )

        eng_score, weight = self.aggregate_one_level_up(eng_score_list, weight_list)

        return eng_score



    def process_for_entire_course(self):

        for topic_id in self.course_structure['topics']:

            for unit_id in self.course_structure['topics'][topic_id]['units']:
                self.aggregate_contents_to_unit(unit_id)

            self.aggregate_units_to_module(topic_id)

        self.aggregate_module_to_course()






# class user_storyline_old():
#
#     def __init__(self, alpha, user, chapter, mode, content, heatmap):
#
#         self.completionRate = 0
#         self.alpha = alpha
#         self.user = user
#         self.chapter = chapter
#         self.mode = mode
#         self.contentName = content
#         self.heatmap = heatmap
#
#         self.noteMeasure_result_value = 0
#         self.timeSpent_result_value = 0
#
#         self.actual_time_spent_on_segments = []
#         self.expected_time_spent_on_segments = []
#
#         self.expected_value_on_note_aspect_text = 10
#         self.actual_value_on_note_aspect_text = 0
#
#         self.expected_value_on_note_aspect_image = 10
#         self.actual_value_on_note_aspect_image = 0
#
#         self.expected_value_on_note_aspect_audio = 10
#         self.actual_value_on_note_aspect_audio = 0
#
#     def update_heatmap(self, inList):
#         self.heatmap = inList
#         # for i in inList:
#         #     if i < len(self.heatmap):
#         #         self.heatmap[i] += 1
#
#
#     def heatmap_to_actual_timeSpent_on_segments(self):
#
#         for i in xrange(0, len(self.heatmap)):
#
#             segment_ind = i/60
#
#             self.actual_time_spent_on_segments[segment_ind] += self.heatmap[i]
#
#
#
#     def get_timeSpent_result_value(self):
#         tmp = 1
#
#         for i, actual_time_spent in enumerate(self.actual_time_spent_on_segments):
#
#             tmp *= math.pow( (1 + actual_time_spent/float(self.expected_time_spent_on_segments[i])) , self.alpha)
#
#         self.timeSpent_result_value = tmp
#
#
#     def get_noteMeasurement_result_value(self):
#
#         tmp1 = math.pow((1 + self.actual_value_on_note_aspect_text / float(self.expected_value_on_note_aspect_text)),
#                         self.alpha_note)
#         tmp2 = math.pow((1 + self.actual_value_on_note_aspect_image / float(self.expected_value_on_note_aspect_image)),
#                         self.alpha_note)
#         tmp3 = math.pow((1 + self.actual_value_on_note_aspect_audio / float(self.expected_value_on_note_aspect_audio)),
#                         self.alpha_note)
#         self.noteMeasure_result_value = tmp1 * tmp2 * tmp3
#
#     def get_engagement_score(self):
#
#         self.engagementScore = self.completionRate * self.timeSpent_result_value * self.noteMeasure_result_value
