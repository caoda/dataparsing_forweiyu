import unittest
from unittest import TestCase
from mock import patch

from absolute_engagement_func import UserPdf, UserStoryline, UserArticle, UserCourse
import functions
import PBP_feature_parsing_for_one_user_abs_eng as parsing

class Test_UserPdf(TestCase):

    def test_get_engagement_score(self):

        alpha_mode = 0.1
        user = 'sammy',
        chapter = 'whatever101',
        content = 'chap1',
        heatmap = [50, 100, 150, 0, 250, 300, 350]
        expected_time_spent_on_segments = [100, 200, 300, 400, 500, 600, 700]

        user_pdf_sample = UserPdf(alpha_mode, user, chapter, content, heatmap, expected_time_spent_on_segments)

        weight = user_pdf_sample.weight_of_file()
        expected_weight = 2800
        self.assertEqual(weight, expected_weight)


        user_pdf_sample.calculate_completion_rate()
        expected_comp_rate = 6/7.0
        self.assertEqual(user_pdf_sample.completionRate, expected_comp_rate)


        user_pdf_sample.calculate_timeSpent_measurements()
        result = user_pdf_sample.timeSpent_result_value
        expected = 1.27542450063
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_pdf_sample.calculate_timeSpent_measurements_normalizer()
        expected = 1.62450479271
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        user_pdf_sample.calculate_note_measurements()
        result = user_pdf_sample.noteMeasure_result_value
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_pdf_sample.calculate_note_measurements_normalizer()
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_pdf_sample.get_engagement_score()
        expected = 100.0 * (6/7.0 * 1.02101212571 * 1.27542450063) / (1.0 * 1.02101212571 * 1.62450479271)
        self.assertEqual("%.4f" %result, "%.4f" %expected)


class Test_UserArticle(TestCase):

    def test_get_engagement_score(self):
        alpha = 0.1
        user = 'sammy',
        chapter = 'whatever101',
        content = 'chap1',
        totalCharacters = 100
        heatmap = [90]*30 + [120]*30 + [150]*10 + [200]*20 + [30]*10

        user_article_sample = UserArticle(alpha, user, chapter, content, totalCharacters, heatmap)

        expected = 2
        result = user_article_sample.num_segments
        self.assertEqual(result, expected)

        result = user_article_sample.segment_size
        expected = 50
        self.assertEqual(result, expected)

        expected = [50/6.66] * 2
        result = user_article_sample.expected_time_spent_on_segments
        self.assertSequenceEqual(result, expected)


        user_article_sample.heatmap_to_actual_timeSpent_on_segments()
        result = user_article_sample.actual_time_spent_on_segments
        expected = [(90*30 + 120*20)/50.0 , (120*10 + 150*10 + 200*20 + 30*10)/50.0]
        self.assertSequenceEqual(result, expected)


        user_article_sample.calculate_timeSpent_result_value()
        result = user_article_sample.timeSpent_result_value
        expected = 1.76086737432
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        user_article_sample.calculate_timeSpent_measurements_normalizer()
        result = user_article_sample.calculate_timeSpent_measurements_normalizer()
        expected = 1.148698355
        self.assertEqual("%.4f" %result, "%.4f" %expected)



        user_article_sample.calculate_note_measurement_result_value()
        result = user_article_sample.noteMeasure_result_value
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)



        result = user_article_sample.calculate_note_measurements_normalizer()
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        user_article_sample.calculate_completionRate()
        result = user_article_sample.completionRate
        expected = 1.0
        self.assertEqual("%.4f" %result, "%.4f" %expected)

        result = user_article_sample.get_engagement_score()
        expected = 100.0 * (1.0 * 1.76086737432 * 1.02101212571) / (1.0 * 1.02101212571 * 1.148698355)
        if expected > 100:
            expected = 100.0
        self.assertEqual("%.4f" %result, "%.4f" %expected)



        alpha = 0.1
        user = 'sammy',
        chapter = 'whatever101',
        content = 'chap1',
        totalCharacters = 49
        heatmap = [90]*30 + [120]*30 + [150]*10 + [200]*20 + [30]*10

        user_article_sample = UserArticle(alpha, user, chapter, content, totalCharacters, heatmap)

        expected = 1
        result = user_article_sample.num_segments
        self.assertEqual(result, expected)

        expected = [49/6.66]
        result = user_article_sample.expected_time_spent_on_segments
        self.assertEqual(result, expected)

        user_article_sample.heatmap_to_actual_timeSpent_on_segments()
        result = user_article_sample.actual_time_spent_on_segments
        expected = [(90*30 + 120*30 + 150*10 + 200*20 + 30*10)/100.0 ]
        self.assertEqual(result, expected)

        user_article_sample.calculate_completionRate()
        result = user_article_sample.completionRate
        expected = 1.0
        self.assertEqual(result, expected)

    def test_basics3(self):

        alpha = 0.1
        user = 'sammy',
        chapter = 'whatever101',
        content = 'chap1',
        totalCharacters = 125
        heatmap = [90]*30 + [120]*30 + [0]*10 + [200]*20 + [30]*10

        user_article_sample = UserArticle(alpha, user, chapter, content, totalCharacters, heatmap)

        result = user_article_sample.num_segments
        expected = 3
        self.assertEqual(result, expected)

        expected = [50/6.66] * 3
        result = user_article_sample.expected_time_spent_on_segments
        self.assertEqual(result, expected)

        user_article_sample.heatmap_to_actual_timeSpent_on_segments()
        result = user_article_sample.actual_time_spent_on_segments
        expected = [(90*30 + 120*10)/40.0 , (120*20 + 200*10)/40.0, (200*10+30*10)/20.0]
        self.assertSequenceEqual(result, expected)

        user_article_sample.calculate_completionRate()
        result = user_article_sample.completionRate
        expected = 0.90
        self.assertEqual(result, expected)


class Test_UserStoryline(TestCase):

    def test_get_engagement_score(self):

        alpha_mode = 0.1
        user = 'sammy'
        chapter = 'whatever101'
        content = 'chap1'
        heatmap = [300, 200, 250, 150, 50, 500, 400, 200, 100]
        expected_time_spent_on_segments = [500] * 9

        user_storyline_sample = UserStoryline(alpha_mode, user, chapter, content, heatmap, expected_time_spent_on_segments)


        result = user_storyline_sample.weight_of_file()
        expected = 4500
        self.assertEqual(result, expected)


        user_storyline_sample.calculate_timeSpentMeasurement()
        result = user_storyline_sample.timeSpent_result_value
        expected = 1.40064704621
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_storyline_sample.calculate_timeSpent_measurements_normalizer()
        expected = 1.86606598307
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        user_storyline_sample.calculate_noteMeasurement()
        result = user_storyline_sample.noteMeasure_result_value
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_storyline_sample.calculate_note_measurements_normalizer()
        expected = 1.02101212571
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        user_storyline_sample.calculate_completionRate()
        result = user_storyline_sample.completionRate
        expected = 1.0
        self.assertEqual("%.4f" %result, "%.4f" %expected)


        result = user_storyline_sample.get_engagement_score()
        expected = 100.0 * (1.0 * 1.40064704621 * 1.02101212571)  /  (1.86606598307 * 1.02101212571 * 1.0)
        self.assertEqual("%.4f" %result, "%.4f" %expected)


class Test_UserCourse(TestCase):

    def general_test(self):
        course_id = 'what101'
        course_structure = {
            'topics': {
                        'topic_1': {'units':{'unit_1':{
                                                    'contents':{'content_1':{'engagement':150, 'weight':10},
                                                                'content_2':{'engagement':60, 'weight':20}},
                                                    'engagement': 0,
                                                    'unit_weight': 30
                                               },
                                             'unit_2':{
                                                    'contents':{'content_1':{'engagement':20, 'weight':10},
                                                                'content_2':{'engagement':20, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 20
                                             },

                                             'unit_3':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 20
                                              },
                                            },
                                    'topic_weight': 0,
                                    'engagement': 0
                        },

                        'topic_2': {'units':{'unit_4':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                },
                                             'unit_5':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                      },
                                             'unit_6':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                      },
                                            },

                                    'topic_weight':0,
                                    'engagement': 0,
                        },

                        'topic_3': {'units':{'unit_7':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                    },
                                             'unit_8':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                      },
                                             'unit_9':{
                                                    'contents':{'content_1':{'engagement':10, 'weight':10},
                                                                'content_2':{'engagement':10, 'weight':10}},
                                                    'engagement': 0,
                                                    'unit_weight': 10
                                                      },
                                            },
                                    'topic_weight': 0,
                                    'engagement': 0
                        },

                    },

        }

        user_course_sample = UserCourse(course_id, course_structure)



        engagement_score_list = [10, 10, 10, 50]
        weight_list = [10,10,10,20]
        aggr_engagement_score, aggr_weight = user_course_sample.aggregate_one_level_up(engagement_score_list, weight_list)
        expected_aggr_engagement_score = 26
        expected_aggr_weight = 50
        self.assertEqual(aggr_engagement_score, expected_aggr_engagement_score)
        self.assertEqual(aggr_weight, expected_aggr_weight)



        unit_id = 'unit_1'
        eng_score, weight = user_course_sample.aggregate_contents_to_unit(unit_id)
        expected_eng = 90
        expected_weight = 30
        self.assertEqual(eng_score, expected_eng)
        self.assertEqual(weight, expected_weight)

        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_2')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_3')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_4')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_5')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_6')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_7')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_8')
        eng_score, weight = user_course_sample.aggregate_contents_to_unit('unit_9')


        eng_score_list, weight_list = user_course_sample.get_engagement_scores_and_weights_content_level('topic_1', 'unit_1')
        expected_eng_score_list = [150, 60]
        expected_weight_list = [10, 20]
        self.assertSequenceEqual(sorted(eng_score_list), sorted(expected_eng_score_list))
        self.assertSequenceEqual(sorted(weight_list), sorted(expected_weight_list))


        eng_score_list, weight_list = user_course_sample.get_engagement_scores_and_weights_unit_level('topic_1')
        expected_eng_score_list = [90, 20, 10]
        expected_weight_list = [30, 20, 20]
        self.assertSequenceEqual(sorted(eng_score_list), sorted(expected_eng_score_list))
        self.assertSequenceEqual(sorted(weight_list), sorted(expected_weight_list))


        eng_score, weight = user_course_sample.aggregate_units_to_module('topic_1')
        expected_eng = 47.142
        expected_weight = 70
        self.assertEqual("%.4f" %eng_score, "%.4f" %eng_score)
        self.assertEqual(weight, expected_weight)



class Test_functions(TestCase):

    def test_filter_with_timestamp(self):
        events = [
            {'index':1, 'timestamp':100},
            {'index':2, 'timestamp':155},
            {'index':3, 'timestamp':186},
            {'index':4, 'timestamp':194},
            {'index':5, 'timestamp':265},
            {'index':6, 'timestamp':267},
            {'index':7, 'timestamp':283},
            {'index':8, 'timestamp':377},
            {'index':9, 'timestamp':412},
            {'index':10, 'timestamp':533},
        ]

        timestamp_filter = 400

        results = functions.filter_with_timestamp(events, timestamp_filter)
        expected = [
            {'index':1, 'timestamp':100},
            {'index':2, 'timestamp':155},
            {'index':3, 'timestamp':186},
            {'index':4, 'timestamp':194},
            {'index':5, 'timestamp':265},
            {'index':6, 'timestamp':267},
            {'index':7, 'timestamp':283},
            {'index':8, 'timestamp':377}
        ]

        for i, dict in enumerate(results):
            self.assertDictEqual(dict, expected[i])


    def test_get_slides_total_duration(self):

        events = [
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter101', 'slideIndex':0, 'duration': 400},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter101', 'slideIndex':1, 'duration': 200},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter101', 'slideIndex':2, 'duration': 300},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter101', 'slideIndex':3, 'duration': 100},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter102', 'slideIndex':4, 'duration': 100},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter102', 'slideIndex':5, 'duration': 200},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter102', 'slideIndex':6, 'duration': 200},
            {'m_type': 'SlidePlaybackEventMeasurement', 'chapter':'chapter102', 'slideIndex':7, 'duration': 200},
        ]

        result = functions.get_slides_total_duration(events)
        expected = {'chapter101':1000, 'chapter102':700}
        self.assertDictEqual(result, expected)

    def test_get_slides_infos_and_durations(self):
        all_slides_play_events_of_one_storyline_from_all_users = [
            {'slideIndex':0, 'slide': 'abc100', 'slideTitle':'this is 0',  'duration': 400},
            {'slideIndex':1, 'slide': 'abc101', 'slideTitle':'this is 1',  'duration': 300},
            {'slideIndex':2, 'slide': 'abc102', 'slideTitle':'this is 2',  'duration': 200},
            {'slideIndex':3, 'slide': 'abc103', 'slideTitle':'this is 3',  'duration': 100},
            {'slideIndex':4, 'slide': 'abc104', 'slideTitle':'this is 4',  'duration': 500},
            {'slideIndex':5, 'slide': 'abc105', 'slideTitle':'this is 5',  'duration': 100}
        ]


        slide_infos, slide_durations_list = functions.get_slides_infos_and_durations(all_slides_play_events_of_one_storyline_from_all_users)

        expected_slide_infos = {'abc100':{'index':0, 'slideTitle':'this is 0'},
                                'abc101':{'index':1, 'slideTitle':'this is 1'},
                                'abc102':{'index':2, 'slideTitle':'this is 2'},
                                'abc103':{'index':3, 'slideTitle':'this is 3'},
                                'abc104':{'index':4, 'slideTitle':'this is 4'},
                                'abc105':{'index':5, 'slideTitle':'this is 5'}
                    }

        expected_slide_durations_list = [400, 300, 200, 100, 500, 100]

        self.assertDictEqual(expected_slide_infos, slide_infos)
        self.assertSequenceEqual(slide_durations_list, expected_slide_durations_list)


    def test_get_unique_questions(self):

        chapter = 'abc101'
        events = [
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc101', 'question':'q1'},
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc101', 'question':'q2'},
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc101', 'question':'q1'},
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc101', 'question':'q2'},
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc102', 'question':'q3'},
            {'m_type':'QuizAnswerMeasurement', 'chapter':'abc101', 'question':'q4'},
        ]

        uniqueQ = functions.get_unique_questions(chapter, events)
        expected_uniqueQ = [
            'q1',
            'q2',
            'q4'
        ]

        for i, ques in enumerate(uniqueQ):
            self.assertEqual(ques, expected_uniqueQ[i])


    def test_get_scroll_count_pdf(self):

        pdf_evts = [
            {'page':0},
            {'page':5},
            {'page':7},
            {'page':8},
            {'page':3},
            {'page':3},
            {'page':4},
            {'page':6},
            {'page':1},
            {'page':9},
            {'page':2},
            {'page':6},
            {'page':6},
            {'page':8}
        ]

        numScroll_up, numScroll_down = functions.get_scroll_count_pdf(pdf_evts)
        expected_numScroll_up = 3
        expected_numScroll_down = 8
        self.assertEqual(numScroll_up, expected_numScroll_up)
        self.assertEqual(numScroll_down, expected_numScroll_down)


    def get_scroll_count_article(self):

        art_evts = [
            {'startIndex':0},
            {'startIndex':5},
            {'startIndex':7},
            {'startIndex':8},
            {'startIndex':3},
            {'startIndex':3},
            {'startIndex':4},
            {'startIndex':6},
            {'startIndex':1},
            {'startIndex':9},
            {'startIndex':2},
            {'startIndex':6},
            {'startIndex':6},
            {'startIndex':8}
        ]

        numScroll_up, numScroll_down = functions.get_scroll_count_article(art_evts)
        expected_numScroll_up = 3
        expected_numScroll_down = 8
        self.assertEqual(numScroll_up, expected_numScroll_up)
        self.assertEqual(numScroll_down, expected_numScroll_down)


    def test_normail_time_to_unix_timestamp(self):

        string_time = '2016_06_14_12_00'
        result = functions.normail_time_to_unix_timestamp(string_time)
        expected = 1465920000.0
        self.assertEqual(result, expected)


    def test_for_testing_1(self):

        attr_thisUser = {
            'chap1':{'article_existing_for_user':1, 'pdf_existing_for_user':1, 'storyline_existing_for_user':1,
                     'article':{'abs_engagement': 160},
                     'pdf':{'abs_engagement': 120},
                     'storyline':{'abs_engagement': 110}
                     }

        }
        ch = 'chap1'
        result = parsing.for_testing_1(attr_thisUser, ch)
        expected = 130
        self.assertEqual(result, expected)

    def test_for_testing_2(self):

        chap_names = [
            'chap1',
            'chap2',
            'chap3',
            'chap4',
            'chap5',
            'chap6',
            'chap7',
            'chap8',
            'chap9',
            'chap10'
        ]

        attr_thisUser = {
            'overall_engagement_just_average': 0,
            'overall_engagement_weighted_average': 0,
            'slope_of_engagement': 0,
            'stdev_of_engagement': 0,
            '#of_zero_chapter_engagement': 0,
            'timespent_on_course': 0,

            'chap1':{'avg_abs_engagement': 90, 'weight':10, 'timespent': 300},
            'chap2':{'avg_abs_engagement': 80, 'weight':15, 'timespent': 400},
            'chap3':{'avg_abs_engagement': 30, 'weight':20, 'timespent': 300},
            'chap4':{'avg_abs_engagement': 60, 'weight':10, 'timespent': 500},
            'chap5':{'avg_abs_engagement': 70, 'weight':15, 'timespent': 600},
            'chap6':{'avg_abs_engagement': 50, 'weight':20, 'timespent': 200},
            'chap7':{'avg_abs_engagement': 90, 'weight':10, 'timespent': 100},
            'chap8':{'avg_abs_engagement': 100, 'weight':15, 'timespent': 700},
            'chap9':{'avg_abs_engagement': 60, 'weight':20, 'timespent': 900},
            'chap10':{'avg_abs_engagement': 100, 'weight':10, 'timespent': 350}
        }

        timespent_on_course, stdev_of_engagement, slope_of_engagement, \
            overall_engagement_weighted_average, overall_engagement_just_average = parsing.for_testing_2(chap_names, attr_thisUser)

        result = [timespent_on_course, stdev_of_engagement, slope_of_engagement, overall_engagement_weighted_average, overall_engagement_just_average]
        expected = [4350, 21.931712199461309, 14.0, 68.62068965517241, 73.0]
        self.assertSequenceEqual(result, expected)