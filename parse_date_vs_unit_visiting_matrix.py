__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import codecs

import functions as func
import define_attributes
import numpy

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


#raw_fileName = 'raw_clickstream/measurements-pbp-pbp_mah_102_4.txt'
#raw_fileName = 'raw_clickstream/measurements-pbp-pbp_mah_103_4.json'
raw_fileName = 'raw_clickstream/measurements-pbp-pbp_wlc_302_4.json'
# raw_fileName = 'raw_clickstream/measurements-pbp-pbp_wlc_303_4.json'
# raw_fileName = 'raw_clickstream/measurements-pbp-pbp_wlc_305_2.json'

#fname = 'first-time-users-pbp-pbp_mah_102_4-from-2015-09-01-to-2015-12-03.csv'
#fname = 'first-time-users-pbp-pbp_mah_103_4-from-2015-09-01-to-2015-12-22.csv'
fname = 'first-time-users-pbp-pbp_wlc_302_4-from-2015-09-01-to-2016-01-12.csv'
# fname = 'first-time-users-pbp-pbp_wlc_305_2-from-2015-09-01-to-2015-12-18.csv'

nameSTR = 'date_vs_unit_Visiting_wlc302_4.csv'

events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


chap_content = {

    'PBP_WLC_305_2_CHAPTER00':[['PBP_WLC_305_2_CONTENT00'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER01':[['PBP_WLC_305_2_CONTENT10'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER02':[['PBP_WLC_305_2_CONTENT20'], ['epud']],
    'PBP_WLC_305_2_CHAPTER03':[['PBP_WLC_305_2_CONTENT30'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER04':[['PBP_WLC_305_2_CONTENT40'], ['storyline']],
    'PBP_WLC_305_2_CHAPTER05':[['PBP_WLC_305_2_CONTENT50'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER06':[['PBP_WLC_305_2_CONTENT60'], ['epud']],
    'PBP_WLC_305_2_CHAPTER07':[['PBP_WLC_305_2_CONTENT70'], ['epud']],
    'PBP_WLC_305_2_CHAPTER08':[['PBP_WLC_305_2_CONTENT80', 'PBP_WLC_305_2_CONTENT81'], ['pdf', 'epud']],
    'PBP_WLC_305_2_CHAPTER09':[['PBP_WLC_305_2_CONTENT90'], ['pdf']],
    'PBP_WLC_305_2_CHAPTER10':[['PBP_WLC_305_2_CONTENT100'], ['epud']],
    'PBP_WLC_305_2_CHAPTER11':[['PBP_WLC_305_2_CONTENT110'], ['epud']]
}

clean_events = []
for evt in events:

    if 'chapter' in evt and evt['chapter'] == 'PBP_WLC_305_2_CHAPTER06':
        continue

    clean_events.append(evt)

events = clean_events
del clean_events







''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names, allChapNums, existingModes, dates = define_attributes.get(events)

for date in dates:
    print(date)

print("   ")

for ch in chap_names:
    print(ch)

'''/////////////////////////////////////  Get the user info ///////////////////////////////////////////////'''
# get userID  -  user name dictionary
csvfile = open(fname, 'rU')
reader = csv.reader(csvfile, dialect='excel')

userInfo = []
givenNames = []
familyNames = []
for row in reader:
    givenName = row[0]
    familyName = row[1]
    givenNames.append(givenName)
    familyNames.append(familyName)
    email = row[2]
    id = row[3]
    userInfo.append(row)

print("there are %s users in %s" %(len(userInfo), fname))


def get_names(user):

    f = 'first_name'
    l = 'last_name'
    email = 'cannot find'

    for line in userInfo:
        if line[3] == user:
            f = line[0]
            l = line[1]
            email = line[2]

    return f, l, email





# todo: main
def main():

    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')

    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)

    users = sorted(uv.keys())

    for user in users:

        first_name, last_name, email = get_names(user)

        mat_dict = {}
        for date in dates:
            mat_dict[date] = {}
            for ch in chap_names:
                mat_dict[date][ch] = 0


        all_evts = uv[user]
        nav_evts = func.get_evt_from_list(all_evts, m_navi)

        for evt in nav_evts:
            if evt['action'] == 'open':
                readTime = datetime.datetime.fromtimestamp(int(evt['timestamp']))
                y = unicode(readTime.year)
                m = unicode(readTime.month)
                day = unicode(readTime.day)
                y = unicodedata.normalize('NFKD', y).encode('ascii', 'ignore')
                m = unicodedata.normalize('NFKD', m).encode('ascii', 'ignore')
                day = unicodedata.normalize('NFKD', day).encode('ascii', 'ignore')
                date_str = y+'_'+m+'_'+day
                chap = evt['chapter']

                if date_str in mat_dict:
                    print('%s  %s  %s' %(user, date_str, chap))
                    mat_dict[date_str][chap] += 1


        # write to csv as a matrix
        wr.writerow(["user", user, first_name, last_name, email ])
        wr.writerow(["dates/chapters"] + chap_names)
        for date in dates:
            tmp = []
            for c in chap_names:
                tmp.append( mat_dict[date][c] )
            wr.writerow([date] + tmp)
        wr.writerow(["  "])

    fileName.close()
    print('csv file done!!')

    return 0





main()






