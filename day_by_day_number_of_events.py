__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

import functions as func



''' set parameters '''
course_name = 'WLC_303'
session_number = 4

''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_wlc_303_4.json'
events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])



def number_of_events(events, time_filter_datetime):
    timestamp_filter = func.normail_time_to_unix_timestamp(time_filter_datetime)
    filtered_events = func.filter_with_timestamp(events, timestamp_filter)
    return len(filtered_events)



time_filter_datetimes = [
                            '2015_12_03_00_00',
                            '2015_12_04_00_00',
                            '2015_12_05_00_00',
                            '2015_12_06_00_00',
                            '2015_12_07_00_00',
                            '2015_12_08_00_00',
                            '2015_12_09_00_00',
                            '2015_12_10_00_00',
                            '2015_12_11_00_00',
                            '2015_12_12_00_00',
                            '2015_12_13_00_00',
                            '2015_12_14_00_00',
                            '2015_12_15_00_00',
                            '2015_12_16_00_00',
                            '2015_12_17_00_00'
                        ]


counts = []
for time_filter_datetime in time_filter_datetimes:
    counts.append( number_of_events(events, time_filter_datetime) )


nameSTR = 'number_of_events_%s_%s.csv' %(course_name, session_number)
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')

for i, date in enumerate(time_filter_datetimes):
    wr.writerow([date, counts[i]])
    print [date, counts[i]]














''' set parameters '''
course_name = 'MAH_102'
session_number = 4

''' input files '''
raw_events_from_reporting_tool = "raw_clickstream/measurements-pbp-pbp_mah_102_4.txt"
events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


time_filter_datetimes = [
                                '2015_10_14_00_00',
                                '2015_10_15_00_00',
                                '2015_10_16_00_00',
                                '2015_10_17_00_00',
                                '2015_10_18_00_00',
                                '2015_10_19_00_00',
                                '2015_10_20_00_00',
                                '2015_10_21_00_00',
                                '2015_10_22_00_00',
                                '2015_10_23_00_00',
                                '2015_10_24_00_00',
                                '2015_10_25_00_00',
                                '2015_10_26_00_00',
                                '2015_10_27_00_00',
                                '2015_10_28_00_00',
                        ]


counts = []
for time_filter_datetime in time_filter_datetimes:
    counts.append( number_of_events(events, time_filter_datetime) )


nameSTR = 'number_of_events_%s_%s.csv' %(course_name, session_number)
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')

for i, date in enumerate(time_filter_datetimes):
    wr.writerow([date, counts[i]])
    print [date, counts[i]]







''' set parameters '''
course_name = 'WLC_305'
session_number = 2

''' input files '''
raw_events_from_reporting_tool = 'raw_clickstream/measurements-pbp-pbp_wlc_305_2.json'
events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


time_filter_datetimes = [
                            '2015_10_01_00_00',
                            '2015_10_02_00_00',
                            '2015_10_03_00_00',
                            '2015_10_04_00_00',
                            '2015_10_05_00_00',
                            '2015_10_06_00_00',
                            '2015_10_07_00_00',
                            '2015_10_08_00_00',
                            '2015_10_09_00_00',
                            '2015_10_10_00_00',
                            '2015_10_11_00_00',
                            '2015_10_12_00_00',
                            '2015_10_13_00_00',
                            '2015_10_14_00_00',
                            '2015_10_15_00_00'
                        ]


counts = []
for time_filter_datetime in time_filter_datetimes:
    counts.append( number_of_events(events, time_filter_datetime) )


nameSTR = 'number_of_events_%s_%s.csv' %(course_name, session_number)
fileName = open(nameSTR, 'wb')
wr = csv.writer( fileName, dialect= 'excel')

for i, date in enumerate(time_filter_datetimes):
    wr.writerow([date, counts[i]])
    print [date, counts[i]]




