from __future__ import division

import json
import traceback

from django.http import HttpResponse


import Performance_evaluation as performance_eval


import multiprocessing as mp

from operator import itemgetter
from django.db import connection
import engagement_funcs_local


algo_debug = False

def print_debug(*args):
    if algo_debug is True:
        print(args);


def getRelevant(dictlist, key, valuelist):
      return [dictio for dictio in dictlist if dictio.get(key) in valuelist]


def get_performance_response(startDate, endDate, courseID, users):
    overall = {}
    try:
        topics = Course.objects.get(id=courseID).topics.order_by('index')

        allChapterIDs = []

        for topic in topics:

            for chapter in topic.chapters.all().order_by('segment_number'):
                allChapterIDs.append(chapter.id)

                # get active users in that timeframe

        userIDs = list(users.values_list('id', flat=True))


        articleM = list(Measurements('ArticleViewPositionMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        videoM = list(Measurements('VideoMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        pdfM =  list(Measurements('PDFMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))


        slideChM =  list(Measurements('SlideChangedMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        slidePlayM =  list(Measurements('SlidePlaybackEventMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        slideCompM =  list(Measurements('SlideCompletedMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        quizM =  list(Measurements('QuizAnswerMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        lifeM =  list(Measurements('LifeCycleMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        navM =  list(Measurements('NavigationMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))

        winM =  list(Measurements('WindowLayoutMeasurement').objects.filter(timestamp__range=(startDate, endDate),
                                                                       course=courseID,
                                                                       user__in=userIDs).order_by('timestamp'))


        measurements = {'articleM':articleM, 'videoM':videoM, 'pdfM':pdfM,'slideChM':slideChM,'slidePlayM':slidePlayM, 'slideCompM':slideCompM,'quizM':quizM,'navM':navM,'winM':winM, 'lifeM':lifeM}

        miic_users = users

        topicDics = []
        for topic in topics:

            chaptersArray = []
            q = mp.JoinableQueue()
            processes = []
            for index, chapter in enumerate(topic.chapters.all().order_by('segment_number')):
                p = mp.Process(target=process_chapter, args=(chapter, courseID, startDate, endDate, miic_users, index, q, measurements))
                processes.append(p)

            connection.close()

            for p in processes:
                p.start()

            q.join()
            for p in processes:
                value = q.get()
                if value is not None:
                    chaptersArray.append(value)
            q.close()

            chaptersArray = sorted(chaptersArray, key=itemgetter('chapterIndex'))

            topicDic = process_and_average_big(chaptersArray, miic_users)
            topicDic['id'] = topic.id
            topicDic['title'] = topic.title

            print_debug(topic.title)
            print_debug(topicDic)

            topicDic['name'] = topic.title
            topicDic['type'] = 'topic'
            topicDic['children'] = chaptersArray
            topicDics.append(topicDic)

        connection.close()
        overall = process_and_average_big(topicDics, miic_users)

        overall['id'] = 'overall'
        overall['title'] = 'Overall'
        overall['name'] = 'Overall'
        overall['type'] = 'overall'
        overall['children'] = topicDics

        overall['result'] = 1

        #print "performance %s" % json.dumps(overall)
    except Exception as e:
        return HttpResponse(
            '{"result":-1, "detail":"%s" "trace":"\n\n%s"}', status=400)

    return overall


def process_chapter(chapter, courseID, startDate, endDate, miic_users, index, queueOutput, measurements):
    try:
        if connection.cursor() is not None:
            connection.close()
        che, weight = engagement_funcs.get_engagement_dic_chapter_users(courseID, chapter, startDate, endDate,
                                                                      miic_users, measurements)
        if connection.cursor() is not None:
            connection.close()

        if che is None:
            return HttpResponse('{"result":-1, "detail":"No users or no information for chapters available yet!"}',
                                status=400)

        for e in che:

            answerList = getRelevant(measurements['quizM'], 'chapter', [chapter.id])
            answerList = getRelevant(answerList, 'user', [e['id']])


            if len(answerList) > 0:


                questions = chapter.questions.all()
                question_count = len(questions)
                if chapter.use_randomized_questions == True and chapter.randomized_questions_pick_count > 0:
                    question_count = chapter.randomized_questions_pick_count

                p = performance_eval.get_measurements({e['id']: answerList}, question_count)

                totalAverage = 0
                totalAbsolute = 0
                count = 0
                for a in p['views']:
                    totalAverage = totalAverage + a['avgScore']
                    totalAbsolute = totalAbsolute + a['absoluteScore']
                    count = count + 1

                if count > 0:
                    e['performance'] = (totalAverage / count) * 100
                    e['performance_real'] = totalAbsolute

                newViews = []
                for question in questions:
                    found = False

                    for item in p['views']:
                        if item['quizID'] == question.id:
                            newViews.append(item)
                            found = True

                    if found == False:
                        d = {}
                        d['quizID'] = question.id
                        d['timeSpent'] = 0
                        d['timesViewed'] = 0
                        d['avgScore'] = 0
                        d['absoluteScore'] = 0
                        newViews.append(d)

                p['views'] = newViews




        dic = process_and_average_for_chapter(che)
        for e in che:
            eng = {}
            if 'article' in e:
                eng['article'] = e.pop('article')
            if 'slideshow' in e:
                eng['slideshow'] = e.pop('slideshow')
            if 'video' in e:
                eng['video'] = e.pop('video')
            if 'pdf' in e:
                eng['pdf'] = e.pop('pdf')
            if 'average' in e:
                eng['average'] = e.pop('average')

            e['engagement'] = eng

        dic['id'] = chapter.id
        dic['title'] = chapter.title
        dic['name'] = chapter.title
        dic['type'] = 'unit'
        dic['students'] = che
        dic['weight'] = weight

        if che is None:
            dic['students'] = []

        dic['chapterIndex'] = index
        queueOutput.put(dic)
        queueOutput.task_done()

    except Exception:
        print "exception stacktrace %s" % traceback.format_exc()
        queueOutput.put(None)
        queueOutput.task_done()

def process_and_average_for_chapter(user_infos):
    article = 0
    aCount = 0
    slideshow = 0
    sCount = 0
    video = 0
    vCount = 0
    pdf = 0
    pCount = 0
    avg = 0
    avgCount = 0
    perf = 0
    perfCount = 0

    perfReal = 0

    for info in user_infos:
        if 'article' in info:
            article = article + info['article']
            aCount = aCount + 1

        if 'slideshow' in info:
            slideshow = slideshow + info['slideshow']
            sCount = sCount + 1

        if 'video' in info:
            video = video + info['video']
            vCount = vCount + 1

        if 'pdf' in info:
            pdf = pdf + info['pdf']
            pCount = pCount + 1

        if 'average' in info:
            avg = avg + info['average']
            avgCount = avgCount + 1

        if 'performance' in info and info['performance'] >= 0:
            perf = perf + info['performance']
            perfCount = perfCount + 1

        if 'performance_real' in info and info['performance_real'] >= 0:
            perfReal = perfReal +info['performance_real']


    eng = {}

    # todo: instead average over active ones, now it is averaged over all users
    numOfUsers = float(len(user_infos))

    if numOfUsers > 0:
        eng['article'] = article / float(numOfUsers)
        # if aCount > 0:
        #     eng['article'] = article / aCount

        eng['slideshow'] = slideshow / float(numOfUsers)
        # if sCount > 0:
        #     eng['slideshow'] = slideshow / sCount

        eng['video'] = video / float(numOfUsers)
        # if vCount > 0:
        #     eng['video'] = video / vCount

        eng['pdf'] = pdf / float(numOfUsers)
        # if pCount > 0:
        #     eng['pdf'] = pdf / pCount

        eng['average'] = avg / float(numOfUsers)
        # if avgCount > 0:
        #     eng['average'] = int(avg / avgCount)

    for key in eng:
        if eng[key] > 0 and eng[key] < 1:
            eng[key] = 1
        else:
            eng[key] = int(round(eng[key]))



    dic = {}

    dic['engagement'] = eng

    if perfCount > 0:
        dic['performance'] = int(perf / perfCount)

        dic['performance_real'] = perfReal

    return dic


def process_and_average_big(chapterInfos, miic_users):
    article = 0
    aCount = 0
    slideshow = 0
    sCount = 0
    video = 0
    vCount = 0
    pdf = 0
    pCount = 0
    avg = 0
    avgCount = 0
    perf = 0
    perfCount = 0

    perfReal = 0
    perfRealCount = 0

    for info in chapterInfos:
        if 'article' in info['engagement']:
            article = article + info['engagement']['article']
            aCount = aCount + 1

        if 'slideshow' in info['engagement']:
            slideshow = slideshow + info['engagement']['slideshow']
            sCount = sCount + 1

        if 'video' in info['engagement']:
            video = video + info['engagement']['video']
            vCount = vCount + 1

        if 'pdf' in info['engagement']:
            pdf = pdf + info['engagement']['pdf']
            pCount = pCount + 1

        if 'average' in info['engagement']:
            avg = avg + info['engagement']['average']
            avgCount = avgCount + 1

        if 'performance' in info and info['performance'] >= 0:
            perf = perf + info['performance']
            perfCount = perfCount + 1

        if 'performance_real' in info and info['performance_real'] >= 0:
            perfReal = perfReal + info['performance_real']
            perfRealCount = perfRealCount + 1


    eng = {}

    if aCount > 0:
        eng['article'] = article / aCount

    if sCount > 0:
        eng['slideshow'] = slideshow / sCount

    if vCount > 0:
        eng['video'] = video / vCount

    if pCount > 0:
        eng['pdf'] = pdf / pCount

    if avgCount > 0:
        eng['average'] = int(avg / avgCount)

    dic = {}
    dic['engagement'] = eng

    if perfCount > 0:
        dic['performance'] = int(perf / perfCount)
    else:
        dic['performance'] = 1000

    if perfRealCount > 0:
        dic['performance_real'] = int(perfReal)
    else:
        dic['performance_real'] = -1000

    userDics = []

    # todo: new engmt computation method:
    total_weight = 0

    for i in chapterInfos:
        if 'weight' in i:
            total_weight += i['weight']

    for miic_u in miic_users:
        id = miic_u.id

        userDic = {}
        userDic['id'] = id

        if miic_u.user.first_name == '':
            userDic['name'] = miic_u.user.last_name
        else:
            userDic['name'] = '%s, %s' % (miic_u.user.last_name, miic_u.user.first_name)

        print_debug( userDic['name'] )

        userDic['groups'] = [miic_u.group.company_group_id]

        article = 0
        aCount = 0
        slideshow = 0
        sCount = 0
        video = 0
        vCount = 0
        pdf = 0
        pCount = 0
        avg = 0 # todo: this value is the key!!
        avgCount = 0
        perf = 0
        perfCount = 0

        perfReal = 0
        perfRealCount = 0

        for i in chapterInfos:
            weight = i['weight']
            if 'students' in i:
                for info in i['students']:
                    if info['id'] != id:
                        continue

                    if 'article' in info['engagement']:
                        article = article + info['engagement']['article']
                        aCount = aCount + 1

                    if 'slideshow' in info['engagement']:
                        slideshow = slideshow + info['engagement']['slideshow']
                        sCount = sCount + 1

                    if 'video' in info['engagement']:
                        video = video + info['engagement']['video']
                        vCount = vCount + 1

                    if 'pdf' in info['engagement']:
                        pdf = pdf + info['engagement']['pdf']
                        pCount = pCount + 1

                    if 'average' in info['engagement']:
                        avg = avg + info['engagement']['average'] * weight
                        avgCount = avgCount + 1
                        print_debug(   i['title'] , i['weight'], avg )


                    if 'performance' in info and info['performance'] >= 0:
                        perf = perf + info['performance']
                        perfCount = perfCount + 1

                    if 'performance_real' in info and info['performance_real'] >= 0:
                        perfReal = perfReal + info['performance_real']
                        perfRealCount = perfRealCount +1

        eng = {}

        if aCount > 0:
            eng['article'] = article / aCount

        if sCount > 0:
            eng['slideshow'] = slideshow / sCount

        if vCount > 0:
            eng['video'] = video / vCount

        if pCount > 0:
            eng['pdf'] = pdf / pCount

        # if avgCount > 0:
        #     eng['average'] = int(avg / avgCount)

        #numberOfAll = float(len(chapterInfos))# todo: number of units in a topic or number of topics in a course
        if total_weight > 0:
            eng['average'] = (avg / float(total_weight))# todo: now each user's overall engmt is proper averaged
        else:
            eng['average'] = avg

        if eng['average'] > 0 and eng['average'] < 1:
            eng['average'] = 1
        else:
            eng['average'] = int(eng['average'])

        userDic['engagement'] = eng

        if perfCount > 0:
            userDic['performance'] = int(perf / perfCount)

        if perfRealCount > 0:
            userDic['performance_real'] = int(perfReal)
        else:
            userDic['performance_real'] = -1000

        userDics.append(userDic)

    dic['students'] = userDics
    dic['weight'] = total_weight

    return dic
