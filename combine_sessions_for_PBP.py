__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline
from pdf_words_in_courses import *
import absolute_engagement_func as AB_eng

import functions as func





def combine_wlc_305_2_and_4():

    matrix_wlc305_2 = 'new_feature_matrix_with_abs_eng/matrix_WLC305_2_new.csv'
    matrix_wlc305_4 = 'new_feature_matrix_with_abs_eng/matrix_WLC305_4_new.csv'


    combined_output = 'new_feature_matrix_with_abs_eng/matrix_WLC305_2_and_4_combined.csv'
    file = open(combined_output, 'wb')
    wr = csv.writer(file, dialect= 'excel')


    csvfile = open(matrix_wlc305_2, 'rU')
    reader = csv.reader(csvfile, dialect='excel')
    features_wlc305_2 = next(reader)
    wr.writerow(features_wlc305_2)

    for row in reader:
        wr.writerow(row)


    csvfile = open(matrix_wlc305_4, 'rU')
    reader = csv.reader(csvfile, dialect='excel')
    features_wlc305_4 = next(reader)

    shift_30 = [-1] * 31
    for row in reader:
        row = row[0:6] + shift_30 + row[6:]
        wr.writerow(row)

    file.close()

    return 0


combine_wlc_305_2_and_4()