import random
import unittest



correct_meta_info = {   "employees": {
                                "employed": ['user0', 'user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7',
                                             'user8', 'user9'],
                                "completed": ['user1', 'user3', 'user4', 'user6']
                             }
                        }



correct_units_info = [
              {
                "versions": [{
                                "id": 0,
                                "title": "chapter0",
                                "employed": ['user0', 'user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7',
                                             'user8', 'user9'],
                                "completed": ['user0', 'user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7',
                                             'user8', 'user9'],
                                "repeated": [],
                                "skipped": []
                            }]
	          },

              {
                  "versions": [{
                                    "id": 1,
                                    "title": "chapter1",
                                    "employed": ['user0', 'user1', 'user2', 'user3', 'user5', 'user6', 'user8', 'user9'],
                                    "completed": ['user0', 'user1', 'user2', 'user3', 'user5', 'user6', 'user8', 'user9'],
                                    "repeated": [],
                                    "skipped": []
                                }]
            },

              {
                  "versions": [{
                                    "id": 2,
                                    "title": "chapter2",
                                    "employed": ['user0', 'user2', 'user3', 'user4', 'user6', 'user8', 'user9'],
                                    "completed": ['user0', 'user2', 'user3', 'user4', 'user6', 'user8', 'user9'],
                                    "repeated": [],
                                    "skipped": []
                                }]
              },

               {
                  "versions": [{
                                    "id": 3,
                                    "title": "chapter3",
                                    "employed": ['user0', 'user3', 'user5', 'user6', 'user8', 'user9'],
                                    "completed": ['user0', 'user3', 'user5', 'user6', 'user8', 'user9'],
                                    "repeated": [],
                                    "skipped": []
                                },

                                {
                                    "id": 4,
                                    "title": "chapter3_v2",
                                    "employed": ['user0', 'user3', 'user5'],
                                    "completed": ['user0', 'user3', 'user5'],
                                    "repeated": [],
                                    "skipped": []
                                }]
               },



            {
		          "versions": [{
                                    "id": 5,
                                    "title": "chapter5",
                                    "employed": ['user0', 'user1', 'user3', 'user5', 'user6', 'user7', 'user8'],
                                    "completed": ['user0', 'user1', 'user3', 'user5', 'user6', 'user7', 'user8'],
                                    "repeated": [],
                                    "skipped": []
                                }]
	        },

            {
                  "versions": [{
                                    "id": 6,
                                    "title": "chapter6",
                                    "employed": ['user0', 'user1', 'user2', 'user3', 'user6', 'user8'],
                                    "completed": ['user0', 'user1', 'user2', 'user3', 'user6', 'user8'],
                                    "repeated": [],
                                    "skipped": []
                                }]
	        },

            {
                  "versions": [{
                                    "id": 7,
                                    "title": "chapter7",
                                    "employed": ['user0', 'user1', 'user3', 'user4', 'user5', 'user8'],
                                    "completed": ['user0', 'user1', 'user3', 'user4', 'user5', 'user8'],
                                    "repeated": [],
                                    "skipped": []
                                }]
	        },

            {
                  "versions": [{
                                    "id": 8,
                                    "title": "chapter8",
                                    "employed": ['user0', 'user1', 'user2', 'user3', 'user4', 'user5',
                                             'user8', 'user9'],
                                    "completed": ['user0', 'user1', 'user2', 'user3', 'user4', 'user5',
                                             'user8', 'user9'],
                                    "repeated": [],
                                    "skipped": []
                                }]
	        },

            {
                  "versions": [{
                                    "id": 9,
                                    "title": "chapter9",
                                    "employed": ['user1', 'user3', 'user4', 'user6'],
                                    "completed": ['user1', 'user3', 'user4', 'user6'],
                                    "repeated": [],
                                    "skipped": []
                                }]
	        }
]




dict_chapIndex_chapID = {
    0:'chapter0',
    1:'chapter1',
    2:'chapter2',
    3:'chapter3',
    4:'chapter3_v2',
    5:'chapter5',
    6:'chapter6',
    7:'chapter7',
    8:'chapter8',
    9:'chapter9'
}


version_info = {
    # unit index : corresponding base unit index
    0:0,
    1:1,
    2:2,
    3:3,
    4:3,
    5:5,
    6:6,
    7:7,
    8:8,
    9:9
}

sequences = {
         "user0" : [0, 1, 2, 3, 4, 5, 6, 7, 8] ,
         "user1" : [0, 1, 5, 6, 7, 8, 9] ,
         "user2" : [0, 1, 2, 6, 8] ,
         "user3" : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] ,
         "user4" : [0, 2, 7, 8, 9] ,
         "user5" : [0, 1, 3, 4, 5, 7, 8] ,
         "user6" : [0, 1, 2, 3, 5, 6, 9] ,
         "user7" : [0, 5] ,
         "user8" : [0, 1, 2, 3, 5, 6, 7, 8] ,
         "user9" : [0, 1, 2, 3, 8] ,
        }



correct_fromToUsers_dict = {
                            0 : {
                                  1 : ['user0', 'user1', 'user2', 'user3', 'user5', 'user6', 'user8', 'user9'],
                                  2 : ['user4'],
                                  5 : ['user7'],
                                },

                            1 : {
                                  2 : ['user0', 'user2', 'user3', 'user6', 'user8', 'user9'],
                                  3 : ['user5'],
                                  5 : ['user1'],
                                },

                            2 : {
                                  3 : ['user0', 'user3', 'user6', 'user8', 'user9'],
                                  6 : ['user2'],
                                  7 : ['user4'],
                                },

                            3 : {
                                  4 : ['user0', 'user3', 'user5'],
                                  5 : ['user6', 'user8'],
                                  8 : ['user9']
                                },

                            4 : {
                                  5 : ['user0', 'user3', 'user5'],
                                },

                            5 : {
                                  6 : ['user0', 'user1', 'user3', 'user6', 'user8'],
                                  7 : ['user5'],
                                },

                            6 : {
                                  8 : ['user2'],
                                  9 : ['user6'],
                                  7 : ['user0', 'user1', 'user3', 'user8'],
                                },

                            7 : {
                                  8 : ['user0', 'user1', 'user3', 'user4', 'user5', 'user8'],
                                },

                            8 : {
                                  9 : ['user1', 'user3', 'user4'],
                                }

                        }



correct_result = [
                    {'pathID': 0, 'from': 'chapter0', 'to': 'chapter1', 'user_count': 8, 'skip': 0},
                    {'pathID': 1, 'from': 'chapter1', 'to': 'chapter2', 'user_count': 6, 'skip': 0},
                    {'pathID': 2, 'from': 'chapter2', 'to': 'chapter4', 'user_count': 3, 'skip': 3},
                    {'pathID': 3, 'from': 'chapter4', 'to': 'chapter5', 'user_count': 3, 'skip': 0},
                    {'pathID': 4, 'from': 'chapter5', 'to': 'chapter6', 'user_count': 5, 'skip': 0},
                    {'pathID': 5, 'from': 'chapter6', 'to': 'chapter9', 'user_count': 1, 'skip': 8},
                    {'pathID': 6, 'from': 'chapter0', 'to': 'chapter5', 'user_count': 1, 'skip': 4},
                    {'pathID': 7, 'from': 'chapter0', 'to': 'chapter2', 'user_count': 1, 'skip': 1},
                    {'pathID': 8, 'from': 'chapter2', 'to': 'chapter7', 'user_count': 1, 'skip': 6},
                    {'pathID': 9, 'from': 'chapter7', 'to': 'chapter8', 'user_count': 6, 'skip': 0},
                    {'pathID': 10, 'from': 'chapter8', 'to': 'chapter9', 'user_count': 3, 'skip': 0},
                    {'pathID': 11, 'from': 'chapter1', 'to': 'chapter3', 'user_count': 1, 'skip': 2},
                    {'pathID': 12, 'from': 'chapter3', 'to': 'chapter4', 'user_count': 3, 'skip': 0},
                    {'pathID': 13, 'from': 'chapter5', 'to': 'chapter7', 'user_count': 1, 'skip': 6},
                    {'pathID': 14, 'from': 'chapter2', 'to': 'chapter6', 'user_count': 1, 'skip': 5},
                    {'pathID': 15, 'from': 'chapter6', 'to': 'chapter8', 'user_count': 1, 'skip': 7},
                    {'pathID': 16, 'from': 'chapter2', 'to': 'chapter3', 'user_count': 5, 'skip': 0},
                    {'pathID': 17, 'from': 'chapter6', 'to': 'chapter7', 'user_count': 4, 'skip': 0},
                    {'pathID': 18, 'from': 'chapter1', 'to': 'chapter5', 'user_count': 1, 'skip': 4},
]




def test():

    PathsInfoOfCourse_instance1 = PathsInfoOfCourse(dict_chapIndex_chapID, sequences)

    # PathsInfoOfCourse_instance1.make_unit_info_list()
    # units_info_list = PathsInfoOfCourse_instance1.units_info_list
    # print(units_info_list)

    result = PathsInfoOfCourse_instance1.get_result()

    return result


class MyTest(unittest.TestCase):

    def test1(self):
        result = test()
        self.assertEqual(result['paths'], correct_result)

    def test2(self):
        result = test()
        self.assertEqual(result['units'], correct_units_info)

    def test3(self):
        result = test()
        self.assertEqual(result['meta'], correct_meta_info)



class PathsInfoOfCourse():

    def __init__(self, dict_chapIndex_chapID, dict_user_sequence_of_visited_units):
        self.path_list = []
        self.dict_from_to_users = {}   #dict[from][to] = [users]
        self.dict_chapIndex_chapID = dict_chapIndex_chapID

        self.dict_user_sequence_of_visited_units = dict_user_sequence_of_visited_units

        self.meta_info = {   "employees": {
                                        "employed": [],
                                        "completed": []
                                     }
            	    }

        # create the units_info upon creation of an instance
        self.units_info_list = [] # this is part of the output
        self.units_info_dict = {} # this is an easy-to-use format, in order to update easily.
        for ind in dict_chapIndex_chapID:
            if version_info[ind] == ind:
                tmp = {'version':[ {'id': ind,
                                    'title': dict_chapIndex_chapID[ind],
                                    'employed': [],
                                    'completed': [],
                                    'repeated': [],
                                    'skipped': []
                                    }]}
                self.units_info_dict[ind] = tmp
            else:
                tmp = { 'id': ind,
                        'title': dict_chapIndex_chapID[ind],
                        'employed': [],
                        'completed': [],
                        'repeated': [],
                        'skipped': []
                        }
                self.units_info_dict[version_info[ind]]['version'].append(tmp)


    def update_with_user_sequence_of_visited_units(self):

        for userID in self.dict_user_sequence_of_visited_units:

            sequence = self.dict_user_sequence_of_visited_units[userID]

            for i in xrange(0, len(sequence)-1):
                from_unit_index = sequence[i]
                to_unit_index = sequence[i+1]

                if [from_unit_index, to_unit_index] not in self.path_list:
                    self.path_list.append([from_unit_index, to_unit_index])

                if from_unit_index not in self.dict_from_to_users:
                    self.dict_from_to_users[from_unit_index] = {to_unit_index : [userID]}
                else:
                    if to_unit_index not in self.dict_from_to_users[from_unit_index]:
                        self.dict_from_to_users[from_unit_index][to_unit_index] = [userID]
                    else:
                        self.dict_from_to_users[from_unit_index][to_unit_index].append(userID)

                base_unit_index = self.get_base_unit_index(i)
                # find the index in verions list
                if base_unit_index is not i:
                    version_ind = 0
                else:
                    for this in xrange(0, len(self.units_info_dict[i]['version'])):
                        if self.units_info_dict[base_unit_index]['version'][this]['id'] == i:
                            version_ind = this


                # update the units_info_dict: employed / completed / repeated / skipped
                if userID not in self.units_info_dict[base_unit_index]['version'][version_ind]['employed']:
                    self.units_info_dict[base_unit_index]['version'][version_ind]['employed'].append(userID)
                if userID not in self.units_info_dict[base_unit_index]['version'][version_ind]['completed']:
                    self.units_info_dict[base_unit_index]['version'][version_ind]['completed'].append(userID)
                if i != 0:
                    if sequence[i] == sequence[i-1]: # repeated
                        if userID not in self.units_info_dict[base_unit_index]['version'][version_ind]['repeated']:
                            self.units_info_dict[base_unit_index]['version'][version_ind]['repeated'].append(userID)


            # find and update skipped info
            for i in xrange(1, len(sequence)-1):
                if sequence[i-1] < sequence[i] - 1:
                    # for each skipped unit, check about version info
                    for skipped_unit_index in xrange(sequence[i-1]+1, sequence[i]):
                        base_unit_index = self.get_base_unit_index(skipped_unit_index)
                        if base_unit_index == skipped_unit_index and userID not in self.units_info_dict[i]['version'][0]['skipped']:
                            self.units_info_dict[i]['version'][0]['skipped'].append(userID)


    def get_base_unit_index(self, skipped_unit_index):
        base_unit_index = skipped_unit_index
        if skipped_unit_index == 4:
            base_unit_index = 3

        return base_unit_index


    def make_list_of_dicts_for_path_info(self):

        list_of_dicts_for_path_info = []

        for pathID, path in enumerate(self.path_list):
            fromIndex = path[0]
            toIndex = path[1]
            from_chap_id = self.dict_chapIndex_chapID[fromIndex]
            to_chap_id = self.dict_chapIndex_chapID[toIndex]
            user_count = len(self.dict_from_to_users[fromIndex][toIndex])
            if toIndex > fromIndex + 1:
                skipped_chap_index = toIndex - 1
            else:
                skipped_chap_index = 0

            list_of_dicts_for_path_info.append({'pathID': pathID,
                                                'from': from_chap_id,
                                                'to': to_chap_id,
                                                'user_count':user_count,
                                                'skip': skipped_chap_index})

        return list_of_dicts_for_path_info


    def update_meta_info(self):

        self.meta_info = {   "employees": {
                                "employed": [],
                                "completed": []
                             }
                        }

        last_unit_index = max(self.dict_chapIndex_chapID.keys())
        print('last unit index : %s' %last_unit_index)

        for userID in self.dict_user_sequence_of_visited_units:
            sequence = self.dict_user_sequence_of_visited_units[userID]
            self.meta_info['employees']['employed'].append(userID)
            if last_unit_index in sequence:
                self.meta_info['employees']['completed'].append(userID)

        self.meta_info['employees']['completed'].sort()
        self.meta_info['employees']['employed'].sort()


    def make_unit_info_list(self):
        self.units_info_list = []
        for ind in self.units_info_dict:
            self.units_info_list.append(self.units_info_dict[ind])


    def get_result(self):

        self.update_with_user_sequence_of_visited_units()
        self.update_meta_info()
        list_of_dicts_for_path_info = self.make_list_of_dicts_for_path_info()
        self.make_unit_info_list()

        result = {'meta':self.meta_info, 'paths':list_of_dicts_for_path_info, 'units':self.units_info_list}

        return result






# print('////////////')
# result = test()
# if result['meta'] == correct_meta_info:
#     print 'yes'
# else:
#     print(result['meta'])
#     print("    ")
#     print(correct_meta_info)
# print('////////////')


# '''
#
# var dataJSON = {
# 	"meta": {
# 		"employees": {
# 			"employed": ["AA AA", "BB BB", "CC CC", "DD DD", "EE EE", "FF FF"],
# 			"completed": ["AA AA", "BB BB", "CC CC"]
# 		}
# 	},
#   "paths": [
#     {
#       "arrowID": 0,
#       "from": 2,
#       "to": 4,
#       "user_count": 2,
#       "skip": false
#     },
#     {
#       "arrowID": 1,
#       "from": 4,
#       "to": 7,
#       "user_count": 2,
#       "skip": false
#     },
#     {
#       "arrowID": 2,
#       "from": 7,
#       "to": 8,
#       "user_count": 1,
#       "skip": false
#     },
#     {
#       "arrowID": 3,
#       "from": 3,
#       "to": 8,
#       "user_count": 3,
#       "skip": 6
#     },
#     {
#       "arrowID": 4,
#       "from": 2,
#       "to": 5,
#       "user_count": 1,
#       "is_skip": false
#     },
#     {
#       "arrowID": 5,
#       "from": 5,
#       "to": 6,
#       "user_count": 2,
#       "skip": false
#     }
#   ],
# 	"units": [{
# 		"versions": [{
#       "id": 2,
# 			"title": "Welcome",
#       "employed": ["AA AA", "BB BB", "CC CC", "DD DD"],
# 			"completed": ["AA AA", "BB BB", "CC CC", "DD DD"],
# 			"repeated": [],
# 			"skipped": []
# 		}]
# 	}, {
# 		"versions": [{
#       "id": 3,
# 			"title": "Laptop Security<br>v1",
#       "employed": ["AA AA", "BB BB", "CC CC"],
# 			"completed": ["AA AA", "BB BB"],
# 			"repeated": ["AA AA"],
# 			"skipped": []
# 		}, {
#       "id": 4,
# 			"title": "Laptop<br>v2",
#       "employed": ["AA AA", "BB BB"],
# 			"completed": ["AA AA", "BB BB"],
# 			"repeated": [],
# 			"skipped": []
# 		}, {
#       "id": 5,
# 			"title": "Security<br>v3",
#       "employed": ["CC CC"],
# 			"completed": ["CC CC"],
# 			"repeated": [],
# 			"skipped": []
# 		}]
# 	}, {
# 		"versions": [{
#       "id": 6,
# 			"title": "In the Office<br>v1",
#       "employed": ["AA AA", "CC CC"],
# 			"completed": ["AA AA"],
# 			"repeated": ["AA AA"],
# 			"skipped": ["BB BB"]
# 		}, {
#       "id": 7,
# 			"title": "In the Office<br>accelerated",
#       "employed": ["AA AA", "BB BB"],
# 			"completed": ["BB"],
# 			"repeated": [],
# 			"skipped": []
# 		}]
# 	}, {
# 		"versions": [{
#       "id": 8,
# 			"title": "While Traveling",
#       "employed": ["AA AA", "BB BB", "CC CC"],
# 			"completed": ["AA AA", "BB BB", "CC CC"],
# 			"repeated": [],
# 			"skipped": []
# 		}]
# 	}]
# };