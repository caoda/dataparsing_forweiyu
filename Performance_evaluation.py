__author__ = 'dacao'
import os
import datetime
import math
import json
import sys




algo_debug = True

def print_debug(*args):
    if algo_debug is True:
        print(args);
        
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////  define parameters  ///////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////  The function which Ruediger will call ///////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def get_measurements(allAnsweringQuizEvents, numOfQ): # for one chapter
    # allAnsweringQuizEvents = {
    #                           # ID : allAnsweringQuizEvents
    #                           user1: [{}, {}, {},....,{}],   # each {} is a single quiz event. The list contains events from just the quiz answering events of this user.
    #                           user2: [{}, {}, {},....,{}],
    #                               ...
    #                           }
    #
    #
    #  scoreD = {
    #                    userID_1 : {
    #                                 quizID1 : performance score,
    #                                 quizID2 : performance score,
    #                                   ...
    #                                }
    #
    #                    userID_2 : {
    #                                 quizID1 : performance score,
    #                                 quizID2 : performance score,
    #                                   ...
    #                                }
    #
    #                     ....
    #
    #
    #                  }
    #

    # return output = {
    #
    #                 averageCompletionRate = 100,
    #                 totalTimeSpent = 121212,
    #
    #                 views = [
    #                                 {
    #                                 questionID = questionID,
    #
    #                                 timeSpent = 99,
    #                                 timesViewed = 2,
    #                                 avgScore = 90,
    #                                 absolute = 99,
    #                                 }
    #                                 ]
    # }
    #


    scoreD = {}
    scoreA = {}
    timeSpentD = {}
    numberOfVisits = {}


    for key in allAnsweringQuizEvents: # go through each user
        allEvents = allAnsweringQuizEvents[key]
        if not(key in scoreD):
            scoreD[key] = {}   # initiate the userID in the score dict
        if not (key  in scoreA):
            scoreA[key] = {}
            
        for i in range(0, len(allEvents)): # go through each event of this user
            event = allEvents[i]
            timestamp = event["timestamp"]
            quizID = event["question"]

            timeSpent = event["timeSpent"]
            confidence = event["isSure"]
            selectedAnswers = event["selectedAnswer"]
            correctAnswers = event["correctAnswer"]
            score = event["score"]

            if not(quizID in scoreD[key]): # if this quiz ID is not in the user's dict:
                # calculate the performance score for this quiz of this user
                perf, absolute = get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score)
                scoreD[key][quizID] = [perf, timestamp]
                scoreA[key][quizID]  = absolute
            else:
                # if quizID already there, then check which is earlier.
                if timestamp < scoreD[key][quizID][1]:
                    perf, absolute = get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score)
                    scoreD[key][quizID] = [perf, timestamp]
                    scoreA[key][quizID]  = absolute

            ################################################
            if not(quizID in timeSpentD):
                timeSpentD[quizID] = timeSpent
            else:
                timeSpentD[quizID] = timeSpentD[quizID] + timeSpent

            if not(quizID in numberOfVisits):
                numberOfVisits[quizID] = 1
            else:
                numberOfVisits[quizID] += 1
            ################################################

    # delete the timestamp info
    for uID in scoreD:
        for qID in scoreD[uID]:
            perf = scoreD[uID][qID][0]
            scoreD[uID][qID] = perf


    views = []
    totalTimeSpent = 0
    for quizID in timeSpentD:
        totalTimeSpent = totalTimeSpent + timeSpentD[quizID]

        totalScore_thisQuiz = 0
        totalAbsoluteScore = 0

        num = 0.0
        for name in scoreD:
            if quizID in scoreD[name]:
                num += 1.0
                totalScore_thisQuiz = totalScore_thisQuiz + scoreD[name][quizID]
                totalAbsoluteScore = totalAbsoluteScore + scoreA[name][quizID]

        avgScore_thisQuiz = totalScore_thisQuiz / num
        totalAbsoluteScore = totalAbsoluteScore / num

        tmp = {"quizID": quizID, "timeSpent":timeSpentD[quizID], "timesViewed":numberOfVisits[quizID], "avgScore":
            avgScore_thisQuiz, "absoluteScore": totalAbsoluteScore}
        views.append(tmp)


    output = {"averageCompletionRate" : int(100*len(timeSpentD.keys())/float(numOfQ)), "totalTimeSpent":totalTimeSpent,
              "views":views}

    return output



def get_perf(timeSpent, confidence, selectedAnswers, correctAnswers, score):

    if confidence:
        if selectedAnswers == correctAnswers:
            perf = 1.0
            absolute = score
        else:
            perf = 0.0
            absolute = 0
    else:
        perf = 0.5
        absolute = score / 2.0

    return perf, absolute



# test:

"""
allAnsweringQuizEvents = {
    "userID_1": [
        {"timestamp":100100.0, "id":"q1",  "chapterID":"", "timeSpent":2.0, "isSure":bool(0),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100110.0, "id":"q2",  "chapterID":"", "timeSpent":2.0, "isSure":bool(0),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100120.0, "id":"q3",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100130.0, "id":"q4",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100140.0, "id":"q1",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100150.0, "id":"q2",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"}
    ],

    "userID_2": [
        {"timestamp":100100.0, "id":"q1",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100110.0, "id":"q2",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100120.0, "id":"q3",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"a",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100130.0, "id":"q4",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100140.0, "id":"q1",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"},
        {"timestamp":100150.0, "id":"q2",  "chapterID":"", "timeSpent":2.0, "isSure":bool(1),  "selectedAnswers":"c",  "score":1,  "correctAnswers":"a",  "type":"multi choice"}
    ]

}


score = get_measurements(allAnsweringQuizEvents)
print_debug(score)
"""


