__author__ = 'dacao'

from measurements_new import Measurements_article, Measurements_pdf, Measurements_storyline
import json
import csv
import datetime

import functions as func
import define_attributes_Vel1_new_features
import VEL1_feature_parsing_for_one_user_abs_eng as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


raw_fileName = 'raw_clickstream/measurements-velocity-chess-vel_1.json'
events = json.load(open(raw_fileName))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])


x = []
for evt in events:
    if evt['m_type'] == 'PDFMeasurement':
        print(evt)
        break

for evt in events:
    if evt['m_type'] == 'ArticleViewPositionMeasurement':
        print(evt)
        break








chap_content = {

    'VEL_1_CHAPTER0':[['VEL_1_CONTENT0'], ['pdf']],
    'VEL_1_CHAPTER1':[['VEL_1_CONTENT1'], ['epub']],
    'VEL_1_CHAPTER2':[['VEL_1_CONTENT2'], ['storyline']],
    'VEL_1_CHAPTER3':[['VEL_1_CONTENT3'], ['storyline']],
    'VEL_1_CHAPTER4':[['VEL_1_CONTENT4'], ['storyline']],
    'VEL_1_CHAPTER5':[['VEL_1_CONTENT5'], ['storyline']],
    'VEL_1_CHAPTER6':[['VEL_1_CONTENT6'], ['epud']]
}

clean_events = []
for evt in events:
    if 'chapter' in evt:
        if 'content' in evt and evt['content'] not in chap_content[ evt['chapter'] ][0]:
            continue
        if 'article' in evt and evt['article'] not in chap_content[ evt['chapter'] ][0]:
            continue
    clean_events.append(evt)

events = clean_events
events = sorted(events, key=lambda k: k['timestamp'])
del clean_events

content_list = []
for evt in events:
    if 'content' in evt and evt['content'] not in content_list:
        content_list.append(evt['content'])
    if 'article' in evt and evt['article'] not in content_list:
        content_list.append(evt['article'])
content_list = sorted(content_list)



chapters = chap_content.keys()
chap_questions = {}
for c in chapters:
    chap_questions[c] = []
for evt in events:
    if 'question' in evt and 'chapter' in evt:
        c = evt['chapter']
        q = evt['question']
        if q not in chap_questions[c]:
            chap_questions[c].append(q)
for c in chap_questions:
    chap_questions[c].sort()

# for c in chap_questions:
#     print('%s : %s' %(c, chap_questions[c]))


uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
users = sorted(uv.keys())




''' /////////////////////////////////////////  define the attributes   ///////////////////////////////////////////'''
attr, chap_names = define_attributes_Vel1_new_features.get(events, chap_questions)

def filter_dup_Pause(storyLine_evts):
    new = []
    ref = -1
    for evt in storyLine_evts:
        add = True
        if 'm_type' in evt and evt['m_type'] == m_slide:
            if 'type' in evt and evt['type'] == 'Pause':
                t = evt['timestamp']
                if t-ref < 0.2 and t-ref > -0.2: # duplicate event found
                    print('dup pause  %s  %s' %(evt['chapter'], evt['user']) )
                    add = False
                else:
                    ref = t
        if add:
            new.append(evt)
    return new



def init_user_file_timespent_dict(users, content_list):

    user_file_timespent = {}
    for user in users:
        user_file_timespent[user] = {}
        for content in content_list:
            user_file_timespent[user][content] = 0

    return user_file_timespent


def get_content_id_from_events(storyline_events):
    contentIDs = []
    for evt in storyline_events:
        if 'content' in evt and evt['content'] not in contentIDs:
            contentIDs.append( evt['content'] )

    if len(contentIDs) == 1:
        return contentIDs[0]
    else:
        return False


def write_user_file_timespent_matrix_to_csv(user_file_timespent, content_list):

    nameSTR = 'user_file_timespent.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + content_list)

    for user in user_file_timespent:
        tmp = [user]
        for content_id in content_list:
            tmp.append(  user_file_timespent[user][content_id]  )

        wr.writerow(tmp)

    fileName.close()
    print('user_file_timespent csv file done!! \n')
    return 0






# todo: main
def main():

    nameSTR = 'matrix_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["user"] + attr)


    user_file_timespent = init_user_file_timespent_dict(users, content_list)

    for user in users:

        row, user_file_timespent = parsing.core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos,
                                                userInfo, course_name, session_number, user_file_timespent)

        wr.writerow([user] + row)

    fileName.close()
    print('csv file done!! \n')


    print('attributes are:   ')
    for i in xrange(0, len(attr)):
        print(attr[i])


    write_user_file_timespent_matrix_to_csv(user_file_timespent, content_list)

    return 0

########################################################################################################################



def get_users_paths(all_nav, user, all_paths):

    path = []
    for evt in all_nav:
        if evt['action'] == 'open':
            chapNum = int ( evt['chapter'][-1] )
            path.append(chapNum)
    #print("user %s has path: %s" %(user, path))
    all_paths[user]= path

    return all_paths


def user_path_dictionary():

    nameSTR = 'users_and_paths_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow(["chapters/paths"])

    all_paths = {}

    for user in users:
        all_evts = uv[user]
        all_paths = get_users_paths(func.get_evt_from_list(all_evts, m_navi), user, all_paths)

    for user in all_paths:
        print('%s:   %s' %(user, all_paths[user]))
        row = [user] + all_paths[user]
        wr.writerow(row)

    fileName.close()
    print( '%s done!!'  %nameSTR )

    return 0


def quiz_matrix():

    user_questionEvents = {}
    for u in users:
        user_questionEvents[u] = []
    for evt in events:
        if 'question' in evt and 'chapter' in evt:
            u = evt['user']
            user_questionEvents[u].append(evt)


    # define attributes
    attr = []
    for ch in sorted( chap_questions.keys() ):
        for q in sorted( chap_questions[ch] ):
            attr.append('%s_%s'  %(ch, q))

    all_num_attempts_to_correct = []
    all_num_of_answering = []

    for user in users:

        this_user_attributes = []
        this_num_of_answering = []

        for ch in sorted( chap_questions.keys() ):
            for q in sorted( chap_questions[ch] ):

                quiz_events = []
                for evt in user_questionEvents[user]:
                    if evt['question'] == q and evt['chapter'] == ch:
                        quiz_events.append(evt)
                quiz_events = sorted(quiz_events, key=lambda k: k['timestamp'])

                num_attemps_to_correct = 1
                for i, quiz_evt in enumerate(quiz_events):
                    if quiz_evt['score'] != 10:
                        num_attemps_to_correct += 1
                    else:
                        break

                    if i == len(quiz_events) - 1  and quiz_evt['score'] != 10:
                        num_attemps_to_correct = -1

                if len(quiz_events) == 0:
                    num_attemps_to_correct = 0

                this_user_attributes.append(num_attemps_to_correct)
                this_num_of_answering.append( len(quiz_events) )

        all_num_attempts_to_correct.append(this_user_attributes)
        all_num_of_answering.append(this_num_of_answering)



    nameSTR = 'num_attempts_to_correct_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow( ['user/quiz'] + attr )
    for i, user in enumerate(users):
        row = [user] + all_num_attempts_to_correct[i]
        wr.writerow(row)
    fileName.close()
    print('%s done!!' %nameSTR)

    nameSTR = 'num_of_answering_Vel1.csv'
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    wr.writerow( ['user/quiz'] + attr )
    for i, user in enumerate(users):
        row = [user] + all_num_of_answering[i]
        wr.writerow(row)
    fileName.close()
    print('%s done!!' %nameSTR)


    return 0



quiz_matrix()
user_path_dictionary()
main()


