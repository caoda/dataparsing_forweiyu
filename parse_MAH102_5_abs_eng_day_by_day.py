__author__ = 'dacao'

import json
import csv
import datetime
import unicodedata
import numpy as np

import functions as func
import define_attributes_new_with_abs_engagement
import PBP_feature_parsing_for_one_user_abs_eng as parsing

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'


''' set parameters '''
alpha_mode = 0.1
pdf_timeSpent_cap_on_page = 5 * 60
article_timeSpent_cap = 5 * 60
end_timestamp = 1492787455  # some time in April of 2017

course_name = 'MAH_102'
session_number = 5
course_name_to_remove_in_feature_name = 'PBP_MAH_102_5_CHAPTER'


''' input files '''
raw_events_from_reporting_tool = "raw_clickstream/measurements-pbp-pbp_mah_102_5.json"
users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_102_5-from-2015-10-01-to-2016-05-31.csv'
pass_fail_extend_info = "pass_info/MAH102_5_Pass_Info.txt"
print_user_info = False
print_pass_info = False


events = json.load(open(raw_events_from_reporting_tool))
events = events['measurements']
events = sorted(events, key=lambda k:k["timestamp"])
print('number of events: ')
print(len(events))
uv = func.get_uv_dict(events)
uv = func.make_valid(uv)
all_users = sorted(uv.keys())
courseID = func.get_course_id(events)

change_chap_names = {
    "PBP_MAH_102_5_CHAPTER00":"PBP_MAH_102_5_CHAPTER00",
    "PBP_MAH_102_5_CHAPTER10":"PBP_MAH_102_5_CHAPTER01",
    "PBP_MAH_102_5_CHAPTER20":"PBP_MAH_102_5_CHAPTER02",
    "PBP_MAH_102_5_CHAPTER30":"PBP_MAH_102_5_CHAPTER03",
    "PBP_MAH_102_5_CHAPTER40":"PBP_MAH_102_5_CHAPTER04",
    "PBP_MAH_102_5_CHAPTER50":"PBP_MAH_102_5_CHAPTER05",
    "PBP_MAH_102_5_CHAPTER60":"PBP_MAH_102_5_CHAPTER06",
    "PBP_MAH_102_5_CHAPTER70":"PBP_MAH_102_5_CHAPTER07",
    "PBP_MAH_102_5_CHAPTER80":"PBP_MAH_102_5_CHAPTER08",
    "PBP_MAH_102_5_CHAPTER90":"PBP_MAH_102_5_CHAPTER09",
    "PBP_MAH_102_5_CHAPTER100":"PBP_MAH_102_5_CHAPTER10",
}


for evt in events:
    if 'chapter' in evt and evt['chapter'] in change_chap_names:
        evt['chapter'] = change_chap_names[  evt['chapter']  ]


# get the storyline contentIDs
storyline_IDs = []
for evt in events:
    if evt.get('m_type') == m_slide and evt['content'] not in storyline_IDs:
        storyline_IDs.append(evt['content'])

dict_storylineID_slideInfos = {}
for contentID in storyline_IDs:
        all_slides_play_events = [evt for evt in events if evt.get('m_type')==m_slide and evt['content'] == contentID]
        slide_infos, slide_durations_list = func.get_slides_infos_and_durations(all_slides_play_events)
        dict_storylineID_slideInfos[contentID] = {'slide_infos':slide_infos, 'slide_durations_list':slide_durations_list}

# define the attributes
attr, chap_names, allChapNums, dates, chap_existing_modes = define_attributes_new_with_abs_engagement.get(events)



'''/////////////////////////////////////  Get the pass/extend info ///////////////////////////////////////////////'''
userInfo = func.user_info_list(users_from_reporting_tool, print_user_info)
passInfo = func.user_pass_fail_info_list(pass_fail_extend_info, print_pass_info)
pass_keywords = ['pass']
nopass_keywords = ['no pass', 'no_pass']
extend_keywords = ['extend', 'extend klh']
expired_keywords = ['expired']





def remove_course_name_from_chapterIDs(attr):
    feature_names = []
    for name in attr:
        if len(name) > 21 and name[0:21] == course_name_to_remove_in_feature_name:
            tmp = name[14:]
            feature_names.append(tmp)
        else:
            feature_names.append(name)

    return feature_names


def main(events, time_filter_datetime):

    ''' file name for output matrix '''
    output_file_name = 'day_by_day_matrix/mah102/matrix_MAH102_5_filtered_%s.csv' %time_filter_datetime

    timestamp_filter = func.normail_time_to_unix_timestamp(time_filter_datetime)
    events = func.filter_with_timestamp(events, timestamp_filter)

    nameSTR = output_file_name
    fileName = open(nameSTR, 'wb')
    wr = csv.writer( fileName, dialect= 'excel')
    feature_names = remove_course_name_from_chapterIDs(attr)
    print('number of features: %s' %len(feature_names))
    wr.writerow(["user"] + feature_names)


    uv = func.get_uv_dict(events)

    uv = func.make_valid(uv)


    for user in all_users:

        user_pass = func.get_pass(user, userInfo, passInfo, pass_keywords)
        user_nopass = func.get_NoPass(user, userInfo, passInfo, nopass_keywords)
        user_extend = func.get_extend(user, userInfo, passInfo, extend_keywords)
        user_expired = func.get_expired(user, userInfo, passInfo, expired_keywords)

        #print('%s  %s  %s  %s %s' %(user, user_pass, user_nopass, user_extend, user_expired))

        row = parsing.core(user, uv, chap_names, courseID, attr, dict_storylineID_slideInfos, userInfo,
                           user_pass, user_nopass, user_extend, user_expired, course_name, session_number, chap_existing_modes)


        if row == -1:
            continue

        wr.writerow([user] + row)
    print('number of feature values: %s' %len(row))

    fileName.close()
    print('csv file done!!      %s'  %output_file_name)


    return 0




def day_by_day():
    time_filter_datetimes = [
                            '2016_02_10_00_00',
                            '2016_02_11_00_00',
                            '2016_02_12_00_00',
                            '2016_02_13_00_00',
                            '2016_02_14_00_00',
                            '2016_02_15_00_00',
                            '2016_02_16_00_00',
                            '2016_02_17_00_00',
                            '2016_02_18_00_00',
                            '2016_02_19_00_00',
                            '2016_02_20_00_00',
                            '2016_02_21_00_00',
                            '2016_02_22_00_00',
                            '2016_02_23_00_00',
                            '2016_02_24_00_00'
                            ]


    for time_filter_datetime in time_filter_datetimes:
        main(events, time_filter_datetime)





    return 0








